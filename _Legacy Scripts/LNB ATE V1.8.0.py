#Modified from LNB ATE V1.6
#Modified by Benjamin Stadnik
#Orbital Research Ltd.

import xlwings as xw
import pyvisa as visa
import time
import math
import json
from twilio.rest import Client
import keysight.command_expert_py3 as kt
import os
import shutil
import tkinter as tk
from tkinter import *
from tkinter import ttk
from os import walk
from os import listdir
import datetime
import socket
import numpy as np
from pynput.keyboard import Key, Controller
from subprocess import Popen, PIPE
import DS_GV0_3 #Custom Datasheet Generation module

## Temp sensor

server_address = ('10.0.10.119', 2000) ## Address of Sensor
message2 = (b'*SRHC\r') ## Channel 2 Temp
message1 = (b'*SRTC\r') ## Channel 1 Temp

coderev = '1.8.1'
def intermdiate():
    try:
        main()
    except KeyboardInterrupt:
        Pwr_off(PS)
        print('Keyboard interrupt detected. Stopping test.')
        sys.exit()

def main():
    ## Equipment Addresses ##
    SA = 'TCPIP0::10.0.10.47::inst0::INSTR'
    SG = 'TCPIP0::10.0.10.138::inst0::INSTR'
    PS = 'USB0::0x0957::0x4D18::MY57510003::0::INSTR'
    PM = 'USB0::0x0957::0x2E18::MY48100296::0::INSTR'

    print("******* New Test ******")
    print("Checking for connection to equipment...")
    v16 = EQID(PS,SG,SA,PM)
    w16 = v16
    x16 = v16
    
    print ("Spec Analyzer = %s" %v16[0])
    print ("Signal Generator = %s" %v16[1])
    print ("Power Supply = %s" %v16[2])
    print ("Power Meter = %s" %v16[3])
    print('\n')

    #Function for Temp Sensor
    t = Get_Temp()
    print (t)
    room_temperature_C = float(t[1])

    ## Converting the value from Celsius to Kelvin
    room_temperature = float(room_temperature_C) + 273.15

    

    ## Obtain Date and Time    
    date = str(datetime.datetime.now().date())
    currenttime = str(datetime.datetime.now().time())

    print (date)
    print (currenttime)

    ## Obtain Program directory path
    path = os.path.dirname(__file__)

    print (path)

    ## Enable settings file array
    set_file = []

    ##Obtaining Settings file names and appending
    set_file.append(popupMenu1.get())
    set_file.append(popupMenu2.get())
    set_file.append(popupMenu3.get())
    print("Band 1 settings file: %s" %set_file[0])
    print("Band 2 settings file: %s" %set_file[1])
    print("Band 3 settings file: %s" %set_file[2])
    print('\n')

    Vsw = V_SW.get()


    if(Vsw == 0):
        print ("Voltage Switch Testing Disabled")
    else:
        print ("Voltage Switch Testing Enabled")
    print('\n')
    ##Obtaining Info from input fields
    Orbital_serial_number = str(SNText.get())
    print ("Serial Number: %s" %Orbital_serial_number)
    if(Orbital_serial_number == ''):
        Orbital_serial_number = 'No Name'
    Stock_number = str(StockText.get())
    print ("Stock Number: %s" %Stock_number)
    MnfN = str(MnfText.get())
    print("Manufacturer S/N: %s" %MnfN)
    Cust = str(CxText.get())
    print ("Customer: %s" %Cust)
    PoNum = str(POText.get())
    print ("PO Number: %s" %PoNum)  
    Col = str(ColorText.get())
    print ("Color: %s" %Col)
    Tester = str(YouText.get())
    print ("Tester: %s" %Tester)
    print( "RF Chamber Temperature: %f" %room_temperature_C)
    print('\n')

    data_file_name = Orbital_serial_number ## Second Variable for adding tags onto
    bname = Orbital_serial_number ## File name for moving files to and from folders
    SPC_serial_number = MnfN
     
    
    print("Please ensure calibration has been done")
    print("Verify external features of product if present (Button switching etc...)")
    print('\n')
    
    ## Opening band 1 settings file
    if (set_file[0] != 'None'):
        file1 = open(path + "\\Set_Files\\" + set_file[0],'rb')
        settings_data = json.load(file1)
    else:
        print("Band 1 Test not active")
        print('\n')
    ## Opening band 2 settings file if selected
    if (set_file[1] != 'None'):
        data_file_name2 = data_file_name + "_band2"
        file2 = open(path + "\\Set_Files\\" + set_file[1],'rb')
        settings_data2 = json.load(file2)
    else:
        print("Band 2 Test not active")
        print('\n')
        
    ## Opening band 3 settings file if selected
    if(set_file[2] != 'None'):
        data_file_name3 = data_file_name + "_band3"
        file3 = open(path + "\\Set_Files\\" + set_file[2],'rb')
        settings_data3 = json.load(file3)
    else:
        print("Band 3 Test not active")
        print('\n')


    Pwr_on(settings_data,PS)
    


    ## Making the Folder
    folder = Make_Folder(data_file_name)
    print('\n')
    ##prompt the user to setup for nf and gain
    if(RS485_EN.get() == 1):
        
        pt = USB_Port_Text.get()
        RS485_Init(pt)

        input('Connect Noise Source to DUT. Press ENTER to continue.')
    if (set_file[0] != 'None'):
        print("Testing Band 1 NF and current...")
        if(CC_SW.get() == 1):
            input('Switch to Band 1. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 1
            RS485_Commands(ID)
        v7 = get_current(settings_data,PS)
        print('\n')
        v1 = get_nf_gain(settings_data,room_temperature,data_file_name,SA,bname)
        print('\n')
    else:
        print("Band 1 settings file not enabled...moving on...")
        print('\n')

    ## NF and Current Test for Band 2
    if (set_file[1] != 'None'):
        print("Testing Band 2 NF and current...")
        if(CC_SW.get() == 1):
            input('Switch to Band 2. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 2
            RS485_Commands(ID)
        w7 = get_current(settings_data2,PS)
        print('\n')
        w1 = get_nf_gain(settings_data2,room_temperature,data_file_name2,SA,bname)
        print('\n')
    else:
        print("Band 2 settings file not enabled...moving on...")
        print('\n')
    ## NF and Current Test for Band 3
    if (set_file[2] != 'None'):
        print("Testing Band 3 NF and current...")
        if(CC_SW.get() == 1):
            input('Switch to Band 3. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 3
            RS485_Commands(ID)
        print('\n')
        x7 = get_current(settings_data3,PS)
        print('\n')
        x1 = get_nf_gain(settings_data3,room_temperature,data_file_name3,SA,bname)
        print('\n')
    else:
        print("Band 3 settings file not enabled...moving on...")
        print('\n')

    
    ## For Multiple Bands create a new data set for each band    
    #prompt the user to setup for p1db
    s = input('Please connect DUT Cable to input port. Press ENTER to continue.')
    if (set_file[0] != 'None'):
        print("Testing Band 1...")
        if(CC_SW.get() == 1):
            input('Switch to Band 1. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 1
            RS485_Commands(ID)
        print('\n')
        Pwr_on(settings_data,PS)
        ## P1dB Test
        v2 = get_p1db(settings_data,SG,PM)
        ## Phase Noise Test
        print('\n')
        v3 = get_phase(settings_data,data_file_name,SG,SA,bname)
        ## Image Rejection Test
        print('\n')
        v4 = get_image_rejection(settings_data,v1[6],data_file_name,SG,SA,bname)
        ## Voltage Log (Switching) Test
        print('\n')
        if(Vsw == 0):            
            v15 = "Voltage switching test not enabled"
            print (v15)
        else:
            v15 = V_Log(settings_data,PS,SG,SA)
        ## In band spurs test
        print('\n')
        v6 = get_spurs(settings_data,v1[6],data_file_name,SG,SA,bname)
        ## LO Leakage Output Test
        print('\n')
        v5 = get_lo_leakage_output(settings_data,data_file_name,SG,SA,bname)
        ## LO Leakage Input Test
        print('\n')
        v10 = get_lo_leakage_input(settings_data,data_file_name,SG,SA,bname)
        ## S11 Test
        print('\n')
        v11 = get_vswrIP(data_file_name,settings_data,bname)
        ## S22 Test
        print('\n')
        v14 = get_vswrOP(data_file_name,PS,settings_data,bname)
        ## Obtaining other values
        v8 = SPC_serial_number
        v9 = Orbital_serial_number
        v12 = Stock_number
        v13 = room_temperature_C
        print('\n')

        #saving band 1 data in a json file
        data = {'nf_gain':v1,'p1db':v2,'phase':v3,'image_rej':v4,'spurs':v6,'lo_leakage':v5,'current':v7,
                'SPC_serial_number':v8,'Orbital_serial_number':v9,'lo_leakage_in':v10,
                'S11':v11,'S22':v14,'stock_number':v12,'room_temperature':v13,'Voltage_Switch_Up' :v15[0],'Voltage_Switch_Down': v15[1],
                'Spec_An':v16[0],'Sig_Gen':v16[2],'Pow_Sup':v16[1],'Pw_Met':v16[3],'Date': date, 'Time': currenttime,'Cx':Cust,
                'PO': PoNum,'Tester': Tester,'color': Col,'Unit_Band': data_file_name, 'CodeRev': coderev}
        with open(data_file_name+'.json','w') as f:
            json.dump(data,f)
        f.close

        ## Moving Files and Generating Datasheet for band 1
        file_name = data_file_name+ '.json'
        path = os.path.dirname(__file__)

        filed = path + "\\" + file_name

        target = path + "\\" + bname
        ## Moves File to Folder
        shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))

        #Copy for stats later
        shutil.copy((target + "\\" + file_name),path + '\\_TESTLOG')

        Datasheet_Gen(folder, settings_data, data_file_name)
        print('\n')
        

    if(set_file[1] != 'None'):
        print("Testing Band 2...")
        if(CC_SW.get() == 1):
            input('Switch to Band 2. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 2
            RS485_Commands(ID)
        print('\n')
        Pwr_on(settings_data2,PS)
        time.sleep(5)
        w2 = get_p1db(settings_data2,SG,PM)
        ## Phase Noise Test
        w3 = get_phase(settings_data2,data_file_name2,SG,SA,bname)
        ## Image Rejection Test
        print('\n')
        w4 = get_image_rejection(settings_data2,w1[6],data_file_name2,SG,SA,bname)
        ## Voltage Log (Switching) Test
        print('\n')
        if(Vsw == 0):            
            w15 = "Voltage switching test not enabled"
            print (v15)
        else:
            w15 = V_Log(settings_data2,PS,SG,SA)
        ## In band spurs test
        print('\n')
        w6 = get_spurs(settings_data2,w1[6],data_file_name2,SG,SA,bname)
        ## LO Leakage Output Test
        print('\n')
        w5 = get_lo_leakage_output(settings_data2,data_file_name2,SG,SA,bname)
        ## LO Leakage Input Test
        print('\n')
        w10 = get_lo_leakage_input(settings_data2,data_file_name2,SG,SA,bname)
        ## S11 Test
        print('\n')
        w11 = get_vswrIP(data_file_name2,settings_data2,bname)
        ## S22 Test
        print('\n')
        w14 = get_vswrOP(data_file_name2,PS,settings_data2,bname)
        ## Obtaining other values
        w8 = SPC_serial_number
        w9 = Orbital_serial_number
        w12 = Stock_number
        w13 = room_temperature_C
        print('\n')

    ## Saving Band 2 data to a json file
    if(set_file[1] != 'None'):
        data2 = {'nf_gain':w1,'p1db':w2,'phase':w3,'image_rej':w4,'spurs':w6,'lo_leakage':w5,'current':w7,
                'SPC_serial_number':w8,'Orbital_serial_number':w9,'lo_leakage_in':w10,
                'S11':w11,'S22':w14,'stock_number':w12,'room_temperature':w13,'Voltage_Switch_Up' :w15[0],'Voltage_Switch_Down': w15[1],
                'Spec_An':w16[0],'Sig_Gen':w16[2],'Pow_Sup':w16[1],'Pw_Met':w16[3],'Date': date, 'Time': currenttime,'Cx':Cust,
            'PO': PoNum,'Tester': Tester,'color': Col,'Unit_Band': data_file_name2, 'CodeRev': coderev}
        with open(data_file_name2+'.json','w') as f:
            json.dump(data2,f)
        f.close    

        ## Moving Files and Generating Datasheet for band 2
        file_name = data_file_name2+ '.json'
        path = os.path.dirname(__file__)

        filed = path + "\\" + file_name

        target = path + "\\" + bname
        ## Moves File to Folder
        shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))

        #Copy for stats later
        shutil.copy((target + "\\" + file_name),path + '\\_TESTLOG')

        Datasheet_Gen(folder, settings_data2, data_file_name2)
        print('\n')

    
    if(set_file[2] != 'None'):
        print("Testing Band 3...")
        if(CC_SW.get() == 1):
            input('Switch to Band 3. Press Enter to continue')
        if(RS485_EN.get() == 1):
            ID = 3
            RS485_Commands(ID)
        print('\n')
        Pwr_on(settings_data3,PS)
        time.sleep(5)
        x2 = get_p1db(settings_data3,SG,PM)
        print('\n')
        ## Phase Noise Test
        x3 = get_phase(settings_data3,data_file_name3,SG,SA,bname)
        print('\n')
        ## Image Rejection Test
        x4 = get_image_rejection(settings_data3,x1[6],data_file_name3,SG,SA,bname)
        ## Voltage Log (Switching) Test
        print('\n')
        if(Vsw == 0):            
            x15 = "Voltage switching test not enabled"
            print (v15)
        else:
            x15 = V_Log(settings_data3,PS,SG,SA)
        ## In band spurs test
        print('\n')
        x6 = get_spurs(settings_data3,x1[6],data_file_name3,SG,SA,bname)
        ## LO Leakage Output Test
        print('\n')
        x5 = get_lo_leakage_output(settings_data3,data_file_name3,SG,SA,bname)
        ## LO Leakage Input Test
        print('\n')
        x10 = get_lo_leakage_input(settings_data3,data_file_name3,SG,SA,bname)
        ## S11 Test
        print('\n')
        x11 = get_vswrIP(data_file_name3,settings_data3,bname)
        ## S22 Test
        print('\n')
        x14 = get_vswrOP(data_file_name3,PS,settings_data3,bname)
        ## Obtaining other values
        x8 = SPC_serial_number
        x9 = Orbital_serial_number
        x12 = Stock_number
        x13 = room_temperature_C
        print('\n')

        ## Saving band 3 Data to a json file
        data3 = {'nf_gain':x1,'p1db':x2,'phase':x3,'image_rej':x4,'spurs':x6,'lo_leakage':x5,'current':x7,
                'SPC_serial_number':x8,'Orbital_serial_number':x9,'lo_leakage_in':x10,
                'S11':x11,'S22':x14,'stock_number':x12,'room_temperature':x13,'Voltage_Switch_Up' :x15[0],'Voltage_Switch_Down': x15[1],
                'Spec_An':x16[0],'Sig_Gen':x16[2],'Pow_Sup':x16[1],'Pw_Met':x16[3],'Date': date, 'Time': currenttime,'Cx':Cust,
            'PO': PoNum,'Tester': Tester,'color': Col,'Unit_Band': data_file_name3, 'CodeRev': coderev}
        with open(data_file_name3+'.json','w') as f:
            json.dump(data3,f)
        f.close    

        ## Moving Files and Generating Datasheet for band 2
        file_name = data_file_name3+ '.json'
        path = os.path.dirname(__file__)

        filed = path + "\\" + file_name

        target = path + "\\" + bname
        ## Moves File to Folder
        shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))

        #Copy for stats later
        shutil.copy((target + "\\" + file_name),path + '\\_TESTLOG')

        Datasheet_Gen(folder, settings_data3,data_file_name3)
        print('\n')
    
    Pwr_off(PS)
    
    try:
        Merge_DSheets(data_file_name,set_file)
    except:
        print("Datasheet Merging failed...please merge manually")
    print ("Test completed, Please input next unit's Info")
    print('\n')

def Merge_DSheets(filename,set_file):
    i= 0
    if(set_file[0] != 'None'):
        if(set_file[2] != 'None'):
            try:
                print ("Merging band 3...")
                f3 = path + "\\" + filename + "\\" + filename + "_band3.xlsx"
                f1 = path + "\\" + filename + "\\" + filename + ".xlsx"
                ## Open WorkBook and copy 10 MHz Ripple value from sheet 2 to prevent showing testing directory
                wb3 = xw.Book(f3)
                a = xw.sheets(2).range('F4').value
                xw.sheets(1).range('I9').value = a

                ## Rename the Sheets
                xw.sheets(1).name = 'Band3'
                xw.sheets(2).name = 'Band3_Data'

                ##Open Parent file to append sheets to
                wb1 = xw.Book(f1)

                ## Specifying which sheets to be copied
                ws1 = wb3.sheets(1)
                ws2 = wb3.sheets(2)

                ## Copying the sheets from band 3 file to parent file
                ## Order reversed for proper formatting
                ws2.api.Copy(Before=wb1.sheets(1).api)
                ws1.api.Copy(Before=wb1.sheets(1).api)

                wb1.save()
                wb1.app.quit()

                i = i + 2
                
            except:
                print("Band 3 Excel file not found...Moving on")



        #### Open second band, format values, rename sheets, move to parent file
        if(set_file[1] != 'None'):
            try:
                print ("Merging Band 2...")
                f2 = path + "\\" + filename + "\\" + filename + "_band2.xlsx"
                f1 = path + "\\" + filename + "\\" + filename + ".xlsx"

                ## Open WorkBook and copy 10 MHz Ripple value from sheet 2 to prevent showing testing directory
                wb2 = xw.Book(f2)
                a = xw.sheets(2).range('F4').value
                xw.sheets(1).range('I9').value = a

                ## Rename the Sheets
                xw.sheets(1).name = 'Band2'
                xw.sheets(2).name = 'Band2_Data'

                ##Open Parent file to append sheets to
                wb1 = xw.Book(f1)

                ## Specifying which sheets to be copied
                ws1 = wb2.sheets(1)
                ws2 = wb2.sheets(2)

                ## Copying the sheets from band 3 file to parent file
                ## Order reversed for proper formatting
                ws2.api.Copy(Before=wb1.sheets(1).api)
                ws1.api.Copy(Before=wb1.sheets(1).api)

                wb1.save()
                wb1.app.quit()

                i = i + 2

                
            except:
                print("Band 2 Excel file not found...Moving on")


        #### Open first band, format values, rename sheets, move to parent file
        if(set_file[0] != 'None'):
            try:
                print ("Formatting Band 1...")
                f1 = path + "\\" + filename + "\\" + filename + ".xlsx"
                ## Open WorkBook and copy 10 MHz Ripple value from sheet 2 to prevent showing testing directory
                wb1 = xw.Book(f1)
                a = xw.sheets(i + 2).range('F4').value
                xw.sheets(i + 1).range('I9').value = a

                ## Specifying which sheets to be copied
                ws1 = wb1.sheets(i + 1)
                ws2 = wb1.sheets(i + 2)

                ## Copying the sheets from band 3 file to parent file
                ## Order reversed for proper formatting
                ws2.api.Copy(Before=wb1.sheets(1).api)
                ws1.api.Copy(Before=wb1.sheets(1).api)

                ## Rename the Sheets
                xw.sheets(1).name = 'Band1'
                xw.sheets(2).name = 'Band1_Data'

                ## Delete Original Sheets labelled "Sheet1 and Sheet2"
                wb1.sheets('Sheet1').delete()
                wb1.sheets('Sheet2').delete()
                

                wb1.save()
                wb1.app.quit()

                
            except:
                print("Band 1 Excel file not found...Moving on")

    else:
        print("Band 1 not detected...no parent file to merge into...skipping datasheet merging")

def Get_Temp():
    data = []
    print ("Obtaining Room Temp for Before/After DUT")
    try:
        ##socket 1
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)
        # Send data

        #print 'sending "%s"' % message
        sock.sendall(message1)

        # Look for the response
        amount_received = 0
        amount_expected = len(message1)

        while amount_received < amount_expected:
            s1 = sock.recv(16)
            data.append(s1)
            amount_received += len(s1)


        ## Socket 2
        #print 'sending "%s"' % message
        sock.sendall(message2)

        # Look for the response
        amount_received = 0
        amount_expected = len(message2)

        while amount_received < amount_expected:
            s2 = sock.recv(16)
            data.append(s2)
            amount_received += len(s2)
            

    finally:
        sock.close()

    return data

def Pwr_on(settings_data,PS):

    rm = visa.ResourceManager()
    U3606B = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
    time.sleep(1)
    U3606B.write(':OUTPut:STATe %d' %(1))
    U3606B.close()
   
def Pwr_off(PS):

    rm = visa.ResourceManager()
    U3606B  = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    U3606B.write(':OUTPut:STATe %d' %(0))
    U3606B.close()
    
def change_dropdown(*args):
    print( tkvar.get() )

def Datasheet_Gen(folder, settings_data, productNumber):
    print("Generating Datasheet...")
    config_file = os.path.dirname(__file__) + "\\Datasheet_Gen_2\\" + settings_data['Data_Sheet']
    data_file = folder + '\\' + productNumber + '.json'
    DS_GV0_3.GenerateDatasheet(folder, data_file, config_file)

def Make_Folder(productNumber):
    drp = os.getcwd()
    dst = str(drp) + '\\' + productNumber
    print ("Creating Folder...")
    if os.path.exists(dst):
        shutil.rmtree(dst)
    os.mkdir(dst)
	
    return dst

def Screenshot(productNumber,SA,tag,name):

    rm = visa.ResourceManager()
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 5000000
    #setting the format to save a screenshot
    E4440A.values_format.is_binary = True
    E4440A.values_format.datatype = 'B'
    E4440A.values_format.is_big_endian = False
    E4440A.values_format.container = bytearray

    #saving a screenshot on the equipment
    E4440A.write(":MMEMory:STORe:SCReen 'C:\\TEST.GIF\'")
    #fetching the image data in an array
    fetch_image_data = E4440A.query_values("MMEM:DATA? 'C:\\TEST.GIF\'")

    #creating a file with the product number to write the image data to it
    save_dir = open(str(productNumber)+ tag, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    E4440A.write(":MMEM:DEL 'C:\\TEST.GIF\'")
    E4440A.write("*CLS")

    ## Move screenshots to proper folder
    file_name = str(productNumber)+ tag
    path = os.path.dirname(__file__)

    filed = path + "\\" + file_name

    target = path + "\\" + name

    
    ## Moves Screenshot to Folder
    shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))


def get_nf_gain(settings_data,room_temperature,productNumber,SA,bname):
    #productNumber is entered by the user

    print ("starting NF and Gain test")
    tag = " NF and Gain.GIF"
    ##setting NFG Path
    [ChannelStatesOn] = kt.run_sequence('NFG Path')
##    print int(ChannelStatesOn)

    rm = visa.ResourceManager()
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 5000000

    #loading state file
    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['nf_gain_state_file'])+"'")

    #waiting for state file to load
    time.sleep(20)

    #loading limit lines
    try:
        E4440A.write(':MMEMory:LOAD:LIMit %s,"%s"' % ('LLINE1', str(settings_data['nf_gain_limit_1_file'])))
        E4440A.write(':CALCulate:NFIGure:LLINe1:DISPlay:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe1:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe1:TEST:STATe %d' % (1))
    except:
        print('Limit line 1 not found')
        pass
    try:
        E4440A.write(':MMEMory:LOAD:LIMit %s,"%s"' % ('LLINE3', str(settings_data['nf_gain_limit_3_file'])))
        E4440A.write(':CALCulate:NFIGure:LLINe3:DISPlay:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe3:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe3:TEST:STATe %d' % (1))
    except:
        print('Limit line 3 not found')
        pass
    try:
        E4440A.write(':MMEMory:LOAD:LIMit %s,"%s"' % ('LLINE4', str(settings_data['nf_gain_limit_4_file'])))
        E4440A.write(':CALCulate:NFIGure:LLINe4:DISPlay:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe4:STATe %d' % (1))
        E4440A.write(':CALCulate:NFIGure:LLINe4:TEST:STATe %d' % (1))  
    except:
        print('Limit line 4 not found')
        pass

    
    #setting the temperature of WR42 transistion
	
    E4440A.write(':SENSe:NFIGure:CORRection:TCOLd:USER:STATe OFF')
    E4440A.write(':SENSe:NFIGure:CORRection:TCOLd:USER:STATe ON')
    E4440A.write(':SENSe:NFIGure:CORRection:TCOLd:USER:VALue ' + str(room_temperature) + ' K')
    print('Room Temperature: ' + str(room_temperature - 273.15))

    #getting the start frequency and saving it in Fetch_start_Freq
    start_Freq = E4440A.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')
    Fetch_start_Freq = float(start_Freq[0])
    
    #getting the number of points and saving it in Fetch_number_of_points
    number_of_points = E4440A.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')
    Fetch_number_of_points = int(number_of_points[0])
    
    #getting the stop frequency and saving it in Fetch_stop_Freq
    stop_Freq = E4440A.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')
    Fetch_stop_Freq = float(stop_Freq[0])

    #activating the marker tab
    E4440A.write(':CALCulate:NFIGure:MARKer:MODE %s' % ('POSition'))

    #calculating the frequency
    Frequency = []
    Frequency.append(Fetch_start_Freq)
    add_next_Freq=((Fetch_stop_Freq-Fetch_start_Freq)/(Fetch_number_of_points-1))
    i = 1
    next_Frequency = Fetch_start_Freq
    while i < Fetch_number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        Frequency.append(next_Frequency)
        i = i + 1   
    Frequency.append(Fetch_stop_Freq)
##    print Frequency


    #Finding the index of the interested start and stop IF frequency 
    interested_start_frequency = float(settings_data['nf_gain_interested_start_Frequency'])
    interested_stop_frequency = float(settings_data['nf_gain_interested_stop_Frequency'])
    i = 0
    while i < len(Frequency):
        if (Frequency[i] == interested_start_frequency):
            index_interested_start_frequency = i
        if (Frequency[i] == interested_stop_frequency):
            index_interested_stop_frequency = i
        i = i + 1
    
    
    loss_after_mode = E4440A.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = E4440A.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')

    #saving the loss after compensation table
    if "FIX" in loss_after_mode:  
        loss_after_table = E4440A.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
##        print loss_after_table

    if "TABL" in loss_after_mode:  
        loss_after_table = E4440A.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
##        print loss_after_table
        
    #saving the loss before compensation table
    if "FIX" in loss_before_mode:
        loss_before_value = E4440A.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
##        print loss_before_value

    if "TABL" in loss_before_mode:
        loss_before_value = E4440A.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
##        print loss_before_value

    #getting the noise figure array
    NF = E4440A.query_ascii_values(':READ:NFIG:ARR:DATA:CORR:NFIG?')
##    print NF
    
    
    #finding the min and max NF over the interested frequencies
    i = index_interested_start_frequency
    interested_NF = []
    while i < index_interested_stop_frequency + 1:
        interested_NF.append(NF[i])
        i = i + 1
    min_NF = min(interested_NF)
    max_NF = max(interested_NF)
    print ("Minimum NF: %f" %min_NF)
    print ("Maximum NF: %f" %max_NF)
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = index_interested_start_frequency
    LinearNF = []
    while i < index_interested_stop_frequency + 1:
        LinearNFValue  = 10**(NF[i]/10)
        LinearNF.append(LinearNFValue)
        i = i + 1
##    print LinearNF

    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    #########################
    i = 0
    ###index_interested_start_frequency
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF = sumLinearNF + LinearNF[i]
        i = i + 1   
    averageLinearNF = sumLinearNF/len(LinearNF)
    averageNF = 10*math.log10(averageLinearNF)
    print ("Average NF: %f" %averageNF)

    #getting the gain array
    Gain = E4440A.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
##    print Gain
        
    #finding the min and max Gain over the interested frequencies
    i = index_interested_start_frequency
    interested_Gain = []
    while i < index_interested_stop_frequency + 1:
        interested_Gain.append(Gain[i])
        i = i + 1
    min_Gain = min(interested_Gain)
    max_Gain = max(interested_Gain)
    print ("Minimum Gain: %f" %min_Gain)
    print ("Maximum Gain: %f" %max_Gain)
    
    #calculating the linear gain values (only for interested frequencies)
    i = index_interested_start_frequency
    LinearGain = []
    while i < index_interested_stop_frequency + 1:
        LinearGainValue  = 10**(Gain[i]/10)
        LinearGain.append(LinearGainValue)
        i = i + 1
##    print LinearGain

    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    ##################
    i = 0 ###index_interested_start_frequency
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain = sumLinearGain + LinearGain[i]
        i = i + 1 
    averageLinearGain = sumLinearGain/len(LinearGain)
    averageGain = 10*math.log10(averageLinearGain)
    print ("Average Gain: %f" %averageGain)

    #finding the length of points to cover 10MHz
    i = index_interested_start_frequency
    added_10MHz_value = Frequency[i] + 10000000
    count_10_MHz = 0
    cond_10_MHz = 0
    while i < index_interested_stop_frequency + 1:
        if(Frequency[i] == added_10MHz_value):
            count_10_MHz = count_10_MHz + 1
            cond_10_MHz = 1
            break
        count_10_MHz = count_10_MHz + 1
        i = i + 1

    #finding the length of points to cover 120MHz
    i = index_interested_start_frequency
    added_120MHz_value = Frequency[i] + 120000000
    count_120_MHz = 0
    cond_120_MHz = 0
    while i < index_interested_stop_frequency + 1:
        if(Frequency[i] == added_120MHz_value):
            count_120_MHz = count_120_MHz + 1
            cond_120_MHz = 1
            break
        count_120_MHz = count_120_MHz + 1
        i = i + 1

    #finding the length of points to cover 500MHz
    i = index_interested_start_frequency
    added_500MHz_value = Frequency[i] + 500000000
    count_500_MHz = 0
    cond_500_MHz = 0
    while i < index_interested_stop_frequency + 1:
        if(Frequency[i] == added_500MHz_value):
            count_500_MHz = count_500_MHz + 1
            cond_500_MHz = 1
            break
        count_500_MHz = count_500_MHz + 1
        i = i + 1

    #finding the length of points to cover 1000MHz
    i = index_interested_start_frequency
    added_1000MHz_value = Frequency[i] + 1000000000
    count_1000_MHz = 0
    cond_1000_MHz = 0
    while i < index_interested_stop_frequency + 1:
        if(Frequency[i] == added_1000MHz_value):
            count_1000_MHz = count_1000_MHz + 1
            cond_1000_MHz = 1
            break
        count_1000_MHz = count_1000_MHz + 1
        i = i + 1
    
    #calculating the amplitude response per 10MHz
    i = index_interested_start_frequency
    amplitude_response_10MHz = []
    temp_10MHz = []
    while (i < index_interested_stop_frequency + 1) and (cond_10_MHz == 1):
        temp_10MHz.append(Gain[i])
        if (len(temp_10MHz) == count_10_MHz):
            amplitude_response_10MHz.append(max(temp_10MHz) - min(temp_10MHz))
            del temp_10MHz[0]
        i = i + 1

##    if (cond_10_MHz == 1):
####        print amplitude_response_10MHz

    #calculating the amplitude response per 120MHz
    i = index_interested_start_frequency
    amplitude_response_120MHz = []
    temp_120MHz = []
    while (i < index_interested_stop_frequency + 1) and (cond_120_MHz == 1):
        temp_120MHz.append(Gain[i])
        if (len(temp_120MHz) == count_120_MHz):
            amplitude_response_120MHz.append(max(temp_120MHz) - min(temp_120MHz))
            del temp_120MHz[0]
        i = i + 1

##    if (cond_120_MHz == 1):
####        print amplitude_response_120MHz

    #calculating the amplitude response per 500MHz
    i = index_interested_start_frequency
    amplitude_response_500MHz = []
    temp_500MHz = []
    while (i < index_interested_stop_frequency + 1) and (cond_500_MHz == 1):
        temp_500MHz.append(Gain[i])
        if (len(temp_500MHz) == count_500_MHz):
            amplitude_response_500MHz.append(max(temp_500MHz) - min(temp_500MHz))
            del temp_500MHz[0]
        i = i + 1

##    if (cond_500_MHz == 1):
####        print amplitude_response_500MHz

    #calculating the amplitude response per 500MHz
    i = index_interested_start_frequency
    amplitude_response_1000MHz = []
    temp_1000MHz = []
    while (i < index_interested_stop_frequency + 1) and (cond_1000_MHz == 1):
        temp_1000MHz.append(Gain[i])
        if (len(temp_1000MHz) == count_1000_MHz):
            amplitude_response_1000MHz.append(max(temp_1000MHz) - min(temp_1000MHz))
            del temp_1000MHz[0]
        i = i + 1

##    if (cond_1000_MHz == 1):
####        print amplitude_response_1000MHz
    
    #calculating the amplitude response max per 10MHz
    if (cond_10_MHz == 1):
        amplitude_response_10MHz_max = (max(amplitude_response_10MHz)) / float(2)
        print ("Per 10MHz: %f" %amplitude_response_10MHz_max)

    #calculating the amplitude response max per 120MHz
    if (cond_120_MHz == 1):
        amplitude_response_120MHz_max = (max(amplitude_response_120MHz)) / float(2)
        print ("Per 120MHz: %f" %amplitude_response_120MHz_max)

    #calculating the amplitude response max per 500MHz
    if (cond_500_MHz == 1):
        amplitude_response_500MHz_max = (max(amplitude_response_500MHz)) / float(2)
        print ("Per 500 MHz: %f" %amplitude_response_500MHz_max)

    #calculating the amplitude response max per 500MHz
    if (cond_1000_MHz == 1):
        amplitude_response_1000MHz_max = (max(amplitude_response_1000MHz)) / float(2)
        print ("Full Band: %f" %amplitude_response_1000MHz_max)
        
    #calculating the max ripple 10MHz
    if (cond_10_MHz == 1):
        max_ripple_10MHz = amplitude_response_10MHz_max
        print ("Max 10 MHz Ripple: %f" %max_ripple_10MHz)

    #fetching the before and after loss compensation temperature settings
    before_temp_value = E4440A.query(':SENSe:NFIGure:CORRection:TEMPerature:BEFore?')
##    print float(before_temp_value)
    after_temp_value = E4440A.query(':SENSe:NFIGure:CORRection:TEMPerature:AFTer?')
##    print float(after_temp_value)
    
    Screenshot(productNumber,SA,tag,bname)

    E4440A.close()
    rm.close()

    #the cond values are just to check if the frequency covers 10MHz, 120MHz, 500MHz, and 1000MHz.
    if (cond_120_MHz == 0):
        amplitude_response_120MHz = ""
        amplitude_response_120MHz_max = ""
    if (cond_500_MHz == 0):
        amplitude_response_500MHz = ""
        amplitude_response_500MHz_max = ""
    if (cond_1000_MHz == 0):
        amplitude_response_1000MHz = ""
        amplitude_response_1000MHz_max = ""
    ## Uniform values array
    values = [Frequency,loss_before_value,loss_after_table,NF,Gain,averageNF,averageGain,amplitude_response_10MHz,amplitude_response_120MHz,amplitude_response_500MHz,amplitude_response_1000MHz,amplitude_response_10MHz_max,amplitude_response_120MHz_max,amplitude_response_500MHz_max,amplitude_response_1000MHz_max,max_ripple_10MHz,before_temp_value,after_temp_value,min_NF,max_NF,min_Gain,max_Gain]


        
    return values

def get_p1db(settings_data,SG,PM):
    print ("starting p1db test")

    ##setting P1dB Path
    [ChannelStatesOn] = kt.run_sequence('P1dB Path')
##    print int(ChannelStatesOn)
    

    #LoFreq is the LO frequency
    LoFreq = float(settings_data['p1db_LO_Frequency'])
    
    rm = visa.ResourceManager()
    E8257D = rm.open_resource('%s' %SG)
    U2001H = rm.open_resource('%s' %PM)
    E8257D.timeout = 50000
    U2001H.timeout = 50000

    #setting the signal generator frequency
    E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['p1db_signal_generator_frequency'])))

    #calculating the IF frequency
    powerMeterFreq = float(settings_data['p1db_signal_generator_frequency']) - LoFreq

    #triggering the power meter to take measurements continuosly 
    U2001H.write(':INITiate1:CONTinuous %d' % (1))

    #setting the power meter frequency
    U2001H.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    
    #p1db_cableLoss is the loss of the cable that connects E8257D and the waveguide adapter on the LNB or the RF switch at the specific RF frequency
    p1db_cableLoss = float(settings_data['p1db_cable_loss'])
    #p1db_muxTLoss_and_cables is the loss of the muxT, to LNB cable, and to RX/modem cable at the specific IF frequency
    p1db_muxTLoss_and_cables = float(settings_data['p1db_muxT_and_cable_loss'])

    #setting the parameters to calculate p1db
    condition = 1
    #the first power amplitude setting for the signal generator
    powerIn1st = float(settings_data['p1db_signal_generator_power_in_sweep_from'])
    powerIn = powerIn1st
    powerSweepTo = float(settings_data['p1db_signal_generator_power_in_sweep_to'])
    powerOut = []
    powersDiff = []
    i = 0
    column = 0
    row = 1
    
    powerSignalSetting = powerIn + p1db_cableLoss
    E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerSignalSetting))
    #turning RF ON
    E8257D.write(':OUTPut:STATe %d' % (1))
    time.sleep(1)
    #powerSweepTo is because we want to sweep from the value of powerIn to powerSweepTo
    while powerIn <= powerSweepTo:
        #powerSignalSetting is the amplitude to be set on E8257D. We compensate for the cable loss
        powerSignalSetting = powerIn + p1db_cableLoss
        #setting the signal generator amplitude to powerSignalSetting
        E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerSignalSetting))
        #turning RF ON
        E8257D.write(':OUTPut:STATe %d' % (1))
        time.sleep(0.2)
        #fetching the measurement and adding it to the powerOut array
        powerOut.append(float(U2001H.query(':FETCh1?')))


        #some math to find the difference between powers so we can use it to find p1db
        if powerIn == powerIn1st:
            powerOut1st = powerOut[0]
        powerInDiff = float(powerIn) - float(powerIn1st)
        powerOutDiff = float(powerOut[i]) - float(powerOut1st)
        powersDiff.append(float(powerInDiff)  - float(powerOutDiff))

        #finding p1db
        if (powersDiff[i] > 1) and (condition == 1):
            condition = 0
            #using interpolation to find the p1db
            interpolate = float(((1-powersDiff[i-1])*(powerOut[i]-powerOut[i-1]))+powerOut[i-1])
            #p1db measurement: We compensate for the muxT and cables from the lnb through the muxT and to the modem
            p1db = float(interpolate) + p1db_muxTLoss_and_cables
            print ("P1dB: %f" %float(p1db))

        i = i + 1
        row = row + 1
        powerIn = powerIn + 1
    #######
##    print "powersOut"
##    i = 0
##    while i < len(powerOut):
##        print powerOut[i]
##        i = i + 1
##        
##    print "powersDiff"
##    i = 0
##    while i < len(powersDiff):
##        print powersDiff[i]
##        i = i + 1
    #turning RF OFF
    E8257D.write(':OUTPut:STATe %d' % (0))

    E8257D.close()
    U2001H.close()
    rm.close()
    return float(p1db)

def get_phase(settings_data,productNumber,SG,SA,bname):
    #productNumber is entered by the user
    print ("starting phase noise test")
    tag = " Phase Noise.GIF"
    ##setting NFG Path
    [ChannelStatesOn] = kt.run_sequence('IPLS Path')
##    print int(ChannelStatesOn)

    rm = visa.ResourceManager()
    E4440A = rm.open_resource('%s' %SA)
    E8257D = rm.open_resource('%s' %SG)

    E8257D.timeout = 50000
    E4440A.timeout = 5000000

    #loading state file
    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['phase_noise_state_file'])+"'")
    #setting the signal generator frequency
    E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['phase_noise_signal_generator_frequency'])))
    powerSignalSetting = float(settings_data['phase_noise_signal_generator_power_in'])

    #setting the signal generator amplitude to powerSignalSetting
    E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerSignalSetting))
    #turning RF ON
    E8257D.write(':OUTPut:STATe %d' % (1))

    #setting the command to use marker to read the phase noise values
    commands = [
                'CALC:LPLot:MARK1:X 10 hz',
                'CALC:LPLot:MARK1:Y?',
                'CALC:LPLot:MARK1:X 100 hz',
                'CALC:LPLot:MARK1:Y?',
                'CALC:LPLot:MARK1:X 1 khz',
                'CALC:LPLot:MARK1:Y?',
                'CALC:LPLot:MARK1:X 10 khz',
                'CALC:LPLot:MARK1:Y?',
                'CALC:LPLot:MARK1:X 100 khz',
                'CALC:LPLot:MARK1:Y?',
                'CALC:LPLot:MARK1:X 1000 khz',
                'CALC:LPLot:MARK1:Y?']

    
	
    phase = []
    #Condition to quickly restart phase noise tests
    while(1):
    #trigger the phase noise sweep
        E4440A.query(':READ:LPLot1?')
        dummy = input('Retry phase noise test? [Y/N]')
        if(dummy != 'Y'):
            break
        print('Restarting phase noise test...\n')

    print('Proceeding to gather phase noise data...')
    #activating marker 1
    E4440A.write(':CALCulate:LPLot:MARKer1:MODE POSition')
    #setting marker one to trace 2
    E4440A.write('CALC:LPLot:MARK1:TRAC 2')
    
    #reading the marker values and saving it to phase array
    for i in range(0,len(commands)):
        time.sleep(0.5)
        if(commands[i].find('CALC:LPLot:MARK1:Y?')>-1):
            rs = E4440A.query(commands[i])
            phase.append(float(rs))
##            print float(rs)
        else:
            rs = E4440A.write(commands[i])

    #turning off marker
    E4440A.write(':CALCulate:LPLot:MARKer1:MODE OFF')

    Screenshot(productNumber,SA,tag,bname)

    #turning RF OFF
    E8257D.write(':OUTPut:STATe %d' % (0))
    print ("10 Hz: %f" %phase[0])
    print ("100 Hz: %f" %phase[1])
    print ("1 kHz: %f" %phase[2])
    print ("10 kHz: %f" %phase[3])
    print ("100 kHz: %f" %phase[4])
    print ("1 MHz: %f" %phase[5])

    E8257D.close()
    E4440A.close()
    rm.close()
    return phase

def get_image_rejection(settings_data,gainAverage,productNumber,SG,SA,bname):
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting image rejection test")
    tag = " Image Rejection.GIF"
    ##image_rejection_cable_loss is the loss of the cable that connects E8257D and the waveguide adapter on the LNB or the RF switch at the specific RF Frequency
    image_rejection_cable_loss = float(settings_data['image_rejection_cable_loss'])
    #zeroDbTolerance is the tolerance used to check if the marker value of E4440A is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    rm = visa.ResourceManager()
    E8257D = rm.open_resource('%s' %SG)
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 500000
    E8257D.timeout = 500000

    #loading state file
    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['image_rejection_state_file'])+"'")
    #waiting for the state file to load
    time.sleep(20)

    #setting the signal generator frequency
    E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['image_rejection_signal_generator_frequency_for_verification'])))
    #powerIn is the amplitude to be set on E8257D. We use the gain average and compensate for the cable loss 
    powerIn = (gainAverage - image_rejection_cable_loss) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
    #turning RF ON
    E8257D.write(':OUTPut:STATe %d' % (1))
    #restrating the sweep
    E4440A.write(':INITiate:IMMediate')

    #waiting for the sweep to complete and then read the maximum marker value
    sweepTime = float(E4440A.query(':SENSe:SWEep:TIME?')) + 6
    time.sleep(sweepTime)
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    measurment = float(E4440A.query(':CALCulate:MARKer1:Y?'))      
    #print('Is 0dBm?: ' + str(measurment))

    #verifying if the measurement is close to 0dB
    if((measurment > zeroDbTolerance) or (measurment < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurment
        #print('Current PowerIn: ' + str(powerIn))
        powerIn = powerIn - difference
        #print('New PowerIn' + str(powerIn))
        E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
        E4440A.write(':INITiate:IMMediate')
        time.sleep(sweepTime)
        E4440A.write(':CALCulate:MARKer1:MAXimum')
        measurment = float(E4440A.query(':CALCulate:MARKer1:Y?'))
        #print('Is 0dBm?: ' + str(measurment))

        #If the measurment is still off, finely tune:
        while((measurment > zeroDbTolerance) or (measurment < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurment > zeroDbTolerance):
                  powerIn = powerIn - 1
            if(measurment < zeroDbTolerance):
                  powerIn = powerIn + 1
            E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
            E8257D.write(':OUTPut:STATe %d' % (1))
            E4440A.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            E4440A.write(':CALCulate:MARKer1:MAXimum')
            measurment = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            #print('Is 0dBm?: ' + str(measurment))
            #print('Power: ' + str(powerIn))
              


    #setting the frequency on the signal generator to image rejection frequency to get the image rejection value
    E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['image_rejection_signal_generator_frequency_for_image_rejection'])))
    E4440A.write(':INITiate:IMMediate')
    time.sleep(sweepTime)
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    imageRejection = float(E4440A.query(':CALCulate:MARKer1:Y?'))
    print ("Image Rejection: %f" %imageRejection)

    Screenshot(productNumber,SA,tag,bname)

    E4440A.close()
    E8257D.close()
    rm.close()
    return imageRejection

def get_spurs(settings_data,gainAverage,productNumber,SG,SA,bname):
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting in band spur test")
    zeroDbTolerance = 1
    #value = ""
    ##in_band_spur_cable_loss is the loss of the cable that connects E8257D and the waveguide adapter on the LNB or the RF switch at the specific RF frequency
    in_band_spur_cable_loss = float(settings_data['in_band_spur_cable_loss'])
    tag = " In Band Spurs.GIF"
    rm = visa.ResourceManager()

    E8257D = rm.open_resource('%s' %SG)
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 5000000
    E8257D.timeout = 5000000
    
    #loading state file
    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['in_band_spur_state_file'])+"'")
    #waiting for 20 seconds for the state file to load
    time.sleep(20)
    #loading limit line
    E4440A.write(':MMEMory:LOAD:LIMit %s,"%s"' % ('LLINE1', str(settings_data['in_band_spur_limit_1_file'])))
    E4440A.write(':CALCulate:LLINe1:DISPlay %d' % (1))
    E4440A.write(':CALCulate:LLINe1:STATe %d' % (1))
    #setting the signal generator frequency
    E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['in_band_spur_signal_generator_frequency'])))
    #powerIn is the amplitude to be set on E8257D. We use the gain average and compensate for the cable loss
    powerIn = (gainAverage - in_band_spur_cable_loss)* - 1
    #setting the signal generator amplitude to powerIn
    E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
    #turning RF ON
    E8257D.write(':OUTPut:STATe %d' % (1))
    #Restarting the sweep
    E4440A.write(':INITiate:IMMediate')
    #Turning limit line display and state on
    E4440A.write(':CALCulate:LLINe:DISPlay %d' % (1))
    E4440A.write(':CALCulate:LLINe1:STATe %d' % (1))

    sweepTime = float(E4440A.query(':SENSe:SWEep:TIME?'))

    time.sleep(sweepTime * 1.5)
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    measurement = float(E4440A.query(':CALCulate:MARKer1:Y?'))
    print(measurement)


    #verifying if the measurement is close to 0dB and at tone frequency
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        #print('Current PowerIn: ' + str(powerIn))
        powerIn = powerIn - difference
        #print('New PowerIn' + str(powerIn))
        E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
        E4440A.write(':INITiate:IMMediate')
        time.sleep(2)
        E4440A.write(':CALCulate:MARKer1:MAXimum')
        measurement = float(E4440A.query(':CALCulate:MARKer1:Y?'))
        #print('Is 0dBm?: ' + str(measurment))

        #If the measurment is still off, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn = powerIn - 1
            if(measurement < zeroDbTolerance):
                  powerIn = powerIn + 1
            E8257D.write(':SOURce:POWer:LEVel:IMMediate:AMPLitude %G' % (powerIn))
            E8257D.write(':OUTPut:STATe %d' % (1))
            E4440A.write(':INITiate:IMMediate')
            time.sleep(2)
            E4440A.write(':CALCulate:MARKer1:MAXimum')
            measurement = float(E4440A.query(':CALCulate:MARKer1:Y?'))
            #print('Is 0dBm?: ' + str(measurment))
            #print('Power: ' + str(powerIn))

    #check if the limit line passed or failed. if the results is 0, it means pass. if the results is 1, it means fail
    measurement = float(E4440A.query(':CALCulate:LLINe:FAIL?'))

##  print float(measurement)
    if (float(measurement) == 0):
        value = "Pass"
    else:          
        value = "Fail"
        
    Screenshot(productNumber,SA,tag,bname)
    
    #turning RF OFF
    E8257D.write(':OUTPut:STATe %d' % (0))

    E4440A.close()
    E8257D.close()
    rm.close()
    print(value)
    return value

def get_lo_leakage_output(settings_data,productNumber,SG,SA,bname):
    #product number is entered by the user
    print ("starting LO leakage output test")
    #lo_leakage_output_muxT_and_cable_loss is the loss of the muxT, to LNB cable, and to RX/modem cable at the LO Frequency
    lo_leakage_output_muxT_and_cable_loss = float(settings_data['lo_leakage_output_muxT_and_cable_loss'])
    tag = " LO Leakage Output.GIF"
    rm = visa.ResourceManager()
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 50000
    
    #loading state file
    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['lo_leakage_output_state_file'])+"'")
    #waiting 20 seconds for the state file to load
    time.sleep(20)

    #restarting the sweep
    E4440A.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = E4440A.query(':SENSe:SWEep:TIME?')
    sweepTime = float(sweepTime) + 1.6
    time.sleep(sweepTime)

    #setting marker1 to maximum
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    #getting the result of marker1
    result = E4440A.query(':CALCulate:MARKer1:Y?')
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageOut = float(result) + lo_leakage_output_muxT_and_cable_loss

    print ("LO Leakage Out: %f" %float(LoLeakageOut))

    Screenshot(productNumber,SA,tag,bname)   
        
    E4440A.close()
    rm.close()
    
    return float(LoLeakageOut)

def get_lo_leakage_input(settings_data,productNumber,SG,SA,bname):
    #product number is entered by the user
    print ("starting LO leakage input test")
    tag = " LO Leakage Input.GIF"
    ##setting NFG Path
    [ChannelStatesOn] = kt.run_sequence('Input Leak Path')
##    print int(ChannelStatesOn)
    
    #cables_loss_at_lo_frequency is the loss of cable that connects the unit to the spectrum analyzer (E4440A) at LO frequency
    cables_loss_at_lo_frequency = float(settings_data['lo_leakage_input_cable_loss'])

    rm = visa.ResourceManager()
    E4440A = rm.open_resource('%s' %SA)

    E4440A.timeout = 50000
    
##    #loading state file
##    E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['lo_leakage_input_state_file'])+"'")
##    #waiting 20 seconds for the state file to load
##    time.sleep(20)
    time.sleep(1)
    #restarting the sweep
    E4440A.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = E4440A.query(':SENSe:SWEep:TIME?')
    sweepTime = float(sweepTime) + 1.6
    time.sleep(sweepTime)

    #setting marker1 to maximum
    E4440A.write(':CALCulate:MARKer1:MAXimum')
    #getting the result of marker1
    result = E4440A.query(':CALCulate:MARKer1:Y?')
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageIn = float(result) + cables_loss_at_lo_frequency

    print ("LO Leakage In: %f" %float(LoLeakageIn))

    Screenshot(productNumber,SA,tag,bname)   
        
    E4440A.close()
    rm.close()
    
    return float(LoLeakageIn)

def get_current(settings_data,PS):
    print("starting current test")
    rm = visa.ResourceManager()
    U3606B = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
    time.sleep(1)
    U3606B.write(':OUTPut:STATe %d' %(1))
    time.sleep(2)
    
    #getting the dc current measurement
    temp_values = U3606B.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')
    dcCurrent = temp_values[0]

    #converting the measurement reading from A to mA
    dcCurrent = dcCurrent*float(1000)
    print ("Current: %f" %abs(dcCurrent))
    
    U3606B.close()
    rm.close()
    return abs(dcCurrent)

def get_vswrIP(productNumber,settings_data,bname):
    print ("starting vswr_S11 test")
    tag = " S11.PNG"
    ##setting NFG Path
    [ChannelStatesOn] = kt.run_sequence('S11 Path')
##    print int(ChannelStatesOn)

    path = os.path.dirname(__file__) + "\\VNAstates\\" + str(settings_data['S11State'])
                                        
    rm = visa.ResourceManager()
    M9375A = rm.open_resource('TCPIP0::M9037A-45473669::hislip0::INSTR')

    M9375A.timeout = 500000
    M9375A.write("MMEM:LOAD '%s'"%path )
    time.sleep(1)
    M9375A.write("OUTP 1")
    time.sleep(1.5)
##    #activating marker 1
##    N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
    M9375A.write("CALC1:PAR:SEL CH1_S11_1")
    #setting marker 1 to max
    M9375A.write('CALC1:MARK1:FUNC:EXEC MAX')
    #fetching marker 1 value
    temp_values = M9375A.query_ascii_values('CALC1:MARK1:Y?')
    S11 = temp_values[0]
    print ("S11: %f" %S11)


    #setting the format to save a screenshot
    M9375A.values_format.is_binary = True
    M9375A.values_format.datatype = 'B'
    M9375A.values_format.is_big_endian = False
    M9375A.values_format.container = bytearray
    
    #saving a screenshot on the equipment
    M9375A.write(":MMEM:STOR:SSCR 'C:\\TEST.BMP\'")# 'C:\\TEST.JPG\'")
    #fetching the image data in an array
    fetch_image_data = M9375A.query_values("MMEM:TRAN? 'C:\\TEST.BMP\'")

    #creating a file with the product number to write the image data to it
    save_dir = open(productNumber+ tag, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

##    #deleting the image on the equipment
    M9375A.write(":MMEM:DEL 'C:\\TEST.BMP\'")
    M9375A.write("*CLS")
    M9375A.close()

    ## Moving Screenshot
    file_name = str(productNumber)+ tag
    path = os.path.dirname(__file__)

    filed = path + "\\" + file_name

    target = path + "\\" + bname
    ## Moves Screenshot to Folder
    shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))

    
    rm.close()
    return S11

def get_vswrOP(productNumber,PS,settings_data,bname): ## Directory Based Recall Done
    print ("starting vswr_S22 test")

    tag = " S22.PNG"
    path = os.path.dirname(__file__) + "\\VNAstates\\" + str(settings_data['S22State'])
    
    
    
    ##setting NFG Path
    [ChannelStatesOn] = kt.run_sequence('S22 Path')
##    print int(ChannelStatesOn)
    
                                        
    rm = visa.ResourceManager()
    M9375A = rm.open_resource('TCPIP0::M9037A-45473669::hislip0::INSTR')
    U3606B = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    
    
    M9375A.timeout = 500000
    M9375A.write("MMEM:LOAD '%s'"%path )
    
    time.sleep(1)
    M9375A.write("OUTP 1")
    time.sleep(1.5)

##    #activating marker 1
##    N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
    M9375A.write("CALC1:PAR:SEL CH1_S22_1")
    #setting marker 1 to max
    M9375A.write('CALC1:MARK1:FUNC:EXEC MAX')
    #fetching marker 1 value
    temp_values = M9375A.query_ascii_values('CALC1:MARK1:Y?')
    S22 = temp_values[0]
    print ("S22: %f" %S22)


    #setting the format to save a screenshot
    M9375A.values_format.is_binary = True
    M9375A.values_format.datatype = 'B'
    M9375A.values_format.is_big_endian = False
    M9375A.values_format.container = bytearray
    
    #saving a screenshot on the equipment
    M9375A.write(":MMEM:STOR:SSCR 'C:\\TEST.BMP\'")# 'C:\\TEST.JPG\'")
    #fetching the image data in an array
    fetch_image_data = M9375A.query_values("MMEM:TRAN? 'C:\\TEST.BMP\'")

    #creating a file with the product number to write the image data to it
    save_dir = open(productNumber+ tag, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

##    #deleting the image on the equipment
    M9375A.write(":MMEM:DEL 'C:\\TEST.BMP\'")
    M9375A.write("*CLS")
    M9375A.write("OUTP 0")
##    U3606B.write(':OUTPut:STATe %d' %(0))
    M9375A.close()
    U3606B.close()

    ## Moving Screenshot
    file_name = str(productNumber)+ tag
    path = os.path.dirname(__file__)

    filed = path + "\\" + file_name

    target = path + "\\" + bname
    ## Moves Screenshot to Folder
    shutil.move(os.path.join(path,file_name),os.path.join(target,file_name))
    
    rm.close()
    return S22


def V_Log(settings_data,PS,SG,SA):
    BCheck = float(settings_data['Voltage']) ## Set LNB to testing voltage
    Vsafe = float(settings_data['V_Max'])
    Vol1 = BCheck
    T1 = 0
    T2 = 0 
    Dif = 0
    count = 0
    NoLoop = 0
    Vsw = [] ## Array to return switching values, Vsw[0] is Up value, Vsw[1] is Down value
    
    try:
        rm = visa.ResourceManager()
        U3606B = rm.open_resource('%s' %PS)
        U3606B.timeout = 50000
        E8257D = rm.open_resource('%s' %SG)
        E8257D.timeout = 50000
        E4440A = rm.open_resource('%s' %SA)
        E4440A.timeout = 5000000


        
        Dif = abs(float(T2)-float(T1))
        E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['image_rejection_state_file'])+"'") ## Loads Image Rej state file
        time.sleep(20)
        E4440A.write(':SENSe:BAND 1000 ') ## Change Res BW for faster sweeps and faster reads
        time.sleep(2.5)
        E8257D.write(':SOURce:FREQuency:FIXed %G' % (float(settings_data['image_rejection_signal_generator_frequency_for_verification'])))
        E8257D.write(':OUTPut:STATe %d' % (1))
        time.sleep(2.5)

        print("Beginning voltage up test")


        ### Voltage switch Up Test
        E4440A.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(0.5)
        T2 = E4440A.query(':CALCulate:MARKer1:Y?')
        while((abs(float(Dif)) <= float(10)) & (NoLoop == 0)):
            if (Vol1 < float(Vsafe)):
                U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(Vol1))
                E4440A.write(':CALCulate:MARKer1:MAXimum')
                time.sleep(0.5)
                T1 = E4440A.query(':CALCulate:MARKer1:Y?')
                Vol1 = float(Vol1) + 0.1
                Dif = abs(float(T1)-float(T2))
                T2 = T1
                time.sleep(0.5)

            else:
                print("Voltage Over Max Safe Voltage")
                U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
                NoLoop = 1
        if(NoLoop == 0):
            Vol1 = float(Vol1) - 0.1
            print("Switching Voltage at: %f" %Vol1)
            U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
            E8257D.write(':OUTPut:STATe %d' % (0))
            Vsw.append(Vol1)
        else:
            print("Could not find voltage switching threshold and went over safe voltage threshold")
            print("Check unit to confirm automatic voltage switching is functional")
            print('\n')
            Vsw.append("null")

        Vol1 = BCheck
        Vsafe = float(settings_data['V_Min'])
        Dif = 0
        NoLoop = 0
        E8257D.write(':OUTPut:STATe %d' % (1))
        time.sleep(2.5)
        E4440A.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(0.5)
        T2 = E4440A.query(':CALCulate:MARKer1:Y?')
        print("Beginning voltage down test")
        time.sleep(2.5)

        ### Voltage switch Down Test
        while((abs(float(Dif)) <= float(20)) & (NoLoop == 0)):
            if (Vol1 > float(Vsafe)):
                U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(Vol1))
                E4440A.write(':CALCulate:MARKer1:MAXimum')
                time.sleep(0.5)
                T1 = E4440A.query(':CALCulate:MARKer1:Y?')
                Vol1 = float(Vol1) - 0.1
                Dif = abs(float(T1)-float(T2))
                T2 = T1
                time.sleep(0.5)
                
                

            else:
                print("Voltage Below Safe Voltage")
                U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
                NoLoop = 1
        if(NoLoop == 0):
            Vol1 = float(Vol1) + 0.1
            print("Switching Voltage at: %f" %Vol1)
            U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
            E8257D.write(':OUTPut:STATe %d' % (0))
            Vsw.append(Vol1)
        else:
            print("Could not find voltage switching threshold and went over safe voltage threshold")
            print("Check unit to confirm automatic voltage switching is functional")
            print('\n')
            Vsw.append("null")

        return Vsw
    except:
        Vsw.append("null - Error Occurd")
        Vsw.append("null - Error Occurd")
        print('\n')
        print("An error was detected during Voltage switching test")
        print('\n')
    
        E8257D.close()
        E4440A.close()
        U3606B.close()
        rm.close()

def EQID(PS,SG,SA,PM):
        ID = ["SA","PS","SG","PM"]
        rm = visa.ResourceManager()
        U3606B = rm.open_resource('%s' %PS)
        U3606B.timeout = 5000
        E8257D = rm.open_resource('%s' %SG)
        E8257D.timeout = 50000
        E4440A = rm.open_resource('%s' %SA)
        E4440A.timeout = 5000000
        U2001H = rm.open_resource('%s' %PM)

        print("Connecting to SA...")
        ID[0] = E4440A.query('*IDN?')
        print("Connected to SA")
        print("Connecting to PS...")
        ID[1] = U3606B.query('*IDN?')
        print("Connected to Power Supply")
        print("Connecting to SG...")
        ID[2] = E8257D.query('*IDN?')
        print("Connected to Signal Gen")
        print("Connecting to PM...")
        ID[3] = U2001H.query('*IDN?')
        print("Connected to Power Meter")

        return ID
def Update_att():
    ## Function is to update the states of the Text Boxes and drop down menus
    r485_en = RS485_EN.get()
    r485_custom = RS485_custom.get()
    if(r485_en == 1):
        USB_Port_Text['state'] = 'normal'
        if(popupMenu1.get() != 'None'):
            A_V1['state'] = 'NORMAL'
        if(popupMenu2.get() != 'None'):
            A_V2['state'] = 'NORMAL'
        if(popupMenu3.get() != 'None'):
            A_V3['state'] = 'NORMAL'
        if(r485_custom == 1):
            if(popupMenu1.get() != 'None'):
                C_Att_Text_B1['state'] = 'normal'
                C_Band_Text_B1['state'] = 'normal'
            if(popupMenu2.get() != 'None'):
                C_Att_Text_B2['state'] = 'normal'
                C_Band_Text_B2['state'] = 'normal'
            if(popupMenu3.get() != 'None'):
                C_Att_Text_B3['state'] = 'normal'
                C_Band_Text_B3['state'] = 'normal'
        else:
            C_Att_Text_B1['state'] = tk.DISABLED
            C_Att_Text_B2['state'] = tk.DISABLED
            C_Att_Text_B3['state'] = tk.DISABLED
            C_Band_Text_B1['state'] = tk.DISABLED
            C_Band_Text_B2['state'] = tk.DISABLED
            C_Band_Text_B3['state'] = tk.DISABLED
    else:
        A_V1['state'] = tk.DISABLED
        A_V2['state'] = tk.DISABLED
        A_V3['state'] = tk.DISABLED
        C_Att_Text_B1['state'] = tk.DISABLED
        C_Att_Text_B2['state'] = tk.DISABLED
        C_Att_Text_B3['state'] = tk.DISABLED
        C_Band_Text_B1['state'] = tk.DISABLED
        C_Band_Text_B2['state'] = tk.DISABLED
        C_Band_Text_B3['state'] = tk.DISABLED
        USB_Port_Text['state'] = tk.DISABLED



def RS485_Init(port):
    ## set controller 
    k = Controller()
    ## Open RS485 Tool
    p = Popen(["RS485.exe"])
    time.sleep(1)
    k.type("connect %s" %port)
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,2,0,*")
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,1,3,*")
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("disc")
    k.press(Key.enter)
    p.terminate()

def RS485_SW(port,band,gain):
    ## set controller 
    k = Controller()
    ## Open RS485 Tool
    p = Popen(["RS485.exe"])

    time.sleep(1)
    k.type("connect %s" %port)
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,2,0,*")
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,1,3,*")
    k.press(Key.enter)
    time.sleep(0.5)
    ## Changing bands
    k.type("s $setst,%s,0,*" %band)
    k.press(Key.enter)
    time.sleep(0.5)
    ##Changing Gain
    k.type("s $setda,%s,%s,*" %(str(band-1),gain))
    k.press(Key.enter)
    time.sleep(0.5)
    ##disconnecting and closing
    k.type("disc")
    k.press(Key.enter)
    p.terminate()
    
def RS485_Commands(ID):

    ## Changing att values from a float to the corresponding hex value
    B1_att = float(A_V1.get())
    B2_att = float(A_V2.get())
    B3_att = float(A_V3.get())
    att_hex.clear()
    att_hex.append(hex(abs(int((B1_att - 31.5)*2))))
    att_hex.append(hex(abs(int((B2_att - 31.5)*2))))
    att_hex.append(hex(abs(int((B3_att - 31.5)*2))))
    print (att_hex)

    ##Check to see which bands are enabled and send the commands
    if (ID == 1):
        if(popupMenu1.get() != 'None'):
            ## If custom commands are enabled, send these commands instead of the standard
            if(RS485_custom.get() == 1):
                pt = USB_Port_Text.get()
                bd_485 = C_Att_Text_B1.get()
                att_485 = C_Band_Text_B1.get()
                RS485_SW(pt,bd_485,att_485)
            else: ## If custom commands are disabled
                pt = USB_Port_Text.get()
                bd_485 = 1
                att_485 = att_hex[0]
                RS485_SW(pt,bd_485,att_485)

    if(ID == 2):
        if(popupMenu2.get() != 'None'):
            ## If custom commands are enabled, send these commands instead of the standard
            if(RS485_custom.get() == 1):
                pt = USB_Port_Text.get()
                bd_485 = C_Att_Text_B2.get()
                att_485 = C_Band_Text_B2.get()
                RS485_SW(pt,bd_485,att_485)
            else: ## If custom commands are disabled
                pt = USB_Port_Text.get()
                bd_485 = 2
                att_485 = att_hex[1]
                RS485_SW(pt,bd_485,att_485)

    if(ID == 3):
        if(popupMenu3.get() != 'None'):
            ## If custom commands are enabled, send these commands instead of the standard
            if(RS485_custom.get() == 1):
                pt = USB_Port_Text.get()
                bd_485 = C_Att_Text_B3.get()
                att_485 = C_Band_Text_B3.get()
                RS485_SW(pt,bd_485,att_485)
            else: ## If custom commands are disabled
                pt = USB_Port_Text.get()
                bd_485 = 3
                att_485 = att_hex[2]
                RS485_SW(pt,bd_485,att_485)
        
def Quit(): 
    Pwr_off(PS)
    raise ValueError  

master = tk.Tk() ## Initialize Menu variable
master.title("ATE Program")

##Variable List
band2 = IntVar()
band3 = IntVar()
choices = ['None']
atten = []
att_hex = []   
atten = np.arange(31.5,-0.5,-0.5)
atten = atten.tolist()
V_SW = IntVar()
CC_SW = IntVar()
Temp = IntVar()
RS485_EN = IntVar()
RS485_custom = IntVar()
path = os.path.dirname(__file__)
mypath = path + '\Set_Files'

###### Drop Down Menu for Bands ######
Bnd = LabelFrame(master, text="Settings File Selection, leave at 'None' if band does not exist")
Bnd.grid(row=1,column=1,sticky=W)


for (dirpath, dirnames, filenames) in walk(mypath):
    choices.extend(filenames)
    break

## Band 1 Settings File
Bnd1Name = Label(Bnd,text="Band 1 Settings File:").grid(row=2,column=1,sticky=W)

## Drop Down Menu 1
popupMenu1 = ttk.Combobox(Bnd, value=choices) ##Makes the menu
popupMenu1.set('None') ## Set Default Value
popupMenu1.grid(row = 2, column =2,sticky=W,ipadx = 100) ## Positions the Menu


## Band 2 Settings File
Bnd2Name = Label(Bnd,text="Band 2 Settings File:").grid(row=3,column=1,sticky=W)

## Drop Down Menu 2
popupMenu2 = ttk.Combobox(Bnd, value=choices) ##Makes the menu
popupMenu2.set('None') ## Set Default Value
popupMenu2.grid(row = 3, column =2,sticky=W,ipadx = 100) ## Positions the Menu

## Band 3 Settings File
Bnd2Name = Label(Bnd,text="Band 3 Settings File:").grid(row=4,column=1,sticky=W)

## Drop Down Menu 3
popupMenu3 = ttk.Combobox(Bnd, value=choices) ##Makes the menu
popupMenu3.set('None') ## Set Default Value
popupMenu3.grid(row = 4, column =2,sticky=W,ipadx = 100) ## Positions the Menu
###### End of Drop Down Menu for Bands ######

#Check Box for Voltage Switching###
Checkbutton(Bnd, text = 'Enable Voltage Switch Test', variable = V_SW).grid(row =5, column =1)

Checkbutton(Bnd, text = 'Enable Contact Closure Band Switching', variable = CC_SW).grid(row =6, column =1)


###### Menu Group for RS485 ######
RS485_G = LabelFrame(master, text="Check Box to Enable RS485 Testing")
RS485_G.grid(row=2,column=1)


#Check Box for RS485
Checkbutton(RS485_G, text = 'Enable RS485 Options (Uncheck and Check to refresh)', variable = RS485_EN,command=Update_att).grid(row = 2, column =1,sticky=W)

## USB Port
USB_Port = Label(RS485_G, text = 'Port Number:').grid(row=5, column = 1, sticky = W)
USB_Port_Text = Entry(RS485_G, state = DISABLED)
USB_Port_Text.grid(row = 5, column = 2, sticky = W,ipadx = 2)

DSA = Label(RS485_G, text = 'DSA Control:').grid(row=6, column = 1, sticky = W)

## Combobox for Band 1 attenuation values
AttName1 = Label(RS485_G,text="Band 1 Attenuation Value:").grid(row=7,column=1,sticky=W)
A_V1 = ttk.Combobox(RS485_G, value=atten,state = DISABLED)
A_V1.set('0.5')
A_V1.grid(row = 7,column = 2, sticky=W)

## Combobox for Band 2 attenuation values
AttName2 = Label(RS485_G,text="Band 2 Attenuation Value:").grid(row=8,column=1,sticky=W)
A_V2 = ttk.Combobox(RS485_G, value=atten,state = DISABLED)
A_V2.set('0.5')
A_V2.grid(row = 8,column = 2, sticky=W)

## Combobox for Band 3 attenuation values
AttName3 = Label(RS485_G,text="Band 3 Attenuation Value:").grid(row=9,column=1,sticky=W)
A_V3 = ttk.Combobox(RS485_G, value=atten,state = DISABLED)
A_V3.set('0.5')
A_V3.grid(row = 9,column = 2, sticky=W)

#Check Box for custom RS485 commands
Checkbutton(RS485_G, text = 'Enable custom commands for non-standard RS485 units (Uncheck and Check to refresh)', variable = RS485_custom,command=Update_att).grid(row = 10, column =1,sticky=E)


### Band 1 custom commands ###
C_Att_B1 = Label(RS485_G, text = 'Custom Attenuation command for Band 1:').grid(row=11, column = 1, sticky = W)
C_Att_Text_B1 = Entry(RS485_G, state = DISABLED)
C_Att_Text_B1.grid(row = 11, column = 2, sticky = E,ipadx = 100)

C_Band_B1 = Label(RS485_G, text = 'Custom switching command for Band 1:').grid(row=12, column = 1, sticky = W)
C_Band_Text_B1 = Entry(RS485_G, state = DISABLED)
C_Band_Text_B1.grid(row = 12, column = 2, sticky = E,ipadx = 100)

### Band 2 custom commands ###
C_Att_B2 = Label(RS485_G, text = 'Custom Attenuation command for Band 2:').grid(row=13, column = 1, sticky = W)
C_Att_Text_B2 = Entry(RS485_G,state = DISABLED)
C_Att_Text_B2.grid(row = 13, column = 2, sticky = E,ipadx = 100)

C_Band_B2 = Label(RS485_G, text = 'Custom switching command for Band 2:').grid(row=14, column = 1, sticky = W)
C_Band_Text_B2 = Entry(RS485_G, state = DISABLED)
C_Band_Text_B2.grid(row = 14, column = 2, sticky = E,ipadx = 100)

### Band 3 custom commands ###

C_Att_B3 = Label(RS485_G, text = 'Custom Attenuation command for Band 3:').grid(row=15, column = 1, sticky = W)
C_Att_Text_B3 = Entry(RS485_G,state = DISABLED)
C_Att_Text_B3.grid(row = 15, column = 2, sticky = E,ipadx = 100)

C_Band_B3 = Label(RS485_G, text = 'Custom switching command for Band 3:').grid(row=16, column = 1, sticky = W)
C_Band_Text_B3 = Entry(RS485_G, state = DISABLED)
C_Band_Text_B3.grid(row = 16, column = 2, sticky = E,ipadx = 100)


###### LNB S/N, Stock Number,Manufacturer Number, Tester Name, Customer, PO, and Color ######

## Frame
Info = LabelFrame(master, text="Input required info here, leave blank if not required")
Info.grid(row=3,column=1,sticky=W)


##Serial Number
SN = Label(Info, text = 'Unit S/N:').grid(row=1, column = 1, sticky = W)
SNText = Entry(Info)
SNText.grid(row = 1, column = 2, sticky = E)

## Stock Number
Stock = Label(Info, text = 'Unit Stock Number:').grid(row=1, column = 3, sticky = W)
StockText = Entry(Info)
StockText.grid(row = 1, column = 4, sticky = E)

## Manufacturer S/N if present
Mnf = Label(Info, text = 'Supplier S/N:').grid(row=2, column = 1, sticky = W)
MnfText = Entry(Info)
MnfText.grid(row = 2, column = 2, sticky = E)

## Unit Color
Color = Label(Info, text = 'Unit Color:').grid(row=2, column = 3, sticky = W)
ColorText = Entry(Info)
ColorText.grid(row = 2, column = 4, sticky = E)

## Customer Name
Cx = Label(Info, text = 'Customer:').grid(row=3, column = 1, sticky = W)
CxText = Entry(Info)
CxText.grid(row = 3, column = 2, sticky = E)

## PO Number
PO = Label(Info, text = 'PO Number:').grid(row=3, column = 3, sticky = W)
POText = Entry(Info)
POText.grid(row = 3, column = 4, sticky = E)

## Tester Name
You = Label(Info, text = 'Tester Name:').grid(row=4, column = 1, sticky = W)
YouText = Entry(Info)
YouText.grid(row = 4, column = 2, sticky = E)

###### End of S/N, Stock Number, Manufacturer Number, Tester Name, Customer, PO and Color ######
        
## Button to Begin Testing
b1 = Button(master,text='Begin',command=intermdiate ).grid(row=7,column=3,sticky=E)

## Button to Quit Testing
b2 = Button(master,text='Unlock Without Exit',command=Quit).grid(row=1,column=1,sticky=E)

mainloop()

#Change Log
#V1.7 - Improved temperature sensing for more accurate NFG readings. Added Closed Contact band switching functionality. Better Image Rejection and In Band Spur signal tracking. Revamped Datasheet generation method through DS_GV0_2.py. Added _TESTLOG to accumulate tests results for statistics later
#V1.7.1 - Added phase noise pause to ask user if they'd like to retry the phase noise test (intermittant failures with uD9 Ka units)
#V1.8 - Added limit lines to NFG tests
