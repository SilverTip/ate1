# Python file: Serial_Mapping_Generator.py
# Version: 2.0.0
# Time: 06/28/2023

# Update 2.0.2 : bug fix
# Update 2.0.1 : Interface improvement
# Update 2.0.0 : change the way identify datasheet, add change p1db to script
# Update 1.2.0 : Add print DSA function, update read dir file to path instead of name

# Update: add DRO
# Update: PN FUnction, rewrite read dir function

from datetime import datetime
import re
import shutil
import xlwings as xw
import time
import os
import json
import numpy
import PyPDF2
from PIL import Image

#########################################################################

band_num = 1

enable_editing = True
copy_datasheet = True
backup_dir = True

assign_serial = False
add_pn = False
DRO = False
print_DSA = False
edit_p1db = False

customer = "LITECOMS"
po_num = "12166 Rev C"
color_code = "SAND"

serial_base = "8310"
serial_start = "001"
serial_range = "2"

#########################################################################

if band_num == 1:
    no_band_id = True
else: 
    no_band_id = False

datasheet_dataposition = {
    'serial': 'D3',
    'stock': 'D4',
    'customer': 'B2',
    'po': 'B3',
    'colour': 'C17',
    'nf_max': 'I7',
    'nf_ave': 'I8',
    'gain_ave': 'I9',
    'p1db': 'I15',
    'oip3': 'I16',
    'tester': 'F3',
    'pass_fail': 'F2'
}

serial_pattern = r'\d{4}-\d{3}'
datasheet_pattern = r'^(?!.*Band).*\.xlsx.*$'
log_pattern = r'(.*)(\.log)'
nodatasheet_pattern = r'^(.*)\.xlsx'
pdf_pattern = r'(.*)\.pdf'
image_pattern = r'(.*)\.PNG'
pn_image_pattern = r'\d{4}-\d{3}\.PNG'
pn_pdf_pattern = r'\d{4}-\d{3}\.pdf'
pn_pattern = r'PN'
pn2_pattern = r'report'
pn_data_pattern = r'-\d{2,3}\.\d{3}'
band_pattern = r'(?<=Band)\d{1}'
stock_pattern = r'(?<=\()(.*)(?=\))'
excel_pattern = r'(.*)(\.xlsx)'

now = datetime.now()

def scan_dir():
    datasheet_dir = {}
    datasheet_root= {}
    folder_in_root = next(os.walk('.'))[1]
    for root, dirs, files in os.walk(".", topdown=False): 
        for name in files:
            path = str(os.path.join(root, name))
            
            datasheet_find = re.search(datasheet_pattern, path)
            excel_find = re.search(excel_pattern, path)
            serial_find = re.search(serial_pattern, path)
            
            if datasheet_find and root != "." and not re.findall("~", path) and not re.findall("_", path): 
                datasheet_info = {}
                datasheet_info["id"] = str(name)                
                datasheet_info["stock"] = None
                datasheet_info["serial"] = None
                dirs=str(dirs)
                if re.search(stock_pattern, path):
                    datasheet_info["stock"] = str(re.search(stock_pattern, path).group())
                if re.search(serial_pattern, path):
                    datasheet_info["serial"] = str(re.search(serial_pattern, path).group())                    
                elif not datasheet_info["stock"]:                   
                    datasheet_info["stock"] = str(re.search(nodatasheet_pattern, name).group(1))
                datasheet_info["datasheet_path"] = str(path)
                datasheet_info["folder_path"] = str(root)
                datasheet_info["file_name"] = str(name)
                datasheet_info["files"] = []
                for item in files: 
                    datasheet_info["files"].append(str(root) + "\\" + str(item))
                datasheet_info["pass"] = True
                if re.findall("_", path):
                    fail_id = str(datasheet_info["id"])
                    print(f"__________unit {fail_id}flag as fail__________" )
                    datasheet_info["pass"] = False      
                datasheet_dir[datasheet_info["id"]] = datasheet_info

            if excel_find and root == "." and serial_find:
                datasheet_info = {}
                datasheet_info["id"] = str(name)
                datasheet_info["serial"] = str(re.search(serial_pattern, name).group())
                datasheet_info["datasheet_path"] = path
                datasheet_root[datasheet_info["id"]] = datasheet_info
    print("Datasheet Found in folder: ")   
    num = 0         
    for name in datasheet_dir:
        num += 1
        print(str(num) + ". "+ str(name))
        
    print(f"---END--- \n total: {num}")    
    print("Datasheet Found in Root: ")        
    for name in datasheet_root:
        print(name)
    print("---END\n")
        
    return datasheet_dir,datasheet_root

def generate_serial_mapping(serial_mapping_dir,band_num): 
    print("start generate serial mapping")

            
    file_not_exist = False
    try:
        # Try to open the workbook
##        serial_map = xw.Book("./Seial_Mapping.xlsx")
        serial_map = xw.Book("./Serial_Mapping.xlsx")
        print("Serial Mapping exist, open it")
    except FileNotFoundError:
        # If the workbook doesn't exist, create a new one
        serial_map = xw.Book()
        print("Serial Mapping not exist, create it")
        file_not_exist = True
    
    serial_sheet = {} 
    if file_not_exist:       
        for band in range(band_num):
            band += 1
            if band != 1: 
                xw.sheets.add(f"Sheet{band}")
            serial_sheet[f"Band{band}"] = serial_map.sheets[f"Sheet{band}"]
            serial_sheet[f"Band{band}"].name = f"Band{band}" 

            column_index = 65 
            for item in datasheet_dataposition:
                serial_sheet[f"Band{band}"][f"{chr(column_index)}1"].value = str(item)
                column_index += 1
        print("Serial Mapping created")
    else: 
        for band in range(band_num):
            band += 1
            serial_sheet[f"Band{band}"] = serial_map.sheets[f"Band{band}"]   
            
    print("start write serial mapping")
    for sheet_band in range(band_num):
        sheet_band += 1
        for data_band in serial_mapping_dir:
            if sheet_band == data_band:
                last_row = serial_sheet[f"Band{sheet_band}"].range('A' + str(serial_sheet[f"Band{sheet_band}"].cells.last_cell.row)).end('up').row
                row_index = last_row + 1
                for id in serial_mapping_dir[data_band]:
                    column_index = 65 
                    for data in serial_mapping_dir[data_band][id]:
                        serial_sheet[f"Band{sheet_band}"][f"{chr(column_index)}{row_index}"].value = str(data)
                        column_index += 1
                    row_index += 1
               
    input("Press Enter to continue...")
##    serial_map.save("./Seial_Mapping.xlsx")
    serial_map.save("./Serial_Mapping.xlsx")

def read_data(sheet,datasheet_dir,band_select,serial_mapping_dir,id):
    serial_mapping_data = []
    print("start read data")
    for item in datasheet_dataposition:
        serial_mapping_data.append(sheet[datasheet_dataposition[item]].value)
    #serial_mapping_dir[str(band_select)][datasheet_dir[id]] = serial_mapping_data
    serial_mapping_dir[band_select][datasheet_dir[id]['serial']] = serial_mapping_data


def Load_JSON(file):
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()
    return data

def backup_folder(date_time_str, datasheet_dir):
    print("Back up oringinal folder before start...")
    backup_dir = r"\\silvertip.local\data\Newmore\_Bruce\_Code_log" + r"\_Backup_" + \
        str(date_time_str) + "_Sreial_" + str(list(datasheet_dir.keys())[0])
    shutil.copytree("./", backup_dir)
    print(f" Done copying {str(list(datasheet_dir.keys())[0])}" )

def initial():

    serial = []
    for serial_num in range(int(serial_range)):
        serial.append(str(serial_base) + "-" +
                      str(int(serial_start)+int(serial_num)).zfill(3))
    return band_num, serial, customer, po_num

def read_pdf_to_string(pdf_file):
    with open(pdf_file, 'rb') as file:
        reader = PyPDF2.PdfReader(file)
        content = []
        for page_num in range(len(reader.pages)):
            page = reader.pages[page_num]
            content.append(page.extract_text())
    return content

def insert_pn(datasheet_dir,band,no_band_id,id):
    pn_data = []
    pn_band = ""
    pn_image_path=""
    print("start incert PN Graph and data")
    #for id in datasheet_dir: 
    if True:
        for pdf in datasheet_dir[id]["files"]:
            if no_band_id:
                band_select = 1
            if re.search(pdf_pattern, pdf) and re.search(pn_pdf_pattern, pdf): 
                if re.search(band_pattern, pdf):
                    band_select = re.search(band_pattern, pdf).group()
                if str(band_select) == str(band) or no_band_id:
                    pn_band = band_select
                    pfd_path = os.path.join(datasheet_dir[id]["folder_path"], pdf)
                    pdf_data = read_pdf_to_string(pfd_path)
                    for data in pdf_data: 
                        pn_data.append(re.findall(pn_data_pattern, data))
            pdf = pdf.upper()
            if re.search(image_pattern, pdf) and re.search(pn_image_pattern, pdf):
                print("starting reading image")
                print(pdf)
                if re.search(band_pattern, pdf):
                    band_select = re.search(band_pattern, pdf).group()
                if str(band_select) == str(band) or no_band_id:
                    pn_image_path = os.path.join(datasheet_dir[id]["folder_path"], pdf)                                       
         
    return pn_data, pn_band,pn_image_path

def insert_png_to_xlsx(sheet, image_path, top_left_cell):
    image_path = image_path.lower()
    print(image_path)
    img = Image.open(image_path)
    img_path = str("r'" + image_path +"'")
    path = os.path.abspath(image_path)
    print(path)
    aspect_ratio = img.width / img.height
    width_in_points = 450 
    height_in_points = width_in_points / aspect_ratio
    pic = sheet.pictures.add(path, name='Image', update=True)
    pic.left = sheet.range(top_left_cell).left
    pic.top = sheet.range(top_left_cell).top
    pic.width = width_in_points
    pic.height = height_in_points

def datasheet_move(datasheet_dir, id):
    print("start mod datasheet/folder")
    print(str(datasheet_dir[id]["folder_path"]) + str(datasheet_dir[id]["serial"]))
    
    datasheet_newpath =  str(datasheet_dir[id]["folder_path"]) + "./" + str(datasheet_dir[id]["serial"]) + ".xlsx"
    os.rename(datasheet_dir[id]["datasheet_path"], datasheet_newpath)
    print(datasheet_newpath)
    shutil.copy(datasheet_newpath, "./")
    #datasheet_newname = str("./" + str(datasheet_dir[id]["serial"]) + ".xlsx")
    #os.rename(str("./" + str(datasheet_dir[id]["serial"]) + ".xlsx"), datasheet_newname)
    #shutil.copy(datasheet_newpath, r"\\svr-1\Newmore\_Bruce\_Code_log\Datahseet_Log")
    print(datasheet_dir[id]["serial"] + " Moved")

def rename_folder(datasheet_dir):
    print("start modify folder")
    for id in datasheet_dir:  
        if datasheet_dir[id]["pass"]:
            stock = ""
            if datasheet_dir[id]["stock"]:
                stock = "(" + datasheet_dir[id]["stock"] + ")"
            new_name = str("./" + datasheet_dir[id]["serial"] + stock)                
            os.rename(datasheet_dir[id]["folder_path"], new_name)
            print("change folder name to: " + str(new_name))       
    print("finished modify folder name")

def main():

    os.system('cls')
    current_time = time.asctime(time.localtime(time.time()))
    date_time_str = now.strftime("%Y%m%d_%H%M%S")
    print(current_time)
    
    print("Program Initailizing")
    band_num, serial, customer, po_num = initial()
    datasheet_dir, datasheet_root = scan_dir()
    
    if backup_dir: 
        backup_folder(date_time_str, datasheet_dir)
        
    num = 0
    for id in datasheet_dir:
        if datasheet_dir[id]["pass"]:
            print(str(datasheet_dir[id]["id"]) + " - "  + str(datasheet_dir[id]["serial"]))
            if assign_serial:
                if not datasheet_dir[id]["stock"]:
                    datasheet_dir[id]["stock"] = datasheet_dir[id]["serial"]
                datasheet_dir[id]["serial"] = serial[num]
                print(str(num+1) + ". Assign Serial Number:" + str(datasheet_dir[id]["id"]) + " - "  + str(datasheet_dir[id]["serial"]))
                num += 1

    if assign_serial: 
        input("\n___Press Enter to Start rename folder: ___\n")
        rename_folder(datasheet_dir)
        datasheet_dir, datasheet_root = scan_dir()
        num = 0
        for id in datasheet_dir: 
            datasheet_dir[id]["serial"] = serial[num]
            num += 1
        num = 0
        for id in datasheet_dir:
            num += 1
            print(str(num) + ". " + str(datasheet_dir[id]["id"]) + " - "  + str(datasheet_dir[id]["serial"]))
    print(datasheet_root)
    input("\n___please confirm serial mapping___\n")
        
    serial_sheet_directionary = {}
    serial_mapping_dir = {}
    
    for num in range(band_num):
        serial_mapping_dir[num + 1] = {}

    index = 0

    for id in datasheet_dir:
        if  datasheet_dir[id]["pass"] and not re.findall(str(datasheet_dir[id]["serial"]), str(datasheet_root.items())):
            # if datasheet_name[file_folder_match] not in datasheet_fail and datasheet_name[file_folder_match] not in datasheet_root:
            if not datasheet_dir[id]["serial"]:
                print(datasheet_dir[id])
                print("Current path: " + datasheet_dir[id]["datasheet_path"])
                print("Serial not in folder name")
                datasheet_dir[id]["serial"] = datasheet_dir[id]["stock"]
                
            print(f"start modify : {id}")
            print("Datasheet Path: " + datasheet_dir[id]["datasheet_path"])

            wb = xw.Book(datasheet_dir[id]["datasheet_path"])
            print("Datasheet opened")
            # datasheet modification
            num_sheets = len(wb.sheets)/2
            print(num_sheets)
            for x in range(int(num_sheets)):
                band = 0 + 2 * x
                band_select = x + 1
                serial_sheet_row = str(index + 2)
                print("Current Sheet:" + str(band))
                sheet = wb.sheets[band]
                
                #Load Json File
                for name in datasheet_dir[id]["files"]:
                    if re.findall(str("Band" + str(band_select)), name) and re.findall(".json", name):
                        json_file = str(name)                
                json_data = Load_JSON(json_file)
                 
                print("Serial from datasheet: " +
                      str(sheet[datasheet_dataposition["serial"]].value))
                
                if enable_editing:
                    if sheet[datasheet_dataposition["serial"]].value != datasheet_dir[id]["serial"] and sheet[datasheet_dataposition["serial"]].value != datasheet_dir[id]["stock"]:
                        print(sheet[datasheet_dataposition["serial"]].value)
                        print(datasheet_dir[id]["serial"])
                        print(datasheet_dir[id]["stock"])
                        input("Stock/Serial not find in Datasheet")
                    sheet[datasheet_dataposition["serial"]].value = datasheet_dir[id]["serial"]
                    if not sheet[datasheet_dataposition["stock"]].value or (sheet[datasheet_dataposition["stock"]].value != datasheet_dir[id]["stock"] and datasheet_dir[id]["stock"]):
                        print("edit stock: " + str(datasheet_dir[id]["stock"]))
                        sheet[datasheet_dataposition["stock"]].value = datasheet_dir[id]["stock"]

                    sheet[datasheet_dataposition["customer"]].value = customer
                    sheet[datasheet_dataposition["po"]].value = str(po_num)
                    sheet[datasheet_dataposition["colour"]].value = color_code
                    
                    po_rng = sheet.range(datasheet_dataposition["po"])
                    po_rng.api.HorizontalAlignment = xw.constants.HAlign.xlHAlignLeft
                    stock_rng = sheet.range(datasheet_dataposition["stock"])
                    stock_rng.api.HorizontalAlignment = xw.constants.HAlign.xlHAlignLeft
                    
                # serial_sheet[band].range('A' + str(serial_sheet[band].cells.last_cell.row)).end('up').row = sheet["D3"].value

                if print_DSA: 
                    print("start write DSA value")
                    print(json_data["NFG"]["DSAdB"])
                    print(json_data["NFG"]["DSAhex"])
                    sheet['G4'].value = 'DSA Setting'
                    sheet['H4'].value = str(json_data['NFG']['DSAdB']) + ' dB' + '(' + str(json_data['NFG']['DSAhex']) + ')'
                    serial_sheet_directionary["band{0}".format(band_select)][("G" + serial_sheet_row)].value = sheet['H4'].value
                    rng = sheet.range('G4:H4')  # Update this to your specific range
                    # Change the font
                    rng.api.Font.Name = 'Calibri'  # Change 'Calibri' to your desired font
                    # Change the font size
                    rng.api.Font.Size = 8  # Update the size as needed
                    # Align text to the left
                    rng.api.HorizontalAlignment = xw.constants.HAlign.xlHAlignLeft
                    
                if edit_p1db: 
                    p1db_list = []
                    oip3_list = []
                    print("Start Modify P1dB/OIP3")
                    for p1db_index in range(0,3):
                        p1db_list.append(json_data["P1dB"]["Data"][p1db_index]["P1dB"])
                        oip3_list.append(json_data["P1dB"]["Data"][p1db_index]["OIP3"])
                        p1db_median = numpy.median(p1db_list)
                        opi3_media = numpy.median(oip3_list)
                        
                    print(p1db_median)
                    print(opi3_media)
                    
                    sheet[datasheet_dataposition["p1db"]].value = p1db_median
                    sheet[datasheet_dataposition["oip3"]].value = opi3_media
                    #sheet[datasheet_dataposition["pass_fail"]].value = "Pass"
                
                if DRO: 
                    print("start modify DRO")
                    sheet["F2"].value = "Pass"
                    sheet["L16"].value = ""
                    sheet["M16"].value = ""
                    sheet["M16"].value = ""
                    wb.app.macro('PERSONAL.XLSB!ClearConditionalFormatting')()
                
                if add_pn: 
                    print("start insert PN data")
                    pn_data, pn_band,pn_image_path = insert_pn(datasheet_dir,band_select,no_band_id,id)
                    pn_data = str(pn_data).split(",")
                    print(pn_data)
                    print(type(pn_data))

                    x = 7 # N7 -13
                    for data in pn_data:
                        data = re.findall(pn_data_pattern, data)
                        print(data)
                        sheet[str("N" + str(x))].value = data
                        x += 1
                    top_left_cell='G28'
                    insert_png_to_xlsx(sheet, pn_image_path, top_left_cell)
                    
                print("Start Reading Datasheet")
                read_data(sheet,datasheet_dir,band_select,serial_mapping_dir,id)
                      
            print("Datasheet Mod Finished")

            wb.save()
            wb.close()
            xw.App().kill()

            if copy_datasheet:
                datasheet_move(datasheet_dir, id)
            else:
                print("Read only enabled, not move datasheet")
            index += 1
            print(datasheet_dir[id]["serial"] + " finshed, move to next unit")
            
    generate_serial_mapping(serial_mapping_dir,band_num)
    print("Program Finished")

main()

