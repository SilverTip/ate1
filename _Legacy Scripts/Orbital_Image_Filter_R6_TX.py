from PIL import Image, ImageOps, ImageDraw
import os
import time
import logging

PATH = os.path.dirname(__file__)

# Colour reference
ORBITAL_LIGHT_BLUE = (0, 170, 238)
ORBITAL_DARK_BLUE = (5, 22, 80)
DARK_GREEN = (1, 68, 33)
PURPLE = (255, 0, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)

ORBITAL_LOGO_old = 'orblogo4.PNG'
ORBITAL_LOGO = 'Orbital_Logo_CMYK.JPG'
IMAGE_INPUT = 'input.png'
IMAGE_OUTPUT = 'output.png'


def Filter(file: str, inversion: bool, SGpower: float, watermark=True) -> None:
    # Open image
    try:
        image = Image.open(file).convert('RGB')
    except FileNotFoundError as e:
        message = F'[{os.path.basename(__file__)}]: File not found : {e}'
        raise FileNotFoundError(message)

    # Open Orbital Logo
    try:
        logo = Image.open(ORBITAL_LOGO).convert('RGBA')
    except FileNotFoundError as e:
        message = F'[{os.path.basename(__file__)}]: Logo not found : {e}'
        raise FileNotFoundError(message)

    # Check if inversion is enabled
    if inversion:
        image = ImageOps.invert(image)

    width, height = image.size
    for x in range(width):
        for y in range(height):
            # access current pixel broken down into RGB values
            r, g, b = image.getpixel((x, y))  # SLOW
            new_pixel = (r, g, b)

            # #Modify pixel: (Quick)
            if (r == g == b):  # is shade
                continue
            elif (r == b == 255 and g < 255):  # If inverted green, make cyan
                new_pixel = ORBITAL_LIGHT_BLUE
            elif (r > 200 and g < 200 and b > 150):  # if pink-ish, make orbital light blue
                new_pixel = ORBITAL_LIGHT_BLUE
            elif (r > 190 and g < 100 and b < 100 and r < 250):  # if brown, make orbital dark blue
                new_pixel = ORBITAL_DARK_BLUE
            elif (r > 190 and g < 100 and b < 100):  # if red, turn green
                new_pixel = PURPLE
            elif (r == 0 and g == 255 and b == 255):  # If cyan, turn red
                new_pixel = RED
            elif (r > 250 and g > 250):  # If yellow, make white
                new_pixel = WHITE
            elif (r > 150 and g > 90 and b < 100):  # If browny or orange, make dark blue
                new_pixel = ORBITAL_LIGHT_BLUE
            elif (r > 80 and g > 30 and b < 30) or (
                    r == 0 and g < 100 and b == 255):  # If brown or almost blue, make blue
                new_pixel = BLUE

            if (r, g, b) != new_pixel:
                image.putpixel((x, y), new_pixel)  # SLOW

    if watermark:
        image = image.convert('RGBA')
        image = add_watermark(image, logo)
        # image = add_logo_lower_right(image, logo)

    if SGpower and SGpower != 0:
        I1 = ImageDraw.Draw(image)
 
        # Add Text to an image
        I1.text((width,height), F"SG Power {SGpower}", fill=(255, 0, 0),anchor='rd')
        
    image.save(file)


def add_watermark(image, logo, ratio=0.7, alpha=0.1, tiled=False) -> Image:
    width, height = image.size
    logo_width, logo_height = logo.size
    logo_width_new = int(ratio * width)
    logo_height_new = int(logo_width_new * logo_height / logo_width)
    logo_resized = logo.resize((logo_width_new, logo_height_new))
    background = Image.new('RGBA', image.size, (255, 255, 255, 255))
    center = (int(width / 2 - logo_width_new / 2), int(height / 2 - logo_height_new / 2))
    # lower_right_corner = (width - logo_width_new, height - logo_height_new)
    if tiled:
        for i in range(0, width, logo_height_new):
            for j in range(0, height, logo_width_new):
                background.paste(logo_resized, (i, j))
    else:
        background.paste(logo_resized, center)
    return Image.blend(image, background, alpha)


def add_logo_lower_right(image, logo):
    width, height = image.size
    orb_width, orb_height = logo.size
    corner = 0
    image.paste(logo, (width - orb_width - corner, height - orb_height - corner))
    return image

if __name__ == '__main__':
    # test_filenames = [
    #     ('ATE1.gif', True),
    #     ('ATE1_PN.png', True),
    #     ('ATE1_VNA.png', False),
    #     ('ATE2.png', True),
    #     ('ATE2_PN.png', True),
    #     ('FF.png', True)
    # ]
    # for file, inversion in test_filenames:
    #     start_time = time.time()
    #     Filter(file, inversion=inversion)
    #     print(F"{file} filter runtime: {time.time() - start_time} seconds")
    SGpower = 20
    Filter(r'\\ORB-SVR-FS01\Production\_To_be_REVIEWED\In_Progress\ATE2\8356-014\8356-014_20230704_121132_27.4C\8356-014_20230704_121132_27.4C_Band1_Image_Rejection.PNG', True,20)

# Version
# R4 - 20210816 - BS - Updated trace colours to blue and purple for best contrast
# R5 - ???????? - ?? - add logo in separated add_logo function
# R6 - 20230531 - BS - Recombine drifting R5 versions at ATE1, ATE2, and ATE3 benches.
#                     Modernized coding style
#                     Updated filter values for better Fieldfox screenshots
#                     Added Orbital blended watermark in background, 90% the size of the image width
#                     Increased filter speed. Only replace pixel if modified (line 72)
#                     New high-res logo: Orbital_Logo_CMYK.jpg
#                     Added logging
#                     Approved by Kathryn Choquer - Director of Marketing
