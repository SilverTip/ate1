#Modified from DS_Gen.1.py for LNB ATE V1.7.py
#Modified by Benjamin Stadnik


import xlsxwriter
import json
from PIL import Image
##from win32com import client
import time
## from win32com import client
import sys
import os

 
def GenerateDatasheet(folder, data_file, config_file):  

	path = os.path.dirname(__file__)

	with open(data_file, 'r') as f:
		datastore = json.load(f)
	f.close

	with open(config_file, 'r') as f:
		config = json.load(f)
	f.close

	workbook = xlsxwriter.Workbook(folder + '\\' + datastore['Unit_Band'] + ".xlsx")
	worksheet = workbook.add_worksheet()
	worksheet.set_landscape()
	bold = workbook.add_format({'bold': True})
	not_bold = workbook.add_format({'bold': False})
	bold.set_font_size(8)
	not_bold.set_font_size(8)

	## Client, PO and Date 
	worksheet.write('A2', 'Client', bold) ## Client Text
	worksheet.write('B2', datastore['Cx'], bold) ## Client Name    
	worksheet.write('A3', 'PO', bold) ## PO Text
	worksheet.write('B3', datastore['PO'], bold) # PO Number
	worksheet.write('A4', 'Date', bold) ## Date Text
	worksheet.write('B4', datastore['Date'], bold) ## Date Field

	##Model, Serial and Stock
	worksheet.write('C2', 'Model', bold) ## Model Text
	worksheet.write('D2', config['Mo_N'], bold) ## Model Number / Product Number
	worksheet.write('C3', 'Serial Number', bold) ## Unit Serial Text
	worksheet.write('D3', datastore['Orbital_serial_number'], bold) ## Serial Number
	worksheet.write('D4', datastore['stock_number'], bold) ## Stock Number
	worksheet.write('C4', 'Stock', bold) ## Stock Text

	## Tester and Reviewer
	worksheet.write('E3', 'Tested By', bold) ## Tested by Text
	worksheet.write('F3', datastore['Tester'], not_bold) ## Tester Name
	worksheet.write('E4', 'Reviewed By', bold) ## Checked by Text
	worksheet.write('F4', 'IM', not_bold) ## Reviewer Name



	## Logo Insert
	worksheet.insert_image('G1', path + '\\Datasheet_Gen_2\\orblogo.png')

	## Place screenshots
	try:
		im = Image.open(folder + '\\' + datastore['Unit_Band']+' NF and Gain.gif')
		im.save(folder + '\\' + datastore['Unit_Band']+' NF and Gain.png')
		worksheet.insert_image('A22', folder + '\\' + datastore['Unit_Band']+' NF and Gain.png')
	except:
		print(folder + '\\' + datastore['Unit_Band']+' NF and Gain.gif not found. Passing...')
		pass
	try:
		im = Image.open(folder + '\\' + datastore['Unit_Band']+' Phase Noise.gif')
		im.save(folder + '\\' + datastore['Unit_Band']+' Phase Noise.png')
		worksheet.insert_image('G22', folder + '\\' + datastore['Unit_Band']+' Phase Noise.png')
	except:
		print(folder + '\\' + datastore['Unit_Band']+' Phase Noise.gif not found. Passing...')
		pass
	##Set column width
	worksheet.set_column('B:C',20)
	worksheet.set_column('D:F',15)
	worksheet.set_column('G:H',15)

	##SCD Number, Headers
	##worksheet.write('A6', config['l9'], bold) ##disabled for general datasheet
	worksheet.write('B6', 'Compliance Parameters',bold) 
	worksheet.write('C6', 'Specification', bold)
	worksheet.write('D6','Unit',bold)
	worksheet.write('E6','Status',bold)
	##worksheet.write('F6',config['l14'],bold) ##disabled for general datasheet
	worksheet.write('G6','Measured Parameters',bold)
	worksheet.write('H6','Spec',bold)
	worksheet.write('I6','Data',bold)
	worksheet.write('J6','Unit',bold)
	##worksheet.write('K6',config['l19'],bold) ##disabled for general datasheet
	worksheet.write('L6','Phase Noise',bold)
	worksheet.write('M6','Spec',bold)
	worksheet.write('N6','Data',bold)
	worksheet.write('O6','Unit',bold)

	## Parameter Titles
	worksheet.write('B7','RF Input Frequency',not_bold)
	worksheet.write('B8','IF Output Frequency',not_bold)
	worksheet.write('B9','Local Osc Frequency',not_bold)
	worksheet.write('B10','DC Input Voltage Range',not_bold)
	worksheet.write('B11','Input Flange',not_bold)
	worksheet.write('B12','Output Connector',not_bold)
	worksheet.write('B13','Desense @-40 dBm input',not_bold)
	worksheet.write('B14','Overdrive @-20dBm input',not_bold)
	worksheet.write('B15','Size,(LengthxWidthxHeight)',not_bold)
	worksheet.write('B16','Weight',not_bold)
	worksheet.write('B17','Color',not_bold)

	## Parameter Specs / Values
	worksheet.write('C7',config['RF_In'],not_bold) ## RF Input Frequencies
	worksheet.write('C8',config['IF_Out'],not_bold) ## IF Output Frequencies
	worksheet.write('C9',config['LO'],not_bold) ## LO Frequency
	worksheet.write('C10',config['Volt'],not_bold) ##Input Voltage Range
	worksheet.write('C11',config['Flange_In'],not_bold) ## Input Flange Type
	worksheet.write('C12',config['Output_Con'],not_bold) ## Output connector Type
	worksheet.write('C13',config['Desense'],not_bold) ## Desense Value, usually '<0.1'
	worksheet.write('C14','no harm',not_bold) ## no harm unless special specification
	worksheet.write('C15',config['DUT_Size'],not_bold)## Size Dimensions of DUT
	worksheet.write('C16',config['Weight'],not_bold) ## Weight
	worksheet.write('C17',datastore['color'],not_bold) ## Color

	## Units
	worksheet.write('D7','GHz',not_bold) ## Should always be in GHz...right?
	worksheet.write('D8','MHz',not_bold) ## Should always be in MHz...right?
	worksheet.write('D9','GHz',not_bold) ## Should always be in GHz...right?
	worksheet.write('D10','VDC',not_bold)## Voltage Unit
	worksheet.write('D11','std',not_bold)## Standard?
	worksheet.write('D12','std',not_bold)## Standard?
	worksheet.write('D13','dB',not_bold) ## Desense Unit
	worksheet.write('D14','dBm',not_bold)## Overdrive Unit
	worksheet.write('D15','mm',not_bold) ## Size Unit
	worksheet.write('D16','g',not_bold)  ## Weight Unit

	## Status
	worksheet.write('E7','confirmed',not_bold)
	worksheet.write('E8','confirmed',not_bold)
	worksheet.write('E9','confirmed',not_bold)
	worksheet.write('E10','confirmed',not_bold)
	worksheet.write('E11','confirmed',not_bold)
	worksheet.write('E12','confirmed',not_bold)
	worksheet.write('E13','0.05 max',not_bold)
	worksheet.write('E14','confirmed',not_bold)


	## Disabled SCD Numbers
	##worksheet.write('F7',config['l64'],not_bold)
	##worksheet.write('F8',config['l65'],not_bold)
	##worksheet.write('F9',config['l66'],not_bold)
	##worksheet.write('F10',config['l67'],not_bold)
	##worksheet.write('F11','',not_bold)
	##worksheet.write('F12','',not_bold)
	##worksheet.write('F13','',not_bold)
	##worksheet.write('F14','',not_bold)
	##worksheet.write('F15',config['l72'],not_bold)
	##worksheet.write('F16',config['l73'],not_bold)
	##worksheet.write('F17',config['l74'],not_bold)
	##worksheet.write('F18',config['l75'],not_bold)


	## Disabled SCD Numbers
	##worksheet.write('A7',config['l76'],not_bold)
	##worksheet.write('A8',config['l77'],not_bold)
	##worksheet.write('A9',config['l78'],not_bold)
	##worksheet.write('A10',config['l79'],not_bold)
	##worksheet.write('A11',config['l80'],not_bold)
	##worksheet.write('A12',config['l81'],not_bold)
	##worksheet.write('A13',config['l82'],not_bold)
	##worksheet.write('A14',config['l83'],not_bold)
	##worksheet.write('A15',config['l84'],not_bold)
	##worksheet.write('A16',config['l85'],not_bold)
	##worksheet.write('A17',config['l86'],not_bold)

	## Spec Titles
	worksheet.write('G7','Noise Figure, Max',not_bold)
	worksheet.write('G8','Noise Figure, Ave',not_bold) 
	worksheet.write('G9','Gain, Ave',not_bold)
	worksheet.write('G10','Max Ripple 10 MHz',not_bold) ## Usually 10 MHz is the spec but can change
	worksheet.write('G11','In Band Spurs',not_bold)
	worksheet.write('G12','Image Rejection',not_bold)
	worksheet.write('G13','LO Leakage Input',not_bold)
	worksheet.write('G14','LO Leakage Output',not_bold)
	worksheet.write('G15','P1dB(Output)',not_bold)
	worksheet.write('G16','OIP3',not_bold)
	worksheet.write('G17',('DC Current, %sV')%(config['Voltage']),not_bold)
	worksheet.write('G18','Input VSWR',not_bold) ## Add option for Isolator and without Isolator
	worksheet.write('G19','Output VSWR',not_bold)
	worksheet.write('G20',config['Spe1'],not_bold) ##Extra Testing Note 1 Ex. Pressure Tested @ 1PSI
	worksheet.write('G21',config['Spe2'],not_bold) ##Extra Testing Note 2
	##worksheet.write('G4','SCD',not_bold) ## IF SCD is required to be listed


	##Spec Values
	worksheet.write('H7',float(config['NF_Spec'])+0.1,not_bold)
	worksheet.write('H8',config['NF_Spec'],not_bold)
	worksheet.write('H9',config['Gain_Spec'],not_bold)
	worksheet.write('H10',config['Per_10M'],not_bold)
	worksheet.write('H11',config['Spur_Spec'],not_bold)
	worksheet.write('H12',config['ImRej_Spec'],not_bold)
	worksheet.write('H13',config['LoIn_Spec'],not_bold)
	worksheet.write('H14',config['LoOut_Spec'],not_bold)
	worksheet.write('H15',config['P1dB_Spec'],not_bold)
	worksheet.write('H16',config['OIP3_Spec'],not_bold)
	worksheet.write('H17',config['Current_Spec'],not_bold)
	worksheet.write('H18',config['S11_Spec'],not_bold)
	worksheet.write('H19',config['S22_Spec'],not_bold)
	##worksheet.write('H4','0132-1211-01',bold) ## SCD Specification if required


	##Measurement Values
	worksheet.write('I7',datastore['nf_gain'][19],not_bold) #['nf_gain'][5] is avg, ['nf_gain'][19] is max
	worksheet.write('I8',datastore['nf_gain'][5],not_bold) #['nf_gain'][5] is avg, ['nf_gain'][19] is max
	worksheet.write('I9',datastore['nf_gain'][6],not_bold)
	worksheet.write('I10',datastore['nf_gain'][16],not_bold)
	## Measurement Data
	worksheet.write('I11',datastore['spurs'],not_bold)
	worksheet.write('I12',datastore['image_rej'],not_bold)
	worksheet.write('I13',datastore['lo_leakage_in'],not_bold)
	worksheet.write('I14',datastore['lo_leakage'],not_bold)
	worksheet.write('I15',datastore['p1db'],not_bold)
	worksheet.write('I16',float(datastore['p1db'])+10,not_bold)
	worksheet.write('I17',datastore['current'],not_bold)
	worksheet.write('I18',datastore['S11'],not_bold)
	worksheet.write('I19',datastore['S22'],not_bold)
	worksheet.write('I20',config['SpN1'],not_bold) ## Comment for Extra Testing Note 1 Ex. Passed Pressure Testing
	worksheet.write('I21',config['SpN2'],not_bold) ## Isolator and pressure test
	


	## Units, Should never change?
	worksheet.write('J7','dB',not_bold)
	worksheet.write('J8','dB',not_bold)
	worksheet.write('J9','dB',not_bold)
	worksheet.write('J10','dB',not_bold)
	worksheet.write('J11','dBc',not_bold)
	worksheet.write('J12','dBc',not_bold)
	worksheet.write('J13','dBm',not_bold)
	worksheet.write('J14','dBm',not_bold)
	worksheet.write('J15','dBm',not_bold)
	worksheet.write('J16','dBm',not_bold)
	worksheet.write('J17','mA',not_bold)
	worksheet.write('J18','',not_bold) ## VSWR is unitless


	#### Disabled SCD Numbers
	##worksheet.write('K7',config['l121'],not_bold)
	##worksheet.write('K8',config['l122'],not_bold)
	##worksheet.write('K9',config['l123'],not_bold)
	##worksheet.write('K10',config['l124'],not_bold)
	##worksheet.write('K11',config['l125'],not_bold)
	##worksheet.write('K12',config['l126'],not_bold)

	## Phase Noise Spec
	worksheet.write('L7','10 Hz',not_bold)
	worksheet.write('L8','100 Hz',not_bold)
	worksheet.write('L9','1 KHz',not_bold)
	worksheet.write('L10','10 KHz',not_bold)
	worksheet.write('L11','100 KHz',not_bold)
	worksheet.write('L12','1000 KHz',not_bold)

	##Phase Noise Spec
	worksheet.write('M7',config['P10_Spec'],not_bold)
	worksheet.write('M8',config['P100_Spec'],not_bold)
	worksheet.write('M9',config['P1K_Spec'],not_bold)
	worksheet.write('M10',config['P10K_Spec'],not_bold)
	worksheet.write('M11',config['P100K_Spec'],not_bold)
	worksheet.write('M12',config['P1M_Spec'],not_bold)

	###Notes under the screenshots, Usually detailing what equipment was used, uncertainty etc
	worksheet.write('A47',config['Note_1'],not_bold)
	worksheet.write('A48',config['Note_2'],not_bold)
	worksheet.write('A49',config['Note_3'],not_bold)



	## Phase Noise Data
	worksheet.write('N7',datastore['phase'][0],not_bold)
	worksheet.write('N8',datastore['phase'][1],not_bold)
	worksheet.write('N9',datastore['phase'][2],not_bold)
	worksheet.write('N10',datastore['phase'][3],not_bold)
	worksheet.write('N11',datastore['phase'][4],not_bold)
	worksheet.write('N12',datastore['phase'][5],not_bold)

	## Phase Noise Units
	worksheet.write('O7','dBc/Hz',not_bold)
	worksheet.write('O8','dBc/Hz',not_bold)
	worksheet.write('O9','dBc/Hz',not_bold)
	worksheet.write('O10','dBc/Hz',not_bold)
	worksheet.write('O11','dBc/Hz',not_bold)
	worksheet.write('O12','dBc/Hz',not_bold)

	## Adding second Page for frequency, NF, and Gain List
	worksheet2 = workbook.add_worksheet()

	#Frequency	Noise 	Gain	Per 10 MHz	Per 120 Mhz	Per 500 MHz
	worksheet2.write('A4','Averages',bold)
	worksheet2.write('A5','Frequency',bold)
	worksheet2.write('B5','Noise',bold)
	worksheet2.write('C5','Gain',bold)
	worksheet2.write('D5','Linear Gain',bold)
	worksheet2.write('E5','Linear NF',bold)
	worksheet2.write('F5','Per 10 MHz',bold) 
	worksheet2.write('G5','Per 120 MHz',bold)
	worksheet2.write('H5','Per 500 MHz',bold)
	worksheet2.write('I5','Per 1000 MHz',bold)

	lenx = len(datastore['nf_gain'][0])
	for i in range(6,6+lenx):
		tmp = "A"
		tmp = tmp + str(i)
		tmp_index = i-6
		worksheet2.write(tmp,datastore['nf_gain'][0][tmp_index],not_bold)

	lenx = len(datastore['nf_gain'][3])
	##WRITE Noise
	for i in range(6,6+lenx):
		tmp = "B"
		tmp = tmp + str(i)
		tmp_index = i-6
		worksheet2.write(tmp,datastore['nf_gain'][3][tmp_index],not_bold)
		
	lenx = len(datastore['nf_gain'][4])
	##WRITE GAIN
	for i in range(6,6+lenx):
		tmp = "C"
		tmp = tmp + str(i)
		tmp_index = i-6
		worksheet2.write(tmp,datastore['nf_gain'][4][tmp_index],not_bold)

	##Labels for Ripple Specs etc
	worksheet2.write('F2','Amplitude Response, Max', bold)
	worksheet2.write('F3',config['Per_10M'], bold)
	worksheet2.write('G3',config['Per_120M'], bold)
	worksheet2.write('H3',config['Per_500M'], bold)
	worksheet2.write('I3',config['Per_1G'], bold)

	##AVERAGES
	worksheet2.write_formula('E4','=10^(B4/10)',not_bold)
	worksheet2.write_formula('D4','=10^(C4/10)',not_bold)
	worksheet2.write('B4',datastore['nf_gain'][5],not_bold)
	worksheet2.write('C4',datastore['nf_gain'][6],not_bold)


	## Calculating the Ripple measurement Values
	##Amplitude Per 10M
	worksheet2.write_formula('F4','=MAX(F8:F107)/2',not_bold)
	##Amplitude Per 120M
	worksheet2.write_formula('G4','=MAX(G30:G118)/2',not_bold)
	##Amplitude Per 500M
	worksheet2.write('H4',datastore['nf_gain'][13],not_bold)
	##Amplitude Per 1000M
	worksheet2.write('I4',datastore['nf_gain'][14],not_bold)

	##Write GAIN LINEAR
	for i in range(6,6+lenx):
		tmp = "D"
		tmp2 = "C"
		tmp = tmp + str(i)
		tmp2 = tmp2 + str(i)
		formula = "=10^(%s/10)" % (tmp2)
		worksheet2.write_formula(tmp,formula,not_bold)


	##Write NF Linear
	for i in range(6,6+lenx):
		tmp = "E"
		tmp2 = "B"
		tmp = tmp + str(i)
		tmp2 = tmp2 + str(i)
		formula = "=10^(%s/10)" % (tmp2)
		worksheet2.write_formula(tmp,formula,not_bold)
		
	##Write per 10mhz
	lenx = len(datastore['nf_gain'][7])
	for i in range(8,8+lenx):
		tmp = "F"
		tmp = tmp + str(i)
		tmp_index = i-8
		worksheet2.write(tmp,datastore['nf_gain'][7][tmp_index],not_bold)


	##WRITE per 120mhz
	lenx = len(datastore['nf_gain'][8])
	for i in range(30,30+lenx):
		tmp = "G"
		tmp = tmp + str(i)
		tmp_index = i-30
		worksheet2.write(tmp,datastore['nf_gain'][8][tmp_index],not_bold)



	### start here


	workbook.close()

	##path = os.path.dirname(__file__) + "\\"
	##xlApp = client.Dispatch("Excel.Application")
	##books = xlApp.Workbooks.Open('C:\\Users\\Administrator\\Documents\\Fed Test\\Beta Codes\\All in one ATE\\template\\' + datastore['Orbital_serial_number']+'.xlsx')
	##
	##books.Worksheets(["Sheet1", "Sheet2"]).Select()
	##ws = books.Worksheets[0]
	##ws.PageSetup.Zoom = False
	##
	##ws.Visible = 1
	##ws.PageSetup.FitToPagesWide = 1
	##xlApp.ActiveSheet.ExportAsFixedFormat(0,'C:\\Users\Administrator\\Documents\\Fed Test\\Beta Codes\\All in one ATE\\template\\'+datastore['Orbital_serial_number']+'.pdf')
	##books.Close(False)
	##xlApp.quit()
