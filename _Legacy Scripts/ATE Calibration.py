import pyvisa as visa
import time
import xlsxwriter
from tkinter import *
import datetime
import os
import shutil
import json
from os import walk
import keysight.command_expert_py3 as kt
from tkinter import ttk

### Equipment Addresses ###
SA = 'TCPIP0::10.0.10.47::inst0::INSTR'
VNA = 'TCPIP0::M9037A-45473669::hislip0::INSTR'
PS = 'USB0::0x0957::0x4D18::MY57510003::0::INSTR'
class Equipment:
    def __init__(self,resource_id):
        self._rm = visa.ResourceManager()
        self._inst = self._rm.open_resource(resource_id)
        self._inst.timeout = 500000000                                   ###check this one
        

    def query_start_Freq(self):
        self._fetch_start_Freq = self._inst.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')
        self._start_Freq = self._fetch_start_Freq[0]
        return self._start_Freq

    def query_stop_Freq(self):
        self._fetch_stop_Freq = self._inst.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')
        self._stop_Freq = self._fetch_stop_Freq[0]
        return self._stop_Freq
    
    def query_number_of_points(self):
        self._fetch_number_of_points = self._inst.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')
        self._number_of_points = self._fetch_number_of_points[0]
        return self._number_of_points

    def query_NF(self):
        self._fetch_NF = self._inst.query_ascii_values(':READ:NFIG:ARR:DATA:CORR:NFIG?')
        return self._fetch_NF
    
    def query_Gain(self):
        self._fetch_Gain = Gain = self._inst.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
        return self._fetch_Gain

    def save_screenshot(self,picture_file_name):
        self._inst.values_format.is_binary = True
        self._inst.values_format.datatype = 'B'
        self._inst.values_format.is_big_endian = False
        self._inst.values_format.container = bytearray
        
        self._inst.write(":MMEMory:STORe:SCReen 'C:\\TEST.GIF\'")
        self._fetch_image_data = self._inst.query_values("MMEM:DATA? 'C:\\TEST.GIF\'")

        self._save_dir = open(picture_file_name+".GIF", 'wb')     #change this so one can enter
        self._save_dir.write(self._fetch_image_data)
        self._save_dir.close()

        self._inst.write(":MMEM:DEL 'C:\\TEST.GIF\'")
        self._inst.write("*CLS")

    def close_instrument(self):
        self._inst.close()
        self._rm.close()

    def get_equipment(self):
        return self._inst

class Excel:
    def __init__(self,inst,excel_file_name):
        self.inst = inst
        self._workbook = xlsxwriter.Workbook(excel_file_name+'.xlsx')
        self._worksheet = self._workbook.add_worksheet()

    def header_excel(self,row,column,header_text):
        self._worksheet.write(row,column,header_text)

    def write_to_excel_Freq(self,row,column):
        self._start_Freq_data = self.inst.query_start_Freq()
        self._number_of_points_data = self.inst.query_number_of_points()
        self._stop_Freq_data = self.inst.query_stop_Freq()
        
        self._worksheet.write(row,column,self._start_Freq_data)                                                                        
        self._worksheet.write(int(self._number_of_points_data)+2,column,self._stop_Freq_data)                        
        self._next_Freq = self._start_Freq_data                                                       
        self._add_next_Freq=((self._stop_Freq_data-self._start_Freq_data)/(self._number_of_points_data-1))                     
        
        
        i = row+1
        while i <= self._number_of_points_data + 2:                                                        
            self._next_Freq = self._next_Freq+self._add_next_Freq
            self._worksheet.write(i,column,self._next_Freq)
            i = i+1

    def write_to_excel_NF(self,row,column):
        self._NF_data = self.inst.query_NF()
        
        i = 0
        while i < len (self._NF_data):                                                               
            self._worksheet.write(row,column,self._NF_data[i])                                     
            i = i+1
            row = row+1

    def write_to_excel_Gain(self,row,column):
        self._Gain_data = self.inst.query_Gain()
        
        i = 0
        while i < len (self._Gain_data):                                                               
            self._worksheet.write(row,column,self._Gain_data[i])                                       
            i = i+1
            row = row+1

    def write_to_excel_tester(self,row,column,tester):
        self._worksheet.write(row,column,'%s' %tester)

    def write_to_excel_date(self,row,column,date):
        self._worksheet.write(row,column,'%s' %str(date))
        
            
    def close_excel(self):
        self._workbook.close()

def Pwr_on(settings_data,PS):

    rm = visa.ResourceManager()
    U3606B = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    U3606B.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude %G' % float(settings_data['Voltage']))
    time.sleep(1)
    U3606B.write(':OUTPut:STATe %d' %(1))
    time.sleep(2)
    U3606B.close()

def Pwr_off(PS):

    rm = visa.ResourceManager()
    U3606B  = rm.open_resource('%s' %PS)
    U3606B.timeout = 50000
    U3606B.write(':OUTPut:STATe %d' %(0))
    U3606B.close()

def main():
    rm = visa.ResourceManager()
    M9375A = rm.open_resource('%s' %VNA)
    M9375A.timeout = 500000

    path = os.path.dirname(__file__)
    set_file = []
    date = str(datetime.datetime.now().date())
    ## Golden unit availability check
    Gu = G_unit.get()
    print(popupMenu1.get())
    set_file.append(popupMenu1.get())

    print("Calibrating for: %s" %set_file[0])
    print('\n')

    file1 = open(path + "\\Set_Files\\" + set_file[0],'rb')
    settings_data = json.load(file1)
    profile = str(settings_data['nf_gain_state_file'])
    profile = profile.replace("C:","")
    profile = profile.replace(".STA","")


    

    ## Open SA
    if(Gu == 1):
        reset = 1
        input("Please set up golden unit and press Enter when ready")
        print('\n')
        Pwr_on(settings_data,PS)
        print("Golden unit calibration enabled, loading state...")
        print('\n')
        rm = visa.ResourceManager()
        E4440A = rm.open_resource('%s' %SA)

        ### Loading NFG state file ###
        E4440A.write("MMEM:LOAD:STAT 1,'"+str(settings_data['nf_gain_state_file'])+"'")
        ##setting NFG Path
        [ChannelStatesOn] = kt.run_sequence('NFG Path')
            
            
        ##waiting for file to load
        time.sleep(20)
        while(reset == 1):
            Pwr_on(settings_data,PS)
            tester = YouText.get()
            picture_file_name = TitText.get() + " " + date
            excel_file_name = picture_file_name
            E4440A = Equipment('%s' %SA)
            filesave = Excel(E4440A,excel_file_name)
            print (E4440A.get_equipment())
            
            filesave.header_excel(2,0,'Frequency')
            filesave.header_excel(2,1,'NF')
            filesave.header_excel(2,2,'Gain')
            filesave.header_excel(2,3,'Tester')
            filesave.header_excel(2,4,'Date')
            
            filesave.write_to_excel_Freq(3,0)
            filesave.write_to_excel_NF(3,1)
            filesave.write_to_excel_Gain(3,2)
            filesave.write_to_excel_tester(3,3,tester)
            filesave.write_to_excel_date(3,4,date)

            
            
            filesave.close_excel()
            Pwr_off(PS)
            r = input("Save calibration data? (y/n)")
            if(r == 'y'):                            
                try:
                    path = os.path.dirname(__file__)
                    cap = picture_file_name + ".GIF"
                    exc = excel_file_name + ".xlsx"
                    target = path + "\\Golden_Cals\\" 
                    print("Moving files to Golden_Cal...")
                    print('\n')
                    E4440A.save_screenshot(picture_file_name)
                    ## Move Screenshot
                    shutil.move(os.path.join(path,cap),os.path.join(target,cap))
                    ## Move Excel
                    shutil.move(os.path.join(path,exc),os.path.join(target,exc))
                    print("If any modifications were made to the state please save as: %s" %profile)
                    print('\n')
                    
                except:
                    print("Error in moving file...moving on...")
                    print('\n')
                reset = 0
            else:
                print("Restarting sweep...")
                print('\n')
                reset = 1
        

    ## Load S11 State
    print('\n')
    print('\n')
    print("Loading S11 State file...")
    print("Please calibrate the state and save")
    print("Ensure Power Supply is OFF!")
    Pwr_off(PS)
    [ChannelStatesOn] = kt.run_sequence('S11 Path')
    path = os.path.dirname(__file__) + "\\VNAstates\\" + str(settings_data['S11State'])
    M9375A.write("MMEM:LOAD '%s'"%path )
    M9375A.write("OUTP 1")
    input("Press enter when finished calibrating, don't forget to save the file!")
    print('\n')


    ## Load S22 State
    print('\n')
    print('\n')
    print("Loading S22 State file...")
    print("Please calibrate the state and save")
    print("Ensure Power Supply is OFF!")
    Pwr_off(PS)
    [ChannelStatesOn] = kt.run_sequence('S22 Path')
    path = os.path.dirname(__file__) + "\\VNAstates\\" + str(settings_data['S22State'])
    M9375A.write("MMEM:LOAD '%s'"%path )
    M9375A.write("OUTP 1")
    M9375A.write("OUTP 0")
    input("Press enter when finished calibrating, don't forget to save the file!")
    print('\n')
    print('\n')

    rm.close()

    print("Calibration Complete...Have fun testing")


    
    
    



## Main window ##
master = Tk()
master.title('ATE Calibration')

## Variables ##
path = os.path.dirname(__file__)
mypath = path + '\Set_Files'
choices = ['None']
tkvar = StringVar(master)
G_unit = IntVar()

###### Drop down menu to select what profile to calibrate #######
Bnd = LabelFrame(master, text="Select which profile you wish to calibrate for")
Bnd.grid(row=1,column=1)


for (dirpath, dirnames, filenames) in walk(mypath):
    choices.extend(filenames)
    break


Bnd1Name = Label(Bnd,text="Profile to calibrate for:").grid(row=2,column=1,sticky=W)

## Drop Down Menu 1
tkvar.set('None') ## Set Default Value
popupMenu1 = ttk.Combobox(Bnd, values = choices) ##Makes the menu
popupMenu1.grid(row = 2, column =2,sticky=W,ipadx=100) ## Positions the Menu


## Frame
Info = LabelFrame(master, text="Please input info here")
Info.grid(row=2,column=1)

## Tester Name
You = Label(Info, text = 'Tester Name:').grid(row=1, column = 0, sticky = W)
YouText = Entry(Info)
YouText.grid(row = 1, column = 1, sticky = E)

## Tester Name
Tit = Label(Info, text = 'File Title:').grid(row=0, column = 0, sticky = W)
TitText = Entry(Info)
TitText.grid(row = 0, column = 1, sticky = E)

#Check Box for Voltage Switching###
Checkbutton(Bnd, text = 'Golden unit (Ignore if no golden unit is available)', variable = G_unit).grid(row = 5, column =1)




## Button to Begin Calibration
b1 = Button(master,text='Begin',command=main).grid(row=7,column=3,sticky=E)
mainloop()



