import pyvisa as visa
import datetime
import time
import shutil
import os

##VNA_VISA = 'TCPIP0::M9037A-45473669::hislip0::INSTR'
##rm = visa.ResourceManager()
##VNA = rm.open_resource(VNA_VISA)
##VNA.timeout = 5000
##print(VNA.query('*IDN?').split('\n')[0])

def VNA_Screenshot(folder_path): 
    #Setup filename and folder path
    filename = "TEST.PNG"
    
    #saving a screenshot on the equipment
    VNA.write(":MMEM:STOR:SSCR 'C:\\TEST.BMP\'")
    #fetching the image data in an array
    fetch_image_data = VNA.query_binary_values("MMEM:TRAN? 'C:\\TEST.BMP\'", datatype='B', is_big_endian=True)

    #creating a file with the product number to write the image data to it
    with open(filename, 'wb') as f:
        f.write(bytes(fetch_image_data))

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'C:\\TEST.BMP\'")
    VNA.write("*CLS")
    VNA.write("OUTP 0")

    shutil.move(filename, folder_path + '\\' + filename) 

    #Include image processing for datasheet (greyscaling, inversion, etc)
    #ImageFilter.Filter(folder_path, filename, False)

def VNA_Save_Data(folder_path): 
    #Setup filename and folder path
    filename = 'TEST.CSV'
    
    #saving a screenshot on the equipment
    VNA.write(F":MMEMory:STORe:CSV:FORMAT 'TEST.CSV'")
    #fetching the image data in an array
    fetch_csv_data = VNA.query_binary_values("MMEM:TRAN? 'TEST.CSV'", datatype='B', is_big_endian=True)

    #creating a file with the product number to write the image data to it
    with open(filename, 'wb') as f:
        f.write(bytes(fetch_csv_data))

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TEST.CSV'")
    VNA.write("*CLS")
    VNA.write("OUTP 0")

    shutil.move(filename, folder_path + '\\' + filename)

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%m%d_%H%M%S")
    now[2] = current_time
    return now

##while(1):
##    command = input("Command:")
##    try:
##        
##        if(command.count('?')):
##            reply = VNA.query(command).strip('\n')
##            print(F"Reply: {reply}")
##        elif(command == 'SS'):
##            VNA_Screenshot(os.getcwd())
##        elif(command == 'CSV'):
##            VNA_Save_Data(os.getcwd())
##        else:
##            VNA.write(command)
##            
##    except Exception as e:
##        print(e)


def function():
    global SLEEPYTIME
    sleepytime = 0
    before = timestamp()[2]
    time.sleep(2)
    after = timestamp()[2]
    duration = after - before
    SLEEPYTIME += duration
    print(duration)

global SLEEPYTIME
SLEEPYTIME = datetime.timedelta(seconds=0)

while(1):
    try:
        function()
    except KeyboardInterrupt:
        print(SLEEPYTIME)
        break

