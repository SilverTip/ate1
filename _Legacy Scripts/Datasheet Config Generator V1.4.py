from tkinter import *
import time
import json


def gen():
    print("Generating...")

    file_name = configText.get()
    x2 = ModelText.get()
    x3 = RF_InSText.get()
    x4 = RF_InFText.get()
    x5 = LOText.get()
    x6 = V_MinText.get()
    x7 = V_MaxText.get()
    x8 = FlangeText.get()
    x9 = Con_OutText.get()
    x10 = DesenseText.get()
    x11 = DimText.get()
    x12 = WeightText.get()
    x13 = NFText.get()
    x14 = GainText.get()
    x15 = SpurText.get()
    x16 = IRejText.get()
    x17 = LO_InText.get()
    x18 = LO_OutText.get()
    x19 = P1dBText.get()
    x20 = OIP3Text.get()
    x21 = DC_CText.get()
    x22 = S11Text.get()
    x23 = S22Text.get()
    x24 = P10Text.get()
    x25 = P100Text.get()
    x26 = P1kText.get()
    x27 = P10kText.get()
    x28 = P100kText.get()
    x29 = P1MText.get()
    x30 = P_10Text.get()
    x31 = P_120Text.get()
    x32 = P_1GText.get()
    x33 = Note_1Text.get()
    x34 = Note_2Text.get()
    x35 = Note_3Text.get()
    x36 = TVoltText.get()
    x37 = Spec1Text.get()
    x38 = Spec2Text.get()
    x39 = SpNote1Text.get()
    x40 = SpNote2Text.get()
    x41 = P_500Text.get()

    ## Formatting RF In Frequencies
    
    
    ## Calculating and formatting IF Frequencies
    if (x5 > x3):
        z2 = round(((float(x5) - float(x3))*1000),4)
        z3 = round(((float(x5) - float(x4))*1000),4)
        y2 = str(z2) +" - "+ str(z3)
        y1 = str(x4) + " - " + str(x3)
    else:
        z2 = round(((float(x3) - float(x5))*1000),4)
        z3 = round(((float(x4) - float(x5))*1000),4)
        y2 = str(z2) +" - "+ str(z3)
        y1 = str(x3) + " - " + str(x4)
    ## Formatting Voltage Range
    y3 = str(x6) +" - "+ str(x7)
    

    data = {'Mo_N': x2,'RF_In': y1,'IF_Out':y2,'LO':x5,'Volt':y3,'Flange_In':x8,'Output_Con':x9,'Desense':x10,
            'DUT_Size':x11,'Weight':x12,'NF_Spec':x13,'Gain_Spec':x14,'Spur_Spec':x15,'ImRej_Spec':x16,'LoIn_Spec':x17,
            'LoOut_Spec':x18,'P1dB_Spec':x19,'OIP3_Spec':x20,'Current_Spec':x21,'S11_Spec':x22,'S22_Spec':x23,
            'P10_Spec':x24,'P100_Spec':x25,'P1K_Spec':x26,'P10K_Spec':x27,'P100K_Spec':x28,'P1M_Spec':x29,
            'Per_10M':x30,'Per_120M':x31,'Per_1G':x32,'Note_1':x33,'Note_2':x34,'Note_3':x35,'Voltage':x36,
            'Spe1': x37,'Spe2':x38,'SpN1': x39,'SpN2':x40,'Per_500M': x41}

    with open('config_'+file_name+".json",'w') as f:
        json.dump(data,f)

    print("Generated!")
    master.quit


## Old Data Trial
    
##    data = {'nf_gain_state_file':(x1 = y1.get()),'nf_gain_limit_1_file':x2 = y2.get(),'nf_gain_limit_3_file':x3 = y3.get(),'nf_gain_limit_4_file':x4 = y4.get(),'nf_gain_interested_start_Frequency':x5 = y5.get(),'nf_gain_interested_stop_Frequency':x6 = y6.get(),
##        'p1db_LO_Frequency':x7 = y7.get(),'p1db_signal_generator_frequency':x8 = y8.get(),'p1db_cable_loss':x9 = y9.get(),'p1db_muxT_and_cable_loss':x10 = y10.get(),'p1db_signal_generator_power_in_sweep_from':x11 = y11.get(),'p1db_signal_generator_power_in_sweep_to':x12 = y12.get(),
##        'phase_noise_state_file':x13 = y13.get(),'phase_noise_signal_generator_frequency':x14 = y14.get(),'phase_noise_signal_generator_power_in':x15 = y15.get(),
##        'image_rejection_state_file':x16 = y16.get(),'image_rejection_signal_generator_frequency_for_verification':x17 = y17.get(),'image_rejection_cable_loss':x18 = y18.get(),'image_rejection_signal_generator_frequency_for_image_rejection':x19 = y19.get(),
##        'lo_leakage_output_state_file':x20 = y20.get(),'lo_leakage_output_muxT_and_cable_loss':x21 = y21.get(),'lo_leakage_input_state_file':x22 = y22.get(),'lo_leakage_input_cable_loss':x23 = y23.get(),
##        'in_band_spur_state_file':x24 = y24.get(),'in_band_spur_limit_1_file':x25 = y25.get(),'in_band_spur_signal_generator_frequency':x26 = y26.get(),'in_band_spur_cable_loss':x27 = y27.get()}
##
##    with open(file_name+'_settings_file.json','w') as f:
##        json.dump(data,f)



## Initializing for temp values




master = Tk()## Open a window
master.title("Datasheet Config Generator")




instructions = "Please input the values into the corresponding box and press the Generate button, Leave blank if not required"
intro = Label(master, text = instructions,justify=CENTER) ## Introduction
intro.grid(row=1,column=0,sticky=W,columnspan=3)

##space = Label(master, text ='') ## Spacing
##space.grid(row=3,column=1)

## Group designation for General Information ##
Info = LabelFrame(master, text="General Info",padx=5,pady=5)
Info.grid(row=3,column=0)

##Name of config file to be generated
configName = Label(Info, text = 'File Name:')
configName.grid(row=1,column=0,sticky=W)
configText = Entry(Info) ##Input Box for Value
configText.grid(row=1,column=1,sticky=E,ipadx = 150)

##Model Number
ModelName = Label(Info, text = 'Model or Part Number:')
ModelName.grid(row=2,column=0,sticky=W)
ModelText = Entry(Info) ##Input Box for Value
ModelText.grid(row=2,column=1,sticky=E,ipadx = 150)

##Testing Voltage
TVoltName = Label(Info, text = 'Testing Voltage:')
TVoltName.grid(row=3,column=0,sticky=W)
TVoltText = Entry(Info) ##Input Box for Value
TVoltText.grid(row=3,column=1,sticky=E,ipadx = 150)

## Group designation for DUT Description ##
DUTDesc = LabelFrame(master, text="DUT Description",padx=5,pady=5)
DUTDesc.grid(row=4,column=1)

##RF Input Frequencies Start
RF_InSName = Label(DUTDesc, text = 'RF Input Start Frequency in GHz:')
RF_InSName.grid(row=1,column=0,sticky=W)
RF_InSText = Entry(DUTDesc) ##Input Box for Value
RF_InSText.grid(row=1,column=1,sticky=E)

##RF Input Frequencies Stop
RF_InFName = Label(DUTDesc, text = 'RF Input Stop Frequency in GHz:')
RF_InFName.grid(row=2,column=0,sticky=W)
RF_InFText = Entry(DUTDesc) ##Input Box for Value
RF_InFText.grid(row=2,column=1,sticky=E)

##LO Frequency
LOName = Label(DUTDesc, text = 'LO Frequency in GHz:')
LOName.grid(row=3,column=0,sticky=W)
LOText = Entry(DUTDesc) ##Input Box for Value
LOText.grid(row=3,column=1,sticky=E)

##Minimum Safe Voltage
V_MinName = Label(DUTDesc, text = 'Minimum Safe Operating Voltage:')
V_MinName.grid(row=4,column=0,sticky=W)
V_MinText = Entry(DUTDesc) ##Input Box for Value
V_MinText.grid(row=4,column=1,sticky=E)

##Minimum Safe Voltage
V_MaxName = Label(DUTDesc, text = 'Maximum Safe Operating Voltage:')
V_MaxName.grid(row=5,column=0,sticky=W)
V_MaxText = Entry(DUTDesc) ##Input Box for Value
V_MaxText.grid(row=5,column=1,sticky=E)

##Input Flange
FlangeName = Label(DUTDesc, text = 'Input Flange:')
FlangeName.grid(row=6,column=0,sticky=W)
FlangeText = Entry(DUTDesc) ##Input Box for Value
FlangeText.grid(row=6,column=1,sticky=E)

##Output Connector
Con_OutName = Label(DUTDesc, text = 'Output Connector Type:')
Con_OutName.grid(row=7,column=0,sticky=W)
Con_OutText = Entry(DUTDesc) ##Input Box for Value
Con_OutText.grid(row=7,column=1,sticky=E)

##Desense Value
DesenseName = Label(DUTDesc, text = 'Desense value (usually <0.1):')
DesenseName.grid(row=8,column=0,sticky=W)
DesenseText = Entry(DUTDesc) ##Input Box for Value
DesenseText.grid(row=8,column=1,sticky=E)

##DUT Dimensions
DimName = Label(DUTDesc, text = 'DUT Dimensions (LxWxH):')
DimName.grid(row=9,column=0,sticky=W)
DimText = Entry(DUTDesc) ##Input Box for Value
DimText.grid(row=9,column=1,sticky=E)

##DUT Weight
WeightName = Label(DUTDesc, text = 'DUT Weight:')
WeightName.grid(row=10,column=0,sticky=W)
WeightText = Entry(DUTDesc) ##Input Box for Value
WeightText.grid(row=10,column=1,sticky=E)

## Group designation for Specs##
Specs = LabelFrame(master, text="DUT Specifications",padx=5,pady=5)
Specs.grid(row=4,column=0)

##NF
NFName = Label(Specs, text = 'NF Spec:')
NFName.grid(row=1,column=0,sticky=W)
NFText = Entry(Specs) ##Input Box for Value
NFText.grid(row=1,column=1,sticky=E)

##Gain
GainName = Label(Specs, text = 'Gain Spec:')
GainName.grid(row=2,column=0,sticky=W)
GainText = Entry(Specs) ##Input Box for Value
GainText.grid(row=2,column=1,sticky=E)

##Spurs
SpurName = Label(Specs, text = 'In Band Spur Spec:')
SpurName.grid(row=3,column=0,sticky=W)
SpurText = Entry(Specs) ##Input Box for Value
SpurText.grid(row=3,column=1,sticky=E)

##Image Rejection
IRejName = Label(Specs, text = 'Image Rejection Spec:')
IRejName.grid(row=4,column=0,sticky=W)
IRejText = Entry(Specs) ##Input Box for Value
IRejText.grid(row=4,column=1,sticky=E)

##Lo Leakage Input
LO_InName = Label(Specs, text = 'LO Leakage In Spec:')
LO_InName.grid(row=5,column=0,sticky=W)
LO_InText = Entry(Specs) ##Input Box for Value
LO_InText.grid(row=5,column=1,sticky=E)

##Lo Leakage Output
LO_OutName = Label(Specs, text = 'LO Leakage Out Spec:')
LO_OutName.grid(row=6,column=0,sticky=W)
LO_OutText = Entry(Specs) ##Input Box for Value
LO_OutText.grid(row=6,column=1,sticky=E)

##P1dB
P1dBName = Label(Specs, text = 'P1dB Spec:')
P1dBName.grid(row=7,column=0,sticky=W)
P1dBText = Entry(Specs) ##Input Box for Value
P1dBText.grid(row=7,column=1,sticky=E)

##OIP3
OIP3Name = Label(Specs, text = 'OIP3 Spec:')
OIP3Name.grid(row=8,column=0,sticky=W)
OIP3Text = Entry(Specs) ##Input Box for Value
OIP3Text.grid(row=8,column=1,sticky=E)

##DC Current
DC_CName = Label(Specs, text = 'DC Current Spec:')
DC_CName.grid(row=9,column=0,sticky=W)
DC_CText = Entry(Specs) ##Input Box for Value
DC_CText.grid(row=9,column=1,sticky=E)

##S11
S11Name = Label(Specs, text = 'S11 Spec:')
S11Name.grid(row=10,column=0,sticky=W)
S11Text = Entry(Specs) ##Input Box for Value
S11Text.grid(row=10,column=1,sticky=E)

##S22
S22Name = Label(Specs, text = 'S22 Spec:')
S22Name.grid(row=11,column=0,sticky=W)
S22Text = Entry(Specs) ##Input Box for Value
S22Text.grid(row=11,column=1,sticky=E)

##Parameter Extra Note 1
Spec1Name = Label(Specs, text = 'Testing Note 1:')
Spec1Name.grid(row=12,column=0,sticky=W)
Spec1Text = Entry(Specs) ##Input Box for Value
Spec1Text.grid(row=12,column=1,sticky=E)

##Parameter Extra Note 1 Comment
SpNote1Name = Label(Specs, text = 'Testing Note 1 Comment:')
SpNote1Name.grid(row=12,column=2,sticky=W)
SpNote1Text = Entry(Specs) ##Input Box for Value
SpNote1Text.grid(row=12,column=3,sticky=E)

##Parameter Extra Note 2
Spec2Name = Label(Specs, text = 'Testing Note 2:')
Spec2Name.grid(row=13,column=0,sticky=W)
Spec2Text = Entry(Specs) ##Input Box for Value
Spec2Text.grid(row=13,column=1,sticky=E)

##Parameter Extra Note 2 Comment
SpNote2Name = Label(Specs, text = 'Testing Note 2 Comment:')
SpNote2Name.grid(row=13,column=2,sticky=W)
SpNote2Text = Entry(Specs) ##Input Box for Value
SpNote2Text.grid(row=13,column=3,sticky=E)

##Phase Noise Group

##10 Hz
P10Name = Label(Specs, text = 'Phase Noise 10Hz Spec:')
P10Name.grid(row=1,column=2,sticky=W)
P10Text = Entry(Specs) ##Input Box for Value
P10Text.grid(row=1,column=3,sticky=E)

##100 Hz
P100Name = Label(Specs, text = 'Phase Noise 100Hz Spec:')
P100Name.grid(row=2,column=2,sticky=W)
P100Text = Entry(Specs) ##Input Box for Value
P100Text.grid(row=2,column=3,sticky=E)

##1 kHz
P1kName = Label(Specs, text = 'Phase Noise 1kHz Spec:')
P1kName.grid(row=3,column=2,sticky=W)
P1kText = Entry(Specs) ##Input Box for Value
P1kText.grid(row=3,column=3,sticky=E)

##10 kHz
P10kName = Label(Specs, text = 'Phase Noise 10kHz Spec:')
P10kName.grid(row=4,column=2,sticky=W)
P10kText = Entry(Specs) ##Input Box for Value
P10kText.grid(row=4,column=3,sticky=E)

##100 kHz
P100kName = Label(Specs, text = 'Phase Noise 100kHz Spec:')
P100kName.grid(row=5,column=2,sticky=W)
P100kText = Entry(Specs) ##Input Box for Value
P100kText.grid(row=5,column=3,sticky=E)

##1 MHz
P1MName = Label(Specs, text = 'Phase Noise 1 MHz Spec:')
P1MName.grid(row=6,column=2,sticky=W)
P1MText = Entry(Specs) ##Input Box for Value
P1MText.grid(row=6,column=3,sticky=E)

##Ripple Spec Group

## Per 10MHz Ripple
P_10Name = Label(Specs, text = 'Per 10 MHz Ripple Spec:')
P_10Name.grid(row=7,column=2,sticky=W)
P_10Text = Entry(Specs) ##Input Box for Value
P_10Text.grid(row=7,column=3,sticky=E)

## Per 120MHz Ripple
P_120Name = Label(Specs, text = 'Per 120 MHz Ripple Spec:')
P_120Name.grid(row=8,column=2,sticky=W)
P_120Text = Entry(Specs) ##Input Box for Value
P_120Text.grid(row=8,column=3,sticky=E)

## Per 500MHz Ripple
P_500Name = Label(Specs, text = 'Per 500 MHz Ripple Spec:')
P_500Name.grid(row=9,column=2,sticky=W)
P_500Text = Entry(Specs) ##Input Box for Value
P_500Text.grid(row=9,column=3,sticky=E)

## Per 1000MHz Ripple
P_1GName = Label(Specs, text = 'Per 1000 MHz Ripple Spec:')
P_1GName.grid(row=10,column=2,sticky=W)
P_1GText = Entry(Specs) ##Input Box for Value
P_1GText.grid(row=10,column=3,sticky=E)

## Group designation for Notes##
Note = LabelFrame(master, text="Notes - Leave Empty if not required",padx=5,pady=5)
Note.grid(row=5,column=0)

## Note1
Note_1Name = Label(Note, text = 'Note 1:')
Note_1Name.grid(row=1,column=0,sticky=W)
Note_1Text = Entry(Note) ##Input Box for Value
Note_1Text.grid(row=1,column=1,sticky=E,ipadx = 200)

## Note1
Note_2Name = Label(Note, text = 'Note 2:')
Note_2Name.grid(row=2,column=0,sticky=W)
Note_2Text = Entry(Note) ##Input Box for Value
Note_2Text.grid(row=2,column=1,sticky=E,ipadx = 200)

## Note1
Note_3Name = Label(Note, text = 'Note 3:')
Note_3Name.grid(row=3,column=0,sticky=W)
Note_3Text = Entry(Note) ##Input Box for Value
Note_3Text.grid(row=3,column=1,sticky=E,ipadx = 200)







b1 = Button(master,text='Generate',command=gen).grid(row=7,column=3,sticky=E)



##master.after(500,updatevar)
mainloop()

