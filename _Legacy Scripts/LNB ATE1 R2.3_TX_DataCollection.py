#LNB ATE1 RX.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2022-04-21

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import tkinter as tk
from tkinter import ttk
import datetime
import socket
import numpy as np
import base64
import keysight.command_expert_py3 as kt
import logging
import matplotlib.pyplot as plt
import serial.tools.list_ports
import shutil

#Custom modules
import Datasheet_Generator_R8_TX as DS_Gen
import Orbital_Image_Filter_R5 as ImageFilter
import LNB_Communication_R10 as COMM
import ATE_Data_Collection_R2 as Collect

## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.47::inst0::INSTR'  
SA_IP = '10.0.10.47'               
SG_VISA = 'TCPIP0::10.0.10.95::inst0::INSTR'  
PM_VISA = 'USB0::0x0957::0x2E18::MY48100296::0::INSTR'                             
PS_VISA = 'USB0::0x0957::0x4D18::MY57510003::0::INSTR'         
VNA_VISA = 'TCPIP0::M9037A-45473669::hislip0::INSTR'
#VNA_VISA = 'PXI0::CHASSIS1::SLOT2::INDEX0::INSTR'
TEMPERATRUE_ADDRESS = ('10.0.10.169', 2000)

SLEEPYTIME = datetime.timedelta(seconds=0)

ATE_PATH = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1"
SETTINGS_PATH = '\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\ATE1_SETTINGS.csv'
VNA_PATH = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\_VNA_States"
PATHS_PATH = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\_PATH_Files\\"
CAL_PATH = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\_Calibration_Logs\\"
GUI_VALUES_PATH = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\_GUI_VALUES.json"


def main(folder_path):
    #Declaring dictionary to manage and store test results
    before = timestamp()[2]
    data = {'NFG': {
            'Frequency': [],'NF' : [],'Gain' : [],
            'Before_DUT_Loss_Table_Name': None, 'Before_DUT_Loss_Table' : None,
            'After_DUT_Loss_Table_Name': None,'After_DUT_Loss_Table' : None,
            'averageNF' : None,'min_NF' : None,'max_NF' : None,
            'averageGain' : None,'min_Gain' : None,'max_Gain' : None,
            'DSAdB': None, 'DSAhex': None
            },
            'P1dB': 
                {
                'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None, "P1dB_in": None, "OIP3_in": None
                }, 
            'phase':
                {
                '10Hz': None, '100Hz' : None, '1kHz' : None,'10kHz' : None, 
                '100kHz' : None, '1MHz' : None, '10MHz' : None, '100MHz' : None
                },
            'image_rej_input': None,'image_rej': None, 'spur_input':None, 'spur_pass': None, 
            'lo_leakage_out': None, 'lo_leakage_in': None, 'S11': None, 'S22': None,
            'Band_Switching': None,'Date': None, 'Time': None, 'Voltage': None, 'current': None, 'lo_lock': None,
            'Band': None, 'Script_Revision' : os.path.basename(__file__), 'Build_Info' : None, 'PLL_Build':None
            }

    #Obtaining user selected settings
    model_name = ModelDropdown.get()
    settings_list = DS_Gen.Find_Settings(settings_master_list, model_name, 'Name')
    if not settings_list:
        logger.warning("No Model Selected")
        return

    #tk.IntVar() to bool
    global test_list
    test_list = {}
    for key in tk_test_list.keys():
        test_list[key] = tk_test_list[key].get()

    GUI_Values = {
            "Name":settings_list[0]["Name"],
            "Model":settings_list[0]["Model"],
            "Orbital_SN": str(SNText.get()),
            "stock_number": str(StockText.get()),
            "Manufacturer_SN": str(MnfText.get()),
            "Customer": str(CxText.get()),
            "PO": str(POText.get()),
            "Color": str(ColorText.get()),
            "Tester": str(YouText.get()),
            "Serial_Port": filterPort(USB_Port_Text.get()),
            "Attenuation": Attenuation.get(),
            "IP": str(IP_Text.get()),
            "IP_Port": str(IP_Port_Text.get()),
            "DSA_Autotune": bool(Tune_DSA_EN.get()),
            "Test_List": test_list
        }

    SaveGUIvalues(GUI_VALUES_PATH,GUI_Values)
    
    #Obtaining info from GUI
    data.update(GUI_Values) 
    #Get equipment names and append them to data  
    data.update(Resources)

    if not data["Orbital_SN"]:
        data["Orbital_SN"] = 'No_Name' 

    #Check for band switching
    if not settings_list[0]['Band_Switching']: #Replace blank string with NoneType
        data['Band_Switching'] = None
    else:
        data['Band_Switching'] = settings_list[0]['Band_Switching']

    #Power on unit
    voltage = settings_list[0]['Voltage']
    delay = 10
    if(settings_list[0]['Startup_Delay'] != ''):
        delay = settings_list[0]['Startup_Delay']
    if(voltage):
        Pwr_on(voltage)  #Change input voltage to first band
        if(delay):
            logger.info(F"Power startup delay: {delay} seconds")
            time.sleep(int(delay))

    global coms
    coms = OpenCommunications(data)

    if(coms):
        data['Build_Info'], data['PLL_Build'] = TestCommunications(coms)

    #Save unit settings
    with open(F"{folder_path}//{data['Model']}_Settings.json",'w') as f:
        json.dump(settings_list,f)

    #Run ATE
    RunATE(settings_list,data,data["Orbital_SN"],folder_path)

    #close com port
    if(coms):
        coms.close()
        
    global SLEEPYTIME
    after = timestamp()[2]
    logger.debug(F"Time spent waiting on equipment and loading state files: {SLEEPYTIME}")
    logger.debug(F"Total test time: {after-before}")
    SLEEPYTIME = datetime.timedelta(seconds=0)
    logger.handlers.clear()

def RunATE(settings_list,data,name,folder_path):
    logger.info('Start ATE1 Test')

    NFG_Only = False
    if not (test_list['P1dB'] or test_list['Image_Rej'] or test_list['In_Band_Spur'] or test_list['LO_Leak_Input'] or test_list['Phase_Noise'] or test_list['S11'] or test_list['S22']):
        NFG_Only = True

    extra = False
    if(data['Model'] == 'Horizon'):
        extra = True

    #Get Date and Time
    data["Date"] = str(datetime.datetime.now().date())   
    data["Time"] = str(datetime.datetime.now().time())
     
    #Begin NFG Test
    band = 1
    input('Connect Noise Source to DUT. Press ENTER to continue.')
    logger.debug("Switched to Noise Source")
    for settings in settings_list:       
        data["Band"] = Change_Band(settings, data, coms, band)
        if(Resources["PS"]):
            if(test_list['Voltage']):
                data["Voltage"] = get_Voltage()
            if(test_list['Current']):
                data["current"] = get_Current()
        if(Resources["SA"]):
            if(test_list['LO_Lock'] or test_list['LO_Leak_Output']):
                data["lo_lock"], data["lo_leakage_out"], data['lo_leak_out_harmonics'], data['max_harmonic'] = get_lo_leakage(settings, name, folder_path, band, 'Output', extra)
            if(test_list['NFG']):
                SA.write('*CLS')
                time.sleep(2)
                data["NFG"] = get_NFG(settings, name, folder_path, band)
                if(data['NFG']['DSAdB']):
                    data['Attenuation'] = data['NFG']['DSAdB']

        #Create .json file for raw band data
        Save_JSON(folder_path, name, data, band)
        if(NFG_Only):
            #Generate band datasheet
            data['spur_pass'] = ""
            data['image_rej'] = int()
            data['lo_leakage_in'] = int()
            data['P1dB']['P1dB'] = int()
            data['P1dB']['OIP3'] = int()
            data['S11'] = int()
            data['S22'] = int()
            DS_Gen.Generate(folder_path, name, data, settings)
            pass
        band += 1 

    if not (NFG_Only):
        input('Connect Coax Cable to DUT. Press ENTER to continue.')
        logger.debug("Switched to input cable")
        #Begin Return Loss Testing
        S11 = None
        S22 = None
        if Resources['VNA']:
            if(test_list['S11']):
                S11 = get_vswr('S11', settings, name, folder_path, band)
            if(test_list['S22']):
                S22 = get_vswr('S22', settings, name, folder_path, band)
            band = 1
            for settings in settings_list:
                data = Load_JSON(folder_path, name, band)
                data['S11'] = S11
                data['S22'] = S22
                Save_JSON(folder_path, name, data, band)
                band += 1

        #Begin Other Testing
        band = 1       
        for settings in settings_list:
            data["Band"] = Change_Band(settings, data, coms, band)
            data = Load_JSON(folder_path, name, band) #Load BandX NFG data into current data dictionary  
            logger.info('Starting Band ' + str(band) + ' test')
            if(Resources["SG"] and Resources["SA"]):
                if(test_list['P1dB']):
                    data["P1dB"] = get_p1db(settings, name, folder_path, band, data)
                if(test_list['Image_Rej']):    
                    data['image_rej_input'], data["image_rej"] = get_image_rejection(settings, name, folder_path, band)
                #data["V_switch"] = get_V_Switch(settings)
                if(test_list['In_Band_Spur']):
                    data['spur_input'], data["spur_pass"], data["spur_num"] = get_spurs(settings, name, folder_path, band, extra)
                if(test_list['LO_Leak_Input']):
                    data["lo_leakage_in"], data['lo_leak_in_harmonics'] = get_lo_leakage(settings, name, folder_path, band, 'Input', extra)    
                if(test_list['Phase_Noise']):
                    if extra:
                        input("Switch Reference to 144MHz")
                        logger.debug(F'Switched to 144MHz ref')
                        Sleepy(20)
                    data["phase"] = get_phase(settings, name, folder_path, band)
                    if extra:
                        input("Switch Reference to 10MHz")
                        logger.debug(F'Switched to 10MHz ref')
                        Sleepy(20)
                                
            Save_JSON(folder_path, name, data, band)

            #Generate band datasheet
            try:  
                DS_Gen.Generate(folder_path, name, data, settings)
                logger.info(F"Generated Band {data['Band']} Datasheet")
                pass
            except Exception as e:
                logger.error("Could not gnerate datasheet", e)

            band += 1

    #Merge data
    try:
        DS_Gen.Merge(folder_path, name + ' Datasheet')
        logger.info("Merged Datasheets")
        pass
    except:
        logger.error('Could not merge datasheets')

    if(NFG_Only == False):
        try:
            Collect.DataCollection(folder_path,data['Model'],1)
            logger.info("Collecting Data")
        except:
            logger.error('Could not collect data')

    logger.info("ATE1 Test Complete")

def Save_JSON(folder_path, name, data, band):
    data_file = F'{folder_path}\\{name}_Band{band}.json'
    with open(data_file,'w') as f:
        json.dump(data,f)
    f.close()

def Load_JSON(folder_path, name, band):
    data_file = F'{folder_path}\\{name}_Band{band}.json'
    with open(data_file, 'r') as f:
        data = json.load(f)
    f.close()

    return data 

def setup_logger(name, log_file, formatter, stream_en):

    level = logging.DEBUG

    handler = logging.FileHandler(log_file)        
    handler.setFormatter(logging.Formatter(formatter))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    if(stream_en):
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(formatter))
        logger.addHandler(stream_handler)

    return logger

def Change_Band(settings, data, coms, band):
    logger.info(F"Changing Band to band {band}")
    
    if(str(data['Band_Switching']).upper().count("VOLTAGE")):
        Pwr_on(settings['Voltage'])  #Change input voltage depending on band
        if(settings['Startup_Delay']):
            logger.info(F"Change band delay: {settings['Startup_Delay']} seconds")
            time.sleep(int(settings['Startup_Delay']))

    if(Resources["OSC"]):
        if(settings['Band_Switching'].count('22k')):
            OSC.write(':WGEN1:FUNCtion SQUare')
            OSC.write(':WGEN1:FREQuency 22000')
            OSC.write(':WGEN1:VOLTage 0.8')
            OSC.write(':WGEN1:OUTPut 1')
            logger.debug("Set OSC ref to 22k")
            time.sleep(2)
        else:
            OSC.write(':WGEN1:OUTPut 0')
            logger.debug("Set OSC ref off")

    if(coms):
        #variable "band" is diplayed band (starting at 1) and is NOT band selection number for Firmware
        #If atten type == 1, TriKa and DKa. Bands start at 1 and DSA starts at 0
        #If atten type == 2, WBKaLNB variant. Bands start at 0 and are alligned with DSA
        fw_band = int(band) - 1
        band_offset = 0
        if(int(settings['Atten_Type']) == 1):
            fw_band += 1
            band_offset = 1
        messages = [F'$setst,{fw_band},*']
        if data['Attenuation'] != 'Disabled':
            DSA_hex = COMM.attenuation(int(settings["Atten_Type"]),float(data['Attenuation']),True)
            messages.append(F'$setda,{fw_band-band_offset},{DSA_hex},*')
        try:
            for message in messages:
                logger.debug(message)
                reply = coms.query(message)
                logger.debug(reply)
        except Exception as e:
            logger.error(e)
            logger.error('Failed to change unit band')
        time.sleep(2)

    return band
    
def OpenCommunications(data):
    coms = None

    if not data['Band_Switching']:
        return None

    method = data['Band_Switching'].upper()
    logger.info(F"Band Switching Method: {method}")

    try:
        if(method.count('VOLTAGE')):
            pass
        if(method.count('BDC')):
            pass
        if(method.count('CC')):
            pass
        if(method.count('RS')):
            if(data['Serial_Port']):
                if(method == 'RS232'):
                    coms = COMM.RS232_Connection(data["Serial_Port"])    
                else:
                    coms = COMM.RS485_Connection(data["Serial_Port"])       
            else:
                logger.error("Serial Port not specified on serial enabled unit")
                raise Exception
        if(method.count('IP')):
            if(data['IP'] and data['IP_Port']): 
                coms = COMM.Socket_Connection(data["IP"],int(data["IP_Port"]))
            else:
                logger.error("IP or Port not specified on IP enabled unit")
                raise Exception
    except Exception as e:
        logger.error(e)
        logger.error('***NO CONNECTION***\nUnable to establish communication channel with unit.\nPlease review connection settings and try again.')
        StopATE()

    return coms 

def TestCommunications(coms):
    reply = []
    build = None
    logger.info("Testing unit communications...")
    try:
        reply = coms.query('$build,1,*')
        if(reply):
            logger.info(F"Build: {reply}")
            build = reply
        else:
            raise Exception
    except Exception as e:
        logger.error(e)
        logger.error('***TEST COMMUNICATION FAILED***\nCommunication method failed to communicate with unit.\nPlease review connection settings and try again.')
        StopATE() 

    #Fast reply
    messages = ["$dbg,2,0,*","$dbg,1,3,*"]
    for info in messages:
        logger.debug(info)
        reply = coms.query(info)
        logger.debug(reply)

    #Slow replies
    default_delay = coms.delay
    coms.delay = 3.1 #3.07 seconds experimentally determined delay for $pll,10,3,0... (talk to Ben, Nov 23, 2022)

    pll = "$pll,3,0,0,*"
    logger.debug(pll)
    reply = coms.query(pll)
    logger.debug(reply)
    pll_build = reply

    messages = ["$dbg,3,0,*","$pll,10,0,0,3,*"]
    for command in messages:
        logger.debug(command)
        reply = coms.query(command) #Return list of 1 item (probably)
        line_data = reply[0].split('\r\n')
        for line in line_data:
            logger.debug(line)
        
    coms.delay = default_delay

    #Fast reply
    messages = ["$dbg,1,0,*","$dbg,2,253,*"]
    for info in messages:
        logger.debug(info)
        reply = coms.query(info)
        logger.debug(reply)
    
    return build, pll_build

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%m%d_%H%M%S")
    now[2] = current_time
    return now

def Get_Temp():
    data = []
    message1 = (b'*SRTC\r') ## Channel 1 Temp
    message2 = (b'*SRHC\r') ## Channel 2 Temp  
    #logger.info("Obtaining Room Temp for Before/After DUT Tables")
    try:
        ##socket 1
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(TEMPERATRUE_ADDRESS)
        # Send data

        #print 'sending "%s"' % message
        sock.sendall(message1)

        # Look for the response
        amount_received = 0
        amount_expected = len(message1)

        while amount_received < amount_expected:
            s1 = sock.recv(16)
            data.append(s1)
            amount_received += len(s1)
        ## Socket 2
        #print 'sending "%s"' % message
        sock.sendall(message2)

        # Look for the response
        amount_received = 0
        amount_expected = len(message2)

        while amount_received < amount_expected:
            s2 = sock.recv(16)
            data.append(s2)
            amount_received += len(s2)
    finally:
        sock.close()

    return data

def Pwr_on(voltage):
    if not voltage:
        return
    
    PS.write(':SOURce:VOLTage:LEVel:IMMediate:AMPLitude ' + str(voltage)) 
    time.sleep(0.1)
    PS.write(':OUTPut:STATe 1')
    logger.info(F'Set Voltage: {voltage}V')
    
def Pwr_off(): 
    PS.write(':OUTPut:STATe 0')

def Make_Folder(path, name): 
    if(name == ''):
        name == 'No_Name' 
    dst = F"{path}\\{name}"
    if(os.path.exists(dst)):
        number = 1
        while(1):
            new_name = F"{dst}_{number}"
            if not os.path.exists(new_name):
                dst = new_name
                break
            number += 1

    time.sleep(1)
    os.mkdir(dst)

    return dst

def SA_Screenshot(productNumber,tag,band,folder_path): 
    SA.write(":MMEMory:STORe:SCReen 'C:\\TEST.GIF\'")
    fetch_image_data = SA.query_binary_values("MMEM:DATA? 'C:\\TEST.GIF\'", datatype='B', is_big_endian=True) 
    filename = folder_path + '\\' + productNumber + "_Band" + str(band) + '_' + tag + '.PNG'   
    with open(filename, 'wb') as f:
        f.write(bytes(fetch_image_data))
    #deleting the image on the equipment
    SA.write(":MMEM:DEL 'C:\\TEST.GIF\'")
    SA.write("*CLS") 
    time.sleep(1)
    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(filename, True)
    logger.debug(F'Saved image: {filename}') 

def SA_Save_Data(productNumber,tag,band,folder_path): 
    SA.write(":MMEMory:STORe:RESults 'C:\\TEST.CSV\'")
    fetch_csv_data = SA.query_binary_values("MMEM:DATA? 'C:\\TEST.CSV\'", datatype='B', is_big_endian=True) 
    filename = folder_path + '\\' + productNumber + "_Band" + str(band) + '_' + tag + '.csv'   
    with open(filename, 'wb') as f:
        f.write(bytes(fetch_csv_data))
    #deleting the image on the equipment
    SA.write(":MMEM:DEL 'C:\\TEST.CSV\'")
    logger.debug(F'Saved data: {filename}') 
    time.sleep(1)   

def VNA_Screenshot(productNumber, tag, folder_path): 
    #Setup filename and folder path
    filename = folder_path + '\\' + productNumber + '_' + tag + '.PNG'
    temp_filename = r"C:\\temp.PNG"
    
    #saving a screenshot on the equipment
    logger.debug(F'save screenshot to equipment') 
    VNA.write(":MMEM:STOR:SSCR 'C:\\TEST.BMP\'")
    #fetching the image data in an array
    logger.debug(F'fetching the image data in an array') 
    fetch_image_data = VNA.query_binary_values("MMEM:TRAN? 'C:\\TEST.BMP\'", datatype='B', is_big_endian=True)

    #creating a file with the product number to write the image data to it
    logger.debug(F'creating a file with the product number to write the image data to it') 
    with open(temp_filename, 'wb') as f:
        f.write(bytes(fetch_image_data))

    #deleting the image on the equipment
    logger.debug(F'deleting the image on the equipment') 
    VNA.write(":MMEM:DEL 'C:\\TEST.BMP\'")

    #Include image processing for datasheet (greyscaling, inversion, etc)
    logger.debug(F'Include image processing for datasheet')
    ImageFilter.Filter(temp_filename, False)
    logger.debug(F'Saved image: {filename}')
    shutil.copy2(temp_filename, filename)
    

def VNA_Save_Data(productNumber, tag, folder_path): 
    #Setup filename and folder path
    filename = folder_path + '\\' + productNumber + '_' + tag + '.CSV'
    
    #saving a screenshot on the equipment
    VNA.write(F":MMEMory:STORe:CSV:FORMAT 'TEST.CSV'")
    #fetching the image data in an array
    fetch_csv_data = VNA.query_binary_values("MMEM:TRAN? 'TEST.CSV'", datatype='B', is_big_endian=True)

    #creating a file with the product number to write the image data to it
    with open(filename, 'wb') as f:
        f.write(bytes(fetch_csv_data))

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TEST.CSV'")
    logger.debug(F'Saved data: {filename}')

def Wait(resource, log): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
    global SLEEPYTIME
    time.sleep(4)
    resource.write('*OPC')
    log.debug("Waiting on resource...")
    before = timestamp()[2]
    flag = False
    
    while not flag: 
        time.sleep(2)
        
        flag = bool(int(resource.query('*ESR?')))
        current = timestamp()[2]
        temp = current - before 
        if temp.total_seconds() > 120:
            break
        
        
    after = timestamp()[2]
    duration = after - before
    SLEEPYTIME += duration
    log.debug(F"Time waited on resource: {duration}")
  
def NFG_Sweep(NFG, settings, interested_freq):

    SA.write('*CLS')
    SA.write('INIT:CONT 0')
    SA.write('INITiate:RESTart')
    Wait(SA, logger)
    
    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG["NF"] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    #Not working???
    i = interested_freq["start"]
    interested_NF = []
    while i <= interested_freq["stop"]:
        interested_NF.append(NFG["NF"][i])
        i += 1
    NFG["min_NF"] = min(interested_NF)
    NFG["max_NF"] = max(interested_NF)
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq["start"]
    LinearNF = []
    while i <= interested_freq["stop"]:
        try:
            LinearNF.append(10**(NFG["NF"][i]/10))
        except:
            LinearNF.append(0)
        i += 1
    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG["averageNF"] = 10 * math.log10(averageLinearNF)

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG["Gain"] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq["start"]
    interested_Gain = []
    while i <= interested_freq["stop"]:
        interested_Gain.append(NFG["Gain"][i])
        i += 1
    NFG["min_Gain"] = min(interested_Gain)
    NFG["max_Gain"] = max(interested_Gain)
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq["start"]
    LinearGain = [] 
    while i <= interested_freq["stop"]:
        try:
            LinearGain.append(10**(NFG["Gain"][i]/10))
        except:
            LinearGain.append(0)
        i += 1
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG["averageGain"] = 10 * math.log10(averageLinearGain)

    return NFG

def get_NFG(settings, name, folder_path, band): 
    
    NFG =  {
            'Frequency': [],'NF' : [],'Gain' : [],
            'Before_DUT_Loss_Table_Name': None, 'Before_DUT_Loss_Table' : None,
            'After_DUT_Loss_Table_Name': None,'After_DUT_Loss_Table' : None,
            'averageNF' : None,'min_NF' : None,'max_NF' : None,
            'averageGain' : None,'min_Gain' : None,'max_Gain' : None,
            'DSAdB': None, 'DSAhex': None
            }
        
    logger.info(F'Start Band {band} NFG test')
    
    #set NFG Path
    kt.run_sequence(PATHS_PATH + 'NFG Path')

    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + settings["nf_gain_state_file"] + "'")
    SA.write(':SENSe:NFIGure:AVERage:STATe OFF') #Stops an extra run of the NFG test after loading the state file and before loading the limits. Not sure why this works
    logger.debug(F"Load state: {settings['nf_gain_state_file']}")
    time.sleep(4)
    Wait(SA, logger)

    SA.write('*CLS')
    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} Noise Figure and Gain Test'")
    #Generate and Load Limit Lines if applicable
    logger.info('Genenerating Limit Lines...')
    upper, lower, gain = Get_Gain_Spec(settings['Gain_Spec'])
    logger.info(F"NF Limits:")
    logger.info(F"    Max: {settings['NF_Spec_Max']} dB")
    logger.info(F"Gain Limits:")
    logger.info(F"    Max: {upper} dB")
    logger.info(F"    Min: {lower} dB")
    limits =    [
                {'number' : 1, 'value': settings["NF_Spec_Max"]}, 
                {'number' : 3, 'value': upper}, 
                {'number' : 4, 'value': lower}
                ]
    start = settings['nf_gain_interested_start_Frequency']
    stop = settings['nf_gain_interested_stop_Frequency']

    for lim in limits:
        SA.write(F":CALC:LIM{lim['number']}:CLE")
        if(lim['value']):
            SA.write(F":CALCulate:LLINe{lim['number']}:DATA {start},{lim['value']},1,{stop},{lim['value']},1")
            SA.write(F":CALCulate:LLINe{lim['number']}:STATe 1")
        else:
            SA.write(F":CALCulate:LLINe{lim['number']}:STATe 0")

    #Load Loss Table
    SA.write('*CLS')
    NFG['Before_DUT_Loss_Table_Name'] = Load_Loss_Table('BEFore',settings['Before_DUT_Loss_Table_Name'])
    NFG['After_DUT_Loss_Table_Name'] = Load_Loss_Table('AFTer',settings['After_DUT_Loss_Table_Name'])

    SA.write('*CLS')
    room_temperature = float(Get_Temp()[1])
    logger.info(F'Room temperature: {room_temperature}C')
    SA.write(':SENSe:NFIGure:CORRection:TCOLd:USER:STATe ON')
    SA.write(F':SENSe:NFIGure:CORRection:TCOLd:USER:VALue {room_temperature} CEL')

    #Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:  
        NFG["After_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:  
        NFG["After_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG["Before_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG["Before_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    points = int(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])
    step = (freq_stop - freq_start) / (points - 1)

    #Saving frequency data
    NFG["Frequency"] = [(freq_start + step * x) for x in range(points)]

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG["Frequency"]):
        if (NFG["Frequency"][i] >= float(settings["nf_gain_interested_start_Frequency"])) and flag == 0:
            interested_freq["start"] = i
            flag = 1
        if (NFG["Frequency"][i] >= float(settings["nf_gain_interested_stop_Frequency"])):
            interested_freq["stop"] = i
            break
        i += 1

    #Tune DSA value if applicable
    if(coms and (('DSA' in settings['Note_4']) or Tune_DSA_EN.get())):
        NFG['DSAdB'], NFG['DSAhex'] = DSA_Tune(NFG, settings, band, interested_freq)
    
    #Start NFG Sweep  
    SA.write(':SENSe:NFIGure:AVERage:STATe ON')
    SA.write(":SENSe:NFIGure:AVERage:COUNt 1")
    logger.debug(F"NFIG Averaging ON: {bool(SA.query(':SENSe:NFIGure:AVERage:STATe?'))}")
    NFG = NFG_Sweep(NFG, settings, interested_freq)

    logger.info("-----------------------")
    logger.info(F"Band {band} Results:")
    logger.info("Noise Figure:")
    logger.info ("    Min: %f" %round(NFG["min_NF"],3))
    logger.info ("    Max: %f" %round(NFG["max_NF"],3))
    logger.info ("    Average: %f" %round(NFG["averageNF"],3))
    logger.info("Gain:")
    logger.info ("    Min: %f" %round(NFG["min_Gain"],3))
    logger.info ("    Max: %f" %round(NFG["max_Gain"],3))
    logger.info ("    Average: %f" %round(NFG["averageGain"],3))
    logger.info("Max Ripple:")
    
    #Gather gain ripple per bandwidth step
    ripple = ['10', '120', '500', '550', '700', '750', '800','900', '1000']
    for value in ripple:
        if(settings[F'Per_{value}M_Spec']):
            try:
                NFG[F"Ripple_{value}M"] = AmplitudeResponse(interested_freq["start"], interested_freq["stop"], NFG["Frequency"], NFG["Gain"], int(value) * 1000000)
                NFG[F"Ripple_{value}M_max"] = max(NFG[F"Ripple_{value}M"])
                logger.info(F"    {value}MHz: {round(NFG[F'Ripple_{value}M_max'],3)}")
            except:
                logger.warning(F"Ripple for {value}MHz not applicable. Please review band start and stop frequency in the settings")
    logger.info("-----------------------")
    
    #Scale state file for screenshot
    logger.debug('Auto scale windows')
    SA.write(F"DISPlay:NFIGure:TRACe:Y:SCALe:RLEVel:VALue NFIGure,{round(NFG['averageNF'],1)}")
    SA.write(F"DISPlay:NFIGure:TRACe:Y:SCALe:RLEVel:VALue GAIN,{round(NFG['averageGain'],0)}")
    SA.write("DISPlay:NFIGure:TRACe:Y:SCALe:RPOSition NFIGure,CENTer")
    SA.write("DISPlay:NFIGure:TRACe:Y:SCALe:RPOSition GAIN,CENTer")
    time.sleep(2)
    
    SA_Screenshot(name, 'NFG', band, folder_path)
    SA_Save_Data(name, 'NFG', band, folder_path)
    
    return NFG

def DSA_Tune(NFG, settings, band, interested_freq):

    logger.info(F'Start Band {band} DSA tune')
    
    attempt_limit = 4
    DSA_step = 0.5 / int(settings['Atten_Type']) # version 1 = 0.5dB/step, version 2 = 0.25dB/step
    band_offset = band - 1
    DSA = None
    setDSAHex = None
    message = F'$setda,{band_offset},*'
    logger.debug(message)
    try:
        reply = coms.query(message)
        logger.debug(reply)
        for item in reply:
            if item.count('SETDAR'):  
                hexDSA = item.split(',')[-2]
                setDSAHex = hexDSA
                DSA = float(COMM.attenuation(int(settings["Atten_Type"]),hexDSA,False))
        if(DSA == None):
            raise Exception
    except Exception as e:
        logger.error(e)
        logger.error(F'Failed to poll unit DSA for Band {band}')
        return  None, None      

    #Sweep NFG
    SA.write(":SENSe:NFIGure:AVERage:STATe 0")
    #SA.write(":SENSe:NFIGure:AVERage:COUNt 1")
    NFG = NFG_Sweep(NFG, settings, interested_freq)
    averageGain = NFG['averageGain']
    logger.debug(F'Current DSA: {DSA}dB')
    logger.debug(F'Average Gain: {averageGain}')
    
    attempts = 0 
    target_gain = float(settings['Gain'])
    while(averageGain > target_gain + DSA_step):
            if(attempts > attempt_limit):
                logger.error(F"Could not change DSA to have gain of {target_gain}dBm")
                logger.error(F"Tuning attempts > {attempt_limit}")
                logger.error("Please review unit or unit gain settings")
                return DSA, setDSAHex
            logger.debug(F'Tuning output power to target {target_gain}dBm...')
            gainoffset = averageGain - target_gain
            DSAchange = round(gainoffset / DSA_step) * DSA_step
            logger.debug(F"Difference between Avereage Gain and Target Gain: {DSAchange}dB")
            if(DSAchange < DSA_step):
                break
            DSA += DSAchange
            logger.debug(F"New DSA Setting: {DSA}dB")
            setDSAHex = COMM.attenuation(int(settings["Atten_Type"]),float(DSA),True)
            message = F'$setda,{band_offset},{setDSAHex},*'
            logger.debug(message)
            try:
                reply = coms.query(message)
                logger.debug(reply)
            except Exception as e:
                logger.error(e)
                logger.error(F'Failed to poll unit DSA for Band {band}')
                return
    
            #Sweep NFG
            NFG = NFG_Sweep(NFG, settings, interested_freq)
            averageGain = NFG['averageGain']

            attempts += 1

    if(attempts == 0 and averageGain < target_gain):
        logger.info(F"Unit average gain at lowest DSA setting cannot reach target gain. Cannot tune DSA to target gain {target_gain}dBm")

    logger.info(F"Average Gain is {averageGain}dBm. Stopping DSA adjustment")

    return DSA, setDSAHex

def Get_Gain_Spec(spec):
    if not spec:
        return None

    #Split spec string to list of numbers
    temp = ''
    numbers = []
    spec += ' '
    for character in spec:
        if character.isdigit() or character == '.':
            flag = True
            temp += character
        elif(flag):
            numbers.append(float(temp))
            temp = ''
            flag = False

    #Interpret list of numbers       
    gain = None
    lower = None
    upper = None
    if(len(numbers) > 2): #Specific Spec ex:60(+3,-1)
        gain = numbers[0]
        upper = gain + numbers[1]
        lower = gain - numbers[2]
    elif(len(numbers) > 1):
        if(numbers[1] > 20): #Range Spec ex:55-65
            lower = numbers[0]
            upper = numbers[1]
            gain = (lower + upper) / 2 
        else: #General Spec ex:55 +/-1
            gain = numbers[0]
            lower = gain - numbers[1]
            upper = gain + numbers[1]  
    else: #No Spec
        gain = numbers[0]

    return upper, lower, gain

def Load_Loss_Table(location, table_name):
    if not table_name:
        return None
    try:
        SA.write(F':MMEM:LOAD:LOSS {location}, "{State_path}{table_name}”')
        logger.info(F"Loaded Loss Table: {table_name}")
    except:
        table_name = F'Error: {location} Loss table {State_path}{table_name} not found'
        logger.error(table_name)

    time.sleep(0.1)

    return table_name

def Generate_LIM(number, lim_type, freq_start, freq_stop, gain): #If intermediate file is necessary
    original = "ATE1_AUTO_LIMIT_TEMPLATE.LIM"
    target = F"AUTOLIM{number}.LIM"

    shutil.copy2(original, target)

    time.sleep(0.1)

    with open(target, "a") as f:
        f.write(F"[Limittype {lim_type}]\n")
        f.write(F'{freq_start}, {gain}, 1\n')
        f.write(F'{freq_stop}, {gain}, 1\n')

    time.sleep(0.1)

    ftp = FTP(SA_IP)
    ftp.login('vsa', 'service')
    ftp.cwd('/cfs/e4440a/userdir/')
    if target in ftp.nlst():
        ftp.delete(target)
    with open(os.getcwd() + '\\' + target, 'rb') as file:
        ftp.storbinary('STOR '+ target, file)
    time.sleep(0.1)
    ftp.quit()

    logger.debug(F"Auto Generated Limit: {target}")

    return target

def Load_Loss_Table(location, table_name):
    if not table_name:
        return None
    try:
        SA.write(F':MMEM:LOAD:LOSS {location}, “{table_name}”')
        logger.debug(F"Load Loss Table: {table_name}")
    except:
        table_name = F'Loss table "{location}" not found'
        logger.error(table_name)

    time.sleep(0.1)

    return table_name

def Load_Limit_Line(i, limit):
    if not limit:
        return None
    try:
        SA.write(F':MMEMory:LOAD:LIMit LLINE{i},"C:{limit}"')
        SA.write(F':CALCulate:NFIGure:LLINe{i}:DISPlay:STATe 1')
        SA.write(F':CALCulate:NFIGure:LLINe{i}:STATe 1')
        SA.write(F':CALCulate:NFIGure:LLINe{i}:TEST:STATe 1')
        logger.debug(F"Load Limit: {limit}")
    except:
        limit = F'Error: Limit line {i} not found'
        logger.error(limit)

    return limit

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def get_p1db(settings, name, folder_path, band, data):

    logger.info(F'Start P1dB band {band} test')  
    
    powerMeterMax = 20
    steps = 30
    fixed_offset = -0.5 # IP1dB = (Measured_OP1dB) - (NFG_Gain - shift). Gives us closer value to RF lab results (determined experimentally)
    
    kt.run_sequence(PATHS_PATH + 'P1dB Path') ##set P1dB Path
     
    PM.write(':INITiate1:CONTinuous %d' % (1)) #triggering the power meter to take measurements continuosly 

    start_gain = float(settings['Gain'])
    stop_gain = float(settings['Gain'])
    mid_gain = float(settings['Gain'])
    if(data['NFG']['Gain']): #If NFG data available, use specific gain at frequency point
        start_gain = float(data['NFG']['Gain'][0])
        stop_gain = float(data['NFG']['Gain'][-1])
        mid_gain = float(data['NFG']['Gain'][int(len(data['NFG']['Gain'])/2)])

    start_freq = int(settings["nf_gain_interested_start_Frequency"])
    stop_freq = int(settings["nf_gain_interested_stop_Frequency"])
    mid_freq = (start_freq + stop_freq) / 2

    interested_freq = [(start_freq,start_gain), (mid_freq,mid_gain), (stop_freq,stop_gain)]

    results = {'Data': [], 'P1dB' : None, 'OIP3' : None, "IP1dB": None, "IIP3": None}

    for freq, gain in interested_freq:

        trace = {
                    "Frequency": freq, 'Ave_Gain': None, 'NFG_Gain': None,  
                    'Input' : [] , 'Input_adjusted' : [], 'Output' : [], 'Gain_Compression': [],
                    'P1dB' : None, 'OIP3' : None, "IP1dB": None, "IIP3": None
                }


        SG.SetFreq(float(settings["LO"]) + freq*float(settings['USB_LSB'])) #setting the signal generator frequency 
        PM.write(':SENSe:FREQuency:FIXed %G' % (freq))
        
        start_power = int(gain) * -1 #Start at about 0dBm @ Output of LNB
        input_power = start_power
        stop_power = start_power + steps #End at 30dB higher than the initial input
        gain_list = []

        while input_power < stop_power:
            SG_power = input_power + float(settings["p1db_cable_loss"]) 
            if not SG_power_check(SG_power, settings):
                logger.warning(F"Break from P1dB measurements: Input power exceed unit safety. Input power = {SG_power}")
                break
            if(input_power == start_power):
                SG.Output(True)
            time.sleep(0.4)    
            output_power = float(PM.query(':FETCh1?')) + float(settings["p1db_muxT_and_cable_loss"]) #fetch measurement, compensate for loss, and add it to P1dB["Output"] list
            if(output_power < -10): #correct outliers
                if(trace["Output"] == []):
                    output_power = 0.1234
                else:
                    output_power = trace["Output"][-1] + 1
            gain_list.append(output_power - input_power)
            trace["Input"].append(input_power)
            trace["Output"].append(output_power) 
            if(output_power >= powerMeterMax):
                logger.warning(F"Break from P1dB measurements: Output power exceed power meter safety. Output power = {output_power}")
                break

            input_power += 1

        SG.Output(False)

        
        average_gain = np.average(gain_list[0:int(len(gain_list)/2)])
        trace["Ave_Gain"] = average_gain
        trace['NFG_Gain'] = gain
        offset = gain - average_gain
        flag = True
        for i, v in enumerate(trace['Output']):
            input_adjusted = trace['Input'][i] - offset - fixed_offset
            trace["Input_adjusted"].append(input_adjusted)
            current_gain = trace['Output'][i] - input_adjusted
            difference = gain - current_gain # 1 < difference < 2.
            trace['Gain_Compression'].append(current_gain - gain)
            overshoot = difference - 1 # 0 < overshoot < 1

            if(difference > 1 and flag): #If the difference between the average gain and the current gain is greater than 1dB, 1dB compression point found.
                flag = False
                trace["P1dB"] = output_power - overshoot #Record P1dB, adjusting for overshoot
                trace["OIP3"] = trace["P1dB"] + 10 #P1dB + 10 = OIP3
                trace["IP1dB"] = input_adjusted - overshoot #Record IP1dB, adjusting for overshoot
                trace["IIP3"] = trace["IP1dB"] + 10 #P1dB + 10 = OIP3
                if(results['P1dB'] == None or trace["P1dB"] < results['P1dB']): #Record min value
                    results["P1dB"] = trace["P1dB"]
                    results["OIP3"] = trace["OIP3"]
                    results["IP1dB"] = trace["IP1dB"]
                    results["IIP3"] = trace["IIP3"]

        logger.debug(F'P1dB @ {float(freq/1000000000)}GHz')
        logger.debug('  P1dB: ' + str(trace["P1dB"]))
        logger.debug('  OIP3: ' + str(trace["OIP3"]))
        if(settings['IP1dB_Spec']):
            logger.debug('  IP1dB: ' + str(trace["IP1dB"]))
            logger.debug('  IIP3: ' + str(trace["IIP3"]))

        results['Data'].append(trace)

    logger.info(F'Results:')
    logger.info(F'---------------:')  
    logger.info('   P1dB: ' + str(results["P1dB"]))
    logger.info('   OIP3: ' + str(results["OIP3"]))
    if(settings['IP1dB_Spec']):
        logger.info('   IP1dB: ' + str(results["IP1dB"]))
        logger.info('   IIP3: ' + str(results["IIP3"]))

    try:
        P1dB_Plot(results, folder_path, name, band)
        P1dB_Gain_Compression_Plot(results, folder_path, name, band)
    except:
        logger.warning('Could not generate P1dB plot')

    return results

def P1dB_Plot(results, folder_path, name, band):
    plt.clf()
    plt.cla()
    for trace in results['Data']:
        plt.plot(trace['Input_adjusted'],trace['Output'], '-', linewidth=3, label=F"{float(trace['Frequency'])/1000000000}GHz")
        if(trace['P1dB']):
            plt.plot(trace['IP1dB'],trace['P1dB'],'ko')
    if(results["P1dB"]):
        plt.plot(results['IP1dB'],results['P1dB'],'ro')
        plt.text(results['IP1dB']+2,results['P1dB']-2,F"({round(results['IP1dB'],2)},{round(results['P1dB'],2)})",color='red')
        plt.axhline(y=results['P1dB'], color="red", linestyle="--")
        plt.axvline(x=results['IP1dB'], color="red", linestyle="--")
    plt.title(F"{name} Band {band} P1dB Plot")
    plt.ylabel('Output Power (dBm)')
    plt.xlabel('Input Power (dBm)')
    plt.legend(loc='lower right')
    filename = F"{folder_path}\\{name}_Band{band}_P1dB.png"
    plt.savefig(filename,dpi=200)
    ImageFilter.Filter(filename, False)
    logger.debug(F'Saved image: {filename}') 

def P1dB_Gain_Compression_Plot(results, folder_path, name, band):
    plt.clf()
    plt.cla()
    for trace in results['Data']:
        plt.plot(trace['Input_adjusted'],trace['Gain_Compression'], '-', linewidth=3, label=F"{float(trace['Frequency'])/1000000000}GHz")
    if(results["P1dB"]):
        plt.axvline(x=results['IP1dB'], color="red", linestyle="--")
        plt.axhline(y=-1, color="red", linestyle="--")
    plt.title(F"{name} Band {band} P1dB Compression Plot")
    plt.ylabel('Gain Compression (dB)')
    plt.xlabel('Input Power (dBm)')
    plt.legend(loc='lower left')
    filename = F"{folder_path}\\{name}_Band{band}_P1dB_Compression.png"
    plt.savefig(filename,dpi=200)
    ImageFilter.Filter(filename, False)
    logger.debug(F'Saved image: {filename}') 

def get_phase(settings, name, folder_path, band):

    phase = {
            '10Hz': None, '100Hz' : None, '1kHz' : None, 
            '10kHz' : None, '100kHz' : None, '1MHz' : None, 
            '10MHz': None, '100MHz': None
            }
    
    logger.info(F'Start band {band} Phase Noise test')
    
    carrier = int(settings["nf_gain_interested_start_Frequency"])
    
    ##setting IPLS Path
    kt.run_sequence(PATHS_PATH + 'IPLS Path')

    #Loading state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + str(settings["phase_noise_state_file"])+"'")
    logger.debug(F"Load state: {settings['phase_noise_state_file']}")
    time.sleep(1)
    Wait(SA, logger)

    time.sleep(1)
    
    #setting the signal generator frequency
    SG.SetFreq(float(settings['LO']) + carrier*float(settings['USB_LSB']))
   
    power = ((float(settings["Gain"])) * -1) + float(settings["phase_noise_loss"])
    if not SG_power_check(power, settings):
        return None
    SG.Output(True)
    time.sleep(2)

    powerIn = Find_Carrier_Level(settings)
    #Start Phase Sweep
    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} Phase Noise Test'")
    SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:IMMediate')
    Wait(SA, logger)
    if(settings['Model'] == 'Horizon'):
        time.sleep(30)
    #Gather phase noise markers
    SA.write('CALC:LPLot:MARK1:X 10 hz')
    phase["10Hz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    SA.write('CALC:LPLot:MARK1:X 100 hz')
    phase["100Hz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    SA.write('CALC:LPLot:MARK1:X 1 khz')
    phase["1kHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    SA.write('CALC:LPLot:MARK1:X 10 khz')
    phase["10kHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    SA.write('CALC:LPLot:MARK1:X 100 khz')
    phase["100kHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    SA.write('CALC:LPLot:MARK1:X 1000 khz')
    phase["1MHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    try: #I dont know how this will behave - will it return zero or cause equipment timeout? (I hope it returns zero)
        SA.write('CALC:LPLot:MARK1:X 10000 khz')
        phase["10MHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
        SA.write('CALC:LPLot:MARK1:X 100000 khz')
        phase["100MHz"] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    except:
        pass

    logger.info(F"Phase Noise Results:")
    for key, value in phase.items():
        logger.info(F"   {key} : {value}")

    #To save data into json file
    phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot3?')
    phasesmoothed = SA.query_ascii_values(':FETCH:LPlot4?') 
    phase['SMOOTH'] = phasesmoothed #Gather measured phase noise smoothed data over number of points from a log scale, unit:dBc/Hz
    
    SA_Screenshot(name, 'Phase_Noise', band, folder_path)
    SA_Save_Data(name, 'Phase_Noise', band, folder_path)

    SG.Output(False)

    return phase

def get_image_rejection(settings, name, folder_path, band):
    logger.info(F'Start band {band} Image Rejection test')

    kt.run_sequence(PATHS_PATH + 'IPLS Path')
    
    resolution_bandwidth = 10
    span = 20000
    signal_freq = int(settings["nf_gain_interested_start_Frequency"])

    #loading state file
    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'" + settings["image_rejection_state_file"] + "'")
    logger.debug(F"Load state: {settings['image_rejection_state_file']}")
    Wait(SA,logger)

    #setting the signal generator frequency
    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} Image Rejection Test'")
    SG.SetFreq(float(settings['LO']) + signal_freq*float(settings['USB_LSB'])) 
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    SA.write(F'SENSe:FREQuency:SPAN {span} Hz')
    SA.write(F":SENSe:BANDwidth {resolution_bandwidth} Hz")
    time.sleep(3)
    
    #Adjust SG so Output power is 0dBm
    powerIn = Locate_0dBm_Output(settings)

    #setting the frequency on the signal generator to image rejection frequency to get the image rejection value
    SG.SetFreq(float(settings['LO']) - signal_freq*float(settings['USB_LSB']))
    
    time.sleep(1)
    SA.write(':INITiate:IMMediate')
    time.sleep(3)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    imageRejection = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    #imageRejection += float(settings["image_rejection_cable_loss"])
    logger.info("Image Rejection: %f" %imageRejection)
    
    #Take screenshot
    SA_Screenshot(name,'Image_Rejection', band, folder_path)
    
    #Turn off Signal Generator
    SG.Output(False)

    return powerIn,imageRejection

def get_spurs(settings, name, folder_path, band, extra): 
    logger.info(F'Start Band {band} In-Band Spur test')

    kt.run_sequence(PATHS_PATH + 'IPLS Path')

    resolution_bandwidth = 5000
    bound_width = 50000000
    limit_start = int(settings['nf_gain_interested_start_Frequency'])
    limit_stop = int(settings['nf_gain_interested_stop_Frequency'])
    signal_freq = int(settings["nf_gain_interested_start_Frequency"]) - (bound_width / 2)
    cable_loss = float(settings["in_band_spur_cable_loss"])
    limit = float(settings['in_band_spur_limit'])

    #loading state file
    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'" + settings["in_band_spur_state_file"] + "'")
    logger.debug(F"Load state: {settings['in_band_spur_state_file']}")
    Wait(SA,logger)
    time.sleep(5)

    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} In-Band Spur Test'")
    
    #Change bandwidth + bounds
    SA.write(F":SENSe:FREQuency:START {limit_start - bound_width}")
    SA.write(F":SENSe:FREQuency:STOP {limit_stop + bound_width}")
    SA.write(F":SENSe:BANDwidth {resolution_bandwidth} Hz")
    SG.SetFreq(float(settings['LO']) + signal_freq*float(settings['USB_LSB']))
    
    #Generate Limit, ignoring harmonics
    flag = True
    harmonic_spacer = 10 * 1000000
    limit_string = F"{limit_start},{limit},1,"
    harmonic = 2
    check_harmonic = 5
    while(harmonic <= check_harmonic):
        harmonic_freq = harmonic * signal_freq
        if(harmonic_freq > limit_stop):
            break
        elif(harmonic_freq == limit_stop):
            limit_string += F"{harmonic_freq - harmonic_spacer},{limit},1"
            flag = False
        else:
            limit_string += F"{harmonic_freq - harmonic_spacer},{limit},1,{harmonic_freq + harmonic_spacer},{limit},0,"
        harmonic += 1
    if(flag):
        limit_string += F"{limit_stop},{limit},1"
    
    SA.write("*CLS")
    logger.debug(limit_string)
    SA.write(F":CALCulate:LLIN:STATe 1") #Off for now
    SA.write(F":CALCulate:LLIN:DISPlay 1") #Off for now
    SA.write(F":CALCulate:LLIN:DATA {limit_string}")
    #logger.debug(F"LLine State: {SA.query(F':CALCulate:LLIN:STATe?')}")
    #logger.debug(F"LLine Display: {SA.query(F':CALCulate:LLIN:DISPlay?')}")
    #logger.debug(F"LLine Display: {SA.query(F':CALCulate:LLIN:DATA?')}")
    #logger.debug(SA.query('SYSTem:ERRor?'))
    time.sleep(3)
    #input("Pause")

    #Adjust SG so Output power is 0dBm
    powerIn = Locate_0dBm_Output(settings)
    #Horizon wants this to be done at 12 dBm
    if extra == True:
        powerIn48 = powerIn + 13
        if not SG_power_check(powerIn48, settings):
            return powerIn
        SG.Output(True)
    
    SA.write("*CLS")
    SA.write(':INITiate:IMMediate')
    Wait(SA,logger)
    #sweepTime = float(SA.query(':SENSe:SWEep:TIME?'))
    #time.sleep(sweepTime)
    time.sleep(3)
    print(SA.query(':CALCulate:LLINe1:FAIL?'))
    LLine_result = not bool(int(SA.query(':CALCulate:LLINe1:FAIL?'))) #SCPI return 1 for fail, 0 for pass
    SA.write(':CALCulate:MARKer1:MAXimum')
    IFpower = float(SA.query(':CALCulate:MARKer1:Y?'))
    print(IFpower)
    result = "Fail"
    if(LLine_result):
        result = "Pass"

    time.sleep(1)
    result_num = 'None'
    if extra == True:
        SA.write(':CALCulate:MARKer1:PEAK:THReshold -65')
        print(SA.query(':CALCulate:MARKer1:PEAK:THReshold?'))
        SA.write(':CALCulate:MARKer1:MAXimum:RIGHt')
        result_num = float(SA.query(':CALCulate:MARKer1:Y?')) - IFpower
        while(result_num >= -25):
            SA.write(':CALCulate:MARKer1:MAXimum:RIGHt')
            result_num = float(SA.query(':CALCulate:MARKer1:Y?')) - IFpower
        time.sleep(4)
        print(SA.query(':CALCulate:MARKer1:PEAK:THReshold?'))
        SA.write(':CALCulate:MARKer1:PEAK:THReshold -90')
        
    #Take screenshot
    SA_Screenshot(name,'In_Band_Spurs', band, folder_path)
    
    #Turn off Signal Generator
    SG.Output(False)
    SA.write(F":CALCulate:LLINe1:DISPlay OFF")
    SA.write(F":CALCulate:LLINe1:STATe OFF")
    SA.write(":SENSe:SWEep:TYPE AUTO")
    logger.info(F'In-Band Spur test: {result}')

    return powerIn, result, result_num

def Locate_0dBm_Output(settings):
    zeroDbTolerance = 1
    tuning_attempt_limit = 2
    inital_power_limit = 15

    #Set Initial Guess
    powerIn = (float(settings["Gain"])) * -1
    if not SG_power_check(powerIn, settings):
        return powerIn
    SG.Output(True)
    
    #Sweep
    SA.write(':INITiate:IMMediate')  
    time.sleep(3)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    
    #verifying if the measurement is close to 0dB
    if(abs(measurement) > inital_power_limit):
        logger.error("Could not calibrate output signal power to 0dBm")
        logger.error(F"Initial output power > {inital_power_limit}dB from 0dBm")
        logger.error("Please review unit or unit gain settings")
        return powerIn
    elif(abs(measurement) > zeroDbTolerance):
        logger.debug(F'Output signal power not within 0dBm +/-{zeroDbTolerance}')
        logger.debug(F'Adjusting Signal Generator input by {abs(measurement)}dBm.') 
        powerIn = powerIn - float(measurement)
        if not SG_power_check(powerIn, settings):
            return powerIn
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(3)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))

        #If the SA measurement is still no 0dBm, finely tune:
        attempts = 0
        while(abs(measurement) > zeroDbTolerance):
            logger.warning('Tuning output power to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn = powerIn - 1
            elif(measurement < zeroDbTolerance):
                  powerIn = powerIn + 1
            if(attempts > tuning_attempt_limit):
                logger.error("Could not calibrate output signal power to 0dBm")
                logger.error(F"Tuning attempts > {tuning_attempt_limit}")
                logger.error("Please review unit or unit gain settings")
                return powerIn
            if not SG_power_check(powerIn, settings):
                return powerIn
            time.sleep(1)
            SA.write(':INITiate:IMMediate')
            time.sleep(3)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            attempts += 1

    return powerIn

def Find_Carrier_Level(settings):
    zeroDbTolerance = 1.5
    tuning_attempt_limit = 1
    inital_power_limit = 20
    pn_carrier_level = float(settings["pn_carrier_level"])
    powerIn = ((float(settings["Gain"])) * -1) + float(settings["phase_noise_loss"])
    #Find carrier level
    
    #SA.write(":SENSe:LPLot:AVERage:STATe 0")
    time.sleep(1)
    
    #Check carrier power
    #SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write(':SENSe:FREQuency:CARRier:SEARch')
    
    #Wait(SA)  
    time.sleep(50)

    measurement = SA.query(':FETCh:LPLot1?')
    end = measurement.find(',')
    carrierp = float(measurement[0:end])
    logger.info(F"Carrier Power is {carrierp}dB ")    
     
    #verifying if the measurement is close to 0dB
    if(abs(carrierp) > inital_power_limit):
        logger.error(F"Could not calibrate output signal power to {pn_carrier_level}dBm")
        logger.error(F"Initial output power > {inital_power_limit}dB from {pn_carrier_level}dBm")
        logger.error("Please review unit or unit gain settings")
        return powerIn
    elif(abs(carrierp-pn_carrier_level) > zeroDbTolerance):
        logger.info(F'Output signal power not within 0dBm +/-{zeroDbTolerance}')
        logger.info(F'Adjusting Signal Generator input by {carrierp}dBm.') 
        powerIn = powerIn - float(carrierp)+ pn_carrier_level
        if not SG_power_check(powerIn, settings):
            return powerIn
        time.sleep(1)

        SA.write(':SENSe:FREQuency:CARRier:SEARch')
        #Wait(SA)
        time.sleep(50)
        measurement = SA.query(':FETCh:LPLot1?')
        end = measurement.find(',')
        carrierp = float(measurement[0:end])
        
        #If the SA measurement is still not 0dBm, finely tune:
        if(settings['Model'] == 'Horizon'):
            attempts = 0
            while(abs(abs(carrierp)-pn_carrier_level) > zeroDbTolerance):
                logger.warning(F'Tuning output power to {pn_carrier_level}dBm...')
                if(carrierp-pn_carrier_level > zeroDbTolerance):
                      powerIn = powerIn - 3
                elif(carrierp-pn_carrier_level < zeroDbTolerance):
                      powerIn = powerIn + 3
                if(attempts > tuning_attempt_limit):
                    logger.error(F"Could not calibrate output signal power to {pn_carrier_level}dBm")
                    logger.error(F"Tuning attempts > {tuning_attempt_limit}")
                    logger.error("Please review unit or unit gain settings")
                    return powerIn
                if not SG_power_check(powerIn, settings):
                    return powerIn
               
                SA.write(':SENSe:FREQuency:CARRier:SEARch')
                time.sleep(50)
                #Wait(SA)
                measurement = SA.query(':FETCh:LPLot1?')
                end = measurement.find(',')
                carrierp = float(measurement[0:end])
                

                time.sleep(2)
                attempts += 1

    return powerIn

def get_lo_leakage(settings, name, folder_path, band, test, harmonic_test): 

    resolution_bandwidth = 3
    span = 500
    freq_threshold = 20
    ref_lvl = -50

    LO_lock = None
    LO_Leak = None
    maxharmonic = 0
    
    logger.info(F'Start Band {band} {test} LO Leakage test')
    if(test == 'Input'):
        kt.run_sequence(PATHS_PATH + F"Input Leak Path")
    else:
        kt.run_sequence(PATHS_PATH + 'IPLS Path')

    cable_loss_list = [float(x) for x in settings[F"lo_leakage_{test.lower()}_cable_loss"].split(",")]
    
    #Load state file
    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'"  + settings["lo_leakage_state_file"] + "'")
    logger.debug(F"Load state: {settings['lo_leakage_state_file']}")
    time.sleep(1)
    Wait(SA,logger)

    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} {test} LO Leakage Test'")
    SA.write(F"DISPlay:WINDow:TRACe:Y:SCALe:RLEVel {ref_lvl} dbm")
    SA.write(F"SENSe:BANDwidth {int(settings['lo_bw'])} Hz")
    SA.write(F'SENSe:FREQuency:SPAN {int(settings["lo_span"])} Hz')

    harmonics = [1]
    if(harmonic_test):
        harmonics += [2,4]
    result = []
    if(len(harmonics) != len(cable_loss_list)):
        i = 0
        while i < (len(harmonics)-len(cable_loss_list)):
            cable_loss_list.append(0)
    for index,harmonic in enumerate(harmonics):
        center_freq = int(settings['LO']) / harmonic
        SA.write(F"SENSe:FREQuency:CENTer {center_freq}")
        SA.write(':INITiate:IMMediate')
        #sweepTime = float(SA.query(':SENSe:SWEep:TIME?'))
        time.sleep(10) #Under 4 seconds may not work
        SA.write(':CALCulate:MARKer1:MAXimum')
        output_freq = float(SA.query(':CALCulate:MARKer1:X?'))
        output = SA.query(':CALCulate:MARKer1:Y?')
        corrected_result = float(output) + cable_loss_list[index]
        if corrected_result > maxharmonic or maxharmonic == 0:
            maxharmonic = corrected_result
        d = {"Harmonic": harmonic, "Frequency": output_freq, "Power": corrected_result}
        result.append(d)

        if(harmonic == 1 and test == 'Output'):
            if(abs(int(settings['LO'])-int(output_freq)) < freq_threshold): 
                LO_lock = True
            else:
                LO_lock = False

            logger.info(F'LO Lock: {LO_lock}')

        logger.debug(F"LO Leakage {test}: {d}")

        if(harmonic > 1):
            SA_Screenshot(name, F'LO_Leakage_{test}_{harmonic}_harmonic', band, folder_path)
        else:
            LO_Leak = corrected_result
            SA_Screenshot(name, F'LO_Leakage_{test}', band, folder_path)

        time.sleep(5)

    SA.write('INITiate:CONTinuous 0')

    if(test == 'Input'):
        logger.info(F"LO Leakage {test}: {LO_Leak}")
        return LO_Leak, result

    else:
        return LO_lock, LO_Leak, result, maxharmonic

def get_Current(): 
    time.sleep(1) #Added delay
    dcCurrent = PS.query_ascii_values(':SOURce:SENSe:CURRent:LEVel?')[0]
    dcCurrent = round(dcCurrent*float(1000),1)
    logger.info("Measured Current: %f" %(dcCurrent))

    return dcCurrent

def get_Voltage(): 
    voltage = round(float(PS.query('SOURce:SENSe:VOLTage:LEVel?')),1)
    logger.info("Measured Voltage: %f" %(voltage))

    return voltage

def get_vswr(test, settings, name, folder_path, band): 
    logger.info(F"Start {test} Return Loss test")
    
    ##setting Path
    kt.run_sequence(PATHS_PATH + F'{test} Path')
    time.sleep(1)

    path = F"{VNA_PATH}\\{settings[F'{test}State']}"

    VNA.write("*CLS")
    VNA.write("MMEM:LOAD '%s'"%path )
    logger.debug(F"Load state: {settings[F'{test}State']}")
    Wait(VNA,logger)
    
    VNA.write("OUTP 1")
    time.sleep(2)
    VNA.write(F"CALC1:PAR:SEL CH1_{test}_1")
    VNA.write('CALC1:MARK1:FUNC:EXEC MAX')
    time.sleep(1)
    S = VNA.query_ascii_values('CALC1:MARK1:Y?')[0]
    logger.info(F"{test}: {S}")
  
    VNA_Screenshot(name, test, folder_path)
    VNA_Save_Data(name, test, folder_path)
    
    VNA.write("OUTP 0")

    return S

def get_V_Switch(settings):
 
    Vsw = {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None} 

    Path.IPLS()

    carrier = settings["nf_gain_interested_start_Frequency"]
    threshold = -40
    
    #Check if Voltage switching enabled
    if(bool(settings["Vsw_EN"]) == False):
        logger.info('Voltage switching test not enabled')
        return Vsw
    else:
        Vsw["Enable"] = True
        logger.info('Voltage switching enabled. Begin test.')
    
    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'"  + str(settings["image_rejection_state_file"])+"'")
    logger.info(F"Load state: {settings['image_rejection_state_file']}")
    time.sleep(1)
    Wait(SA,logger)
    
    #Setup SG and SA with frequency and power level
    SA.write('SENSe:FREQuency:CENTer ' + str(carrier))
    SA.write('SENSe:BANDwidth:RESolution 1000 ') 
    SA.write('SENSe:SWEep:TIME 500 ms') 
    SG.SetFreq(float(settings["LO"]) + carrier*float(settings['USB_LSB']))
    powerIn = (float(settings["Gain"]) * -1) + float(settings["image_rejection_cable_loss"])
    if not SG_power_check(powerIn, settings):
        return None, None
    SG.Output(True)
    
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?'))
    time.sleep(sweepTime)

    logger.info("Beginning voltage up test:" + str(settings["V_Switch_Up"])) 
    #Voltage switch Up Test
    #sweep +/-1V around switch up point
    if(bool(settings["V_Switch_Up"]) == True):       
        for voltage in np.arange(float(settings["V_Switch_Up"])-1,float(settings["V_Switch_Up"])+1,0.1):
            PS.write(F':SOURce:VOLTage:LEVel:IMMediate:AMPLitude {str(voltage)}')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw["V_Switch_Up"] = round(voltage, 3)
                prilogger.info('Voltage switch up: ' + str(Vsw["V_Switch_Up"]))
                break
    else:
        prilogger.info('Voltage up test not enabled')
    
    prilogger.info("Beginning voltage down test:" + str(settings["V_Switch_Down"]))
    #Voltage switch Down Test
    #sweep +/-1V around switch down point
    if(bool(settings["V_Switch_Down"]) == True):       
        for voltage in np.arange(float(settings["V_Switch_Down"])+1,float(settings["V_Switch_Down"])-1,-0.1): 
            PS.write(F':SOURce:VOLTage:LEVel:IMMediate:AMPLitude {str(voltage)}')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw["V_Switch_Down"] = round(voltage, 3)
                logger.info('Voltage switch down: ' + str(Vsw["V_Switch_Down"]))
                break
    else:
        logger.info('Voltage down test not enabled')

    SG.Output(False)
    Pwr_on(settings['Voltage'])
    logger.info(Vsw)    
    return Vsw

def SG_power_check(power, settings):
    max_power = -20
    if settings['Band_Switching'].upper().count('BDC'):
        max_power = -10
    
    if(power > max_power):
        logger.warning("***SIGNAL GENERATOR MAXIMUM OUTPUT POWER REACHED***")
        return False
    
    SG.SetPower(power)

    return True

class SigGen:
    def __init__(self, root_manager, address):
        self.VISA = address
        self.INSTR = root_manager.open_resource(self.VISA)
        self.IDN = self.GetIDN()
        self.INSTR.timeout = 20000

        if(self.IDN.count("Agilent")):
            self.name = 'KS'
        elif(self.IDN.count("ANRITSU")):
            self.name = 'AN'
        elif(self.IDN.count("Holzworth")):
            self.name = 'HW'
            self.INSTR.write_termination = '\n'
            self.INSTR.read_termination  = '\n'
            self.INSTR.timeout = 10000
            self.INSTR.write(':REF:EXT:10MHz')
            self.INSTR.write(":CH2:PWR:20dBm")

    def GetIDN(self):
        return self.INSTR.query('*IDN?').split('\n')[0]

    def SetFreq(self, freq):
        if(self.name == 'KS'):
            self.INSTR.write(F':SOURce:FREQuency:FIXed {freq}')
        elif(self.name == 'AN'):
            self.INSTR.write(F'F1 {freq} HZ')
        elif(self.name == 'HW'):
            self.INSTR.write(F":CH1:FREQ:{freq}Hz")
            self.INSTR.write(F":CH2:FREQ:{freq}Hz")

    def SetPower(self, power):
        if(self.name == 'KS'):
            self.INSTR.write(F":SOURce:POWer:LEVel:IMMediate:AMPLitude {power}")
        elif(self.name == 'AN'):
            self.INSTR.write(F'L1 {power} DM') 
        elif(self.name == 'HW'):
            self.INSTR.write(F":CH1:PWR:{power}dBm")

    def Output(self, state):
        if(self.name == 'KS'):
            self.INSTR.write(F':OUTPut:STATe {int(state)}')
        elif(self.name == 'AN'):
            self.INSTR.write(F'RF{int(state)}')
        elif(self.name == 'HW'):
            if(state):
                self.INSTR.write(":CH1:PWR:RF:ON")
                self.INSTR.write(":CH2:PWR:RF:ON")
            else:
                self.INSTR.write(":CH1:PWR:RF:OFF")
                self.INSTR.write(":CH2:PWR:RF:OFF")
    
def Equipment_Init(): 
    global rm, SA, SG, PS, PM, DMM, AMM, VNA, TChamber, OSC, Resources
    Resources = {}
    error_str = ' Communication Error. ATE functionality may be limited. Press Enter to acknowledge and continue without resource.'
    print("Connecting to ATE1 equipment...\n")
    rm = visa.ResourceManager()
    try:
        #raise Exception
        SA = rm.open_resource(SA_VISA)
        SA.timeout = 20000
        SA.write(F":SYSTem:ERRor:VERBose ON")
        Resources["SA"] = SA.query('*IDN?').split('\n')[0]
    except:
        input('Spectrum Analyzer' + error_str)
        Resources["SA"] = None
    try:
        SG = SigGen(rm, SG_VISA)
        print(True)
        Resources["SG"] = SG.GetIDN()
    except Exception as e:
        print(e)
        input('Signal Generator' + error_str)
        Resources["SG"] = None
    try:
        PS = rm.open_resource(PS_VISA)
        PS.timeout = 10000
        Resources["PS"] = PS.query('*IDN?').split('\n')[0]
    except:
        input('Power Supply' + error_str)
        Resources["PS"] = None
    try:
        PM = rm.open_resource(PM_VISA)
        PM.timeout = 10000
        Resources["PM"] = PM.query('*IDN?').split('\n')[0]
    except:
        input('Power Meter' + error_str)
        Resources["PM"] = None
    try:
        VNA = rm.open_resource(VNA_VISA)
        VNA.timeout = 10000
        Resources["VNA"] = VNA.query('*IDN?').split('\n')[0]
    except:
        input('VNA' + error_str)
        Resources["VNA"] = None
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(TEMPERATRUE_ADDRESS)
        Resources["Temperature_Sensor"] = TEMPERATRUE_ADDRESS
        sock.close()
    except:
        input('Temperature Sensor' + error_str)
        Resources["Temperature_Sensor"] = None

    Resources["OSC"] = None

    print("Equipment List:")
    print("----------------------")
    for keys,values in Resources.items():
        print(str(keys)+' : '+ str(values))
    print("----------------------")
    print('\n' + 'Launching ATE1 GUI')

def GUI(settings_master_list):
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title(os.path.basename(__file__))
    master.resizable(width=0, height=0) 

    #Tkinter Icon
    enocdedImage = 'iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAADs4SURBVHhe7X0HgFvFtba3uGLihjHGBowNhGZaQjGEUAKhGBywQw31kVCMIZQXAo/QbCA/LaHbuJf1uuLeO+69997W3pW0klZdW3z+883ea2Qxu95y79WVNN97HwZnV3M1d853zsycOVOHFBQU0hZKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABTSBseYZfwPnaUaS2JZZgBjPk9vA+3x/wvaCUoAFNICUbbAwuJjdDBURrsCpbS5qJTWeEpoaWEJzXeW0Kz8Ypp1hHk4SrMOxvGAjJGf/z32Zw8x8RlHi2muo5iWuEpEOzv8pZQXLiMvP0PZMfvIgBIAhaQHzCneq8ezsLiM1ntLaOrRKOUcDNM3u8PUc1uQXt0QpKdWB6nrUj91XVBEXecwp3tO5NSTMPZnZzHxGQuL6JGVfnppfZA+2BqkgfvDNLsgStt9JRQuLfvF8+kRgtVQAqCQ1Aix9TgiZbQ3UEZbfKW0mr3tSva6K9n7rmQvvJK9Ov6ceShCfXaG6P/WB+jZVT7qtthHN833UseZHmo92UMNfyykhiNc1HCokxoOdFDDATHsJ2PBz/+u/xx+bzBzGH/GKBc1m+imC1kUbprnpSeX++j9jQEasjtEi49Ejj/XSkeJeOY9HJUURo9ZLgJKABRsDxgFqHv5WB7msHqhq5iGcUj+6Y4wvcYevTt73u6LfNR9dhF1n8WcW0RPz/FSl5le+v1UN/1mspsuYuM8e7ybThvLhj+mkOqMdFGdXGaOk+oMc1CdoTEcIiP/nP7v+s/h90B8Bn9W3dEsAvz5aOdybvPWaW7qNstLz831lj8bRwrdOfLAMw/az6LgLqEoTw+Ofz/te5sJJQAKtkaUrQDz5qNs6PDyO3juvsPLxLyaDWZmXpQ+3x6iv64M0B9+KqIO7HFbjyuk1sOd1Jq9c+u+TPbOrdgzN2ejbcKG2pg9dKMcF9UfzkbKhprJnr8OCBEA2XBPThYN2d+z5xefwZ+XwczmNtDOKdwe2j6Nn+GMQfxMeLb+TP5ZPPMTK/zUe1eYNruLxffC9zwQKiMfVhJNhBIABdsAQz2eCO8RIo/PK6avd0eo58Yg9VzDZIPpucxPPTiU78Le/Tc8Fz9rgpsasDeH4WWwsWWwgWX0d1LGQOZgJof3GWz8GWyMwtuP0Dw/OAoGzUQ0wF67xsTvg/g8TQjQTgaLgGibBUA8ywCQn4//Ds98BT//A/OK6F2eKvRczt9tfZD674uIxcrY/jAaSgAUEg4M7AjHvEXs7ZyRY1TAnq8gWM4VPJ/vtzdMf18XpFt/8tFFkzh8H8PkMPsiNuj2bMxnsLdvygbdUPfmulHrhq0bt07dSMe65fyxlpR9JtqLfYYYgcAzN8ktpDb8PS5kQbgIZDG7f5mPRh6IlvcF94mHIyGjAwIlAAoJRynPe3fzIF/gKKahPBfuuy1MfdkD9t0UpA/WBOjxRUV0Pc/fT+e5dCaMnL1oJnv3TA6jMwdpXlX36jAq3ch/ZI5j49MpM9ZE8Pjz8PPhWfmZ8fz4HuJ7gSxw7Se76dklPurLUU/fzSGxg+FmETASSgAULAOGLra84MUwtxfkvwiUlNH0gmJ6f0uQ7lzko04TPdSJDaITz6cvZ0/ZfqSTWrKR1BfhtGbo+p8j2YB0Dx/v2WXGZzfGRgd6VIDvxWzM3/F8/u9O/Gcn/pke6wIinwB9Vsx9Z4QUKAFQsAwI8/cGSmlZYQlN4jn9pP1RmrQnQj/uDtEb7Olv4zlws/GFlMVz5KzvCyjrBybPkzMHl8+VhWHASHTvHm9MdvLyNaUeGfD3xLpB5kAHZfXhfuhXQFfN8NB3W0Oiz35yFov+rC2UACiYAgxNjE94fJ1Odvnj8yL05mb29Dyfv3Oal+7kAf9HNvoreNC3YS9YDx6Q58J1YPDadtrxubzw7hplxpMK1EUM3xFRASId9AP3SQv+79/x/34nT4UQDbg4DEC/8v/XGEoAFEyBn+N8ePsV7O0XHSmmRYejNPFAhF5d46dr53gpmw06m+fv2ezps3VPP5QHOwwe22nCw8cZeip4+OowVgzGlq8TZPV1UDZHBBdP99CkA2FadLSY8iPIGKgZlAAo1BrwQCJxJYYw/hEHI/TK+gA9xaH9UzO99BCHsNdOclNrGLbu2YZo3p7DXTGfh9dLZQ9fY3KfIArCYudQjga4jx6a7qWnFvjEdAB9zv9fbSgBUKg1kKyCQzabikppk6uENjlLaNz+MHVf7acL2FM1H+qg5v0c1JS9fCM2+LowdggAkmbiF+2SZfEukeQ+Q4JRs/7crywIn24Jin7HNmF1oQRAodrAMBOE12Fu9ZVS7sEovbkpRG8u9dObPL9/er6Xrp3moVMRug5xUMYg5mD29PBg+sq97unTLbSvDfW+4j7MwBYoRwOdZ3pEv6/1lIr3UR0oAVCoFrCFB09ziD3+QX8pHWTjH82h/t9WBaj9NC+1Z8/UfqCT2nBY34T/PQueXizigWzw8R5fsWZE/6FPuX9bcl+j3wfuidDBQPXWA5QAKFQLMP4FPOf8eneYvlwfpC/XBunZxT76DRt/Js/fM9nTI0FHpLsKb8+saNtOsZYsF1SRQDTQQY9z5PXllpD2pqoGJQAKlQIJJ8HSY+Rjw/dFj9E2nud/vC1E18zz0lVs8FfluqgDs1kuD0Z4+xEc5scn5yhvbx5FJMB9nOukdhwFXDXJI94TkqyqAiUACpViX6CUZhcU08i9ERrJhv/VpiB1nl9E9Xn+jq27rL4FwvtkDNO8Peb1ak5vPVloRdIQvwe8J6zLVAVKABROALaTMM/X69tNPxql7jiIM81Dt7KBX8cGfhZCfXh79joiaQeGr3t7hKVKAKwnhBfvhKOAWzkiyDkYEUlCJ4MSAIXjQGopcs3nsMefcyhKcw5E6c21frp0hpeyhzop+9t8ysIBHOzd4yjtWB5wyT63h1idIFiaiMUSxlUZf/E72mclQgh5+pX9XT79bblf1CI8GZQApDng8WNTdQfsj9ADy/z0ABv9A1M8dM2EQmoBzw5PzyJwQsJO7GBPBsoM8gQB4O8jM3B8f5343jr1v4v/eV0IEiEAeB5+Tx35Od7nqQDeb2VQApDGgMc/ECyj1YUltDq/mGYciojadY154Dbm+WTjHwqo/mCeV2I1H1tOybiYh2cGZQaMxTOdiGj0RczKiFRlMf2p7Ge1n9E/+xeCEfN8ZpCNv94AB904zysiusqgBCDNAIcArwA62OOP4lD/pdUBemluEf0PE7XrxJweNe6QuCPm+NrAxeBKhFerDnWPLp5Te24YH74TG604d88eUpC/H+oJYPFMnMHHeQQQ/43tTJAF8DiHgPzzw5j4M/Z/w8/i9/TPwOfxf59QiQjRE4QBz4NI4fhzmkBury23ge3ayoIAJQBpBD3M381eYbenlBbyXP/FNQFqw2F+Gx6wZ/BgbYz8fDFIYwaqnT1/vIfXDR5E1ALDZ2LRsgELwK/YCFvwd2zFwtaG/zwbSTTMDvzdO7Chdhjmog4sEh345wT5dzrw7wvy53Xgz+3AfSL+1P8eP4Ofxe/h9/E5/Hnn8p9t+bNP57aacVuN+Wey0bf8O4J4Rv2Z9e8h+441IX92fRapexb5xMGsiqAEIA2A1w8GWAFmstF/vDVIH68I0P8u9dN10z3lNfRYAES6Ljw+wlYYvmxgJZqxHhPPCCNChALvDi+reXZRBxDfB7UB+fvVZyNrO95NV0310C0zPNRltpcem1dEz//ko38u8dFbbChv4U/uE8FlGpczV2hcCQboLY6YxJ/63+Nn8LP672qf99piHz05v4ju5bZumOahiyZ5qAk/s6gPyFGDeEY20gwhuvwdIASx37VW5M+CsHF/bfZVvBioBCDFgSQeV5QZKqNdvlJ6Z0uQLpnupkvYU53PHqs5G87xOSu8ku6NpIMqQdSfR/eSutdElMLPjYMxDXiwn8pG1ZSNqgWLWYsfmPgzh8lG15YN//dsjP8DI94QoP9uD1PO3jDNPBylda4obXaV/MzCCuiOo+xnYj5nhSNKo/eH6Qtu6+8caXVb4qeLZnjFuf4WPD3AMzbnP0/hqQW+w/H+HxPznWtDbucUFsORh8LaaPgllACkONZ6S2ngvggN3ByiLzcE6ba5XsrkgSbq6fFcVSTwwOPrK9eygZQI6p4eBoHn07y8fhxWEDX4WQCa889cPMFNt0zxUFc2sKdnMqdrfy4ooqfZO7+wLkAfbgvRkAMRmnI0Kq7sQrIMyo2fUIvfQPpZfHEN2SJuaywLzbd7IvT6hhA9zVHC07P5ufgZH5npoU6T3XQ6ohmIALZYURcBgozvHt8v1SJHGywAzy31aaPhl1ACkGLgcSfm+noiT182/hs41L2BjegaZmvMi2FIoL6dB48jHUAWMza8F/+O5yt/VhQLyWJvma0VxBDsx+TvhNt3HuKw+6NNQRrBXn1xXpQWs8GJP48W02JHsShDtoHFcDcbJA4yOSLHxH0DYe4s9JkZwHvAUekCbmt/sIy2seCscpfSYp6GLT7C5GeccjBC/7s2QJezEGSzGGf3RoEUFmcIgdh5qaUI8GdcwIKPZ5FBCUCKARdgLuHBPpU93dS9EfoLq38WG7moK8fGgzmymG/CuGINLtHU5/Pw9hAneEPMjXOd1IAHMMqFdfzRRTfxPP5u9vaCkz10N8/hX2Tv/j0b/k/OEtrPxo3BXsriJ/6Mo/DO3E/8h2mGHwu9HT0qOOF5+EFwZ+GkI1F6lacld88porv5+93CPAveH1EAT22EUNdYCArFjsQaj3w7UAlACiB2YG30logLLztP81BnNvDzMXAwkGBMMCoYmB5a2kIA8CxMfi5RBBPbazw1EYKFUmFYRZ/oods5ivn7Kj/14zB+CgubIEc3U/KKaSEbPi7QOMLhPBY6kwmYfiA6wJRkyiGOCPh7DdoRort56pLFQpjFEQG2Ko9P02ryzoY66KPt8lOCSgBSAGIAsREsYWPozS/6Mp771h1QQHW/x+ApnycLD1LbcNII6ot48PaIRHRvz8/YiNmWvX1HDluv5//teh7010/1UJclfvpga4jGs6fcFywVJxRjiV0u2D3/qyVe3WhAwPEd9O9zOFxK/+b3eP0Mr+iHc7E4iHeIvqrJusBIJ/2BBUUGJQBJCAxyPaTEXfOjD0XomRUBemaWl+5hz98Sho4FMswjMWhgbLKBYSWPz+mZPJix+CiSZ3hakok5L4vAWRPddA97+rfY0/ffFCrnzrCoNoS6d9v9peL2oFQHFg+xcNh/V5j6bwzRowuLyi9EwRQO71NETXH9WxlHu6gd97+s55QAJCECbAS7cR02z/W3uIrppTV+askvuiXP9ZqyUYktJX0BSd86ix8UVlBvG0YPap4MyTAteCC3H+6ki4cyWagunuSmzot89DF7+vmOKDmwUAdyWI9rs7GYhgW7JIvwawQIO5J3cC8i+qDPnhBdPNkt+qoRBAD9WJ2FWx4HSArCVerxUAKQJMCrE+R/7GRPiBTPt5f66e0lPurEXj8jl+fQSD3VF/kQKiZqjv8Lb4/kHCS+lD9fE/7fbpzpoR6Li+jD5X76cBlzY5B+4PnvPEeJWKU//n1jmE6I/d64NvzDTUHRV+04SkI/isiuqpEARHiwU5z7iIcSgCQBEnry2Bvm+cto4uEo/ZG95XlsWOexQTXDYMAcEV4fRpcojw+KqEMzfvZUuH77V/ycZ7AAnMkD90z+78tmeei19QGakRehXZ5S2uVm+krFAEXSUhguUOE4UIYN/YO+umOhj87kKK8eojz0sewdyDjURct5WhEPJQBJgi08AL7bE6Zv1gWpx1IftWVPoB88EbX39FVi2cu3gjB6PANSiRGFYOWaB+lp/Ew3TPXQ03O91IPn9z2W+On/Ngdp3JEoe/rSmLWMcirT/yXQJ3r//GdnWPTjmehz9upVXhTkMYJkpHgoAbApYl96kD0AFvqu5Rf/W/agF7C3bwiPD+rJPIn2+qM51GeDr8eevlG/Amr0HZP/GzsSSHSZwYNvVX4xreIQf723PBlHNidVqBwimYj78bo5XmrUp6A86pO9k3jyuPmWHUg8lADYFLELfWN3hKn7Ch/VZUPD3rg4ZoqwH4Zn9Txfb0+E+EyEokhWyXHSKRwBXD6+kO6f4qZuEzzUbb6PXue5/YS86PF77HQm65ZdogGHgP57e3OIuk3ylEdciLxONg7YWWBrMR5KAGwGGEX8Qt9tHOZdjAs2sPqLFw7Pr4d+VgmA3g7+lIT42YMK6KwpHnpxlZ+m7Q/T7H1Rmn2kPAUXeQpG3GSr8DNWuUto9v4oZWOrFwJcBQHotS2o/fbPUAJgM8gW+nCB5s8v02LDF+Q2ITjaNp4sxMfhIvzsaH7uUGlZ+VkEzVsp2zce6Ff08c0svqhzcNIFQX5v721RAmBLwD70+b5soU/6Qq2ixNvLQnyFxCB3d4TOmOAu3/6VvT+dLABYfI2HEgAbIHa+L13ok71Qiyjz9rIQXyExKCo+Rnct9tMpEIDKogAeR+9vVWsAtgG8viD/I3a+L13ok71QC4iwXubtaxPiu91uWrp0KeXm5tI333xD7733Hr344ov00EMP0R/+8Ae64oor6KKLLqILLriAOnToIIh/v/jii6ljx4504403Urdu3ah79+70/vvv0/fff08//vgjLVq0iPLz87VW0gfo+v77ItSOowCxIFvRbhCPpe94jMVDCUCCUFFij3ShzypKwv2aevtAIEBz586lL7/8kp5//nm66aabqFWrVlSnTh1TiTbuuOMO+uc//0kjRoygrVu3Umlp1W7JSVZgHHVe7Ct/fxUJAL/T8XkR7Td+hhKABKGixJ7yF6YZvqULffJwv6re3uv10tSpU+mNN96g6667jrKzs6UGmgiecsopdN9999GAAQNSMkrAK8EefyZuaqrojAA7l9Xu4vJfiIESAAuBo55BtqRKE3tkL88MGrC4t23bNurZsyddddVVlJGRITU+O/Kaa66hXr160YYNG7RvkvxA6bHmmAYgN0P2voc4KS/0y0hICYCFQA26sQcjlSf2yF6eCazp4p5u9JiPy4wr2QgxyMnJoWg0uXcyEJk9uSrAnh5OJG4cYVrAEQCcTzyUAJgMdLm+xTf0QEQU5aw0scciVmdxz+fziQW7VDF6Gc844wz64IMPknqKgMpIcCbldzbGvG8eW7gXQWL/SgDMRuwW31+X+8VZ+IQk9khC/pN5+507d9LLL79Mp556qtRoUpH16tWjZ555ho4cOaL1QvIAot0GTmVMnACMdlHbyR7tp06EEgATAKEV5H/EbvGJUtxaxpzVlIX8Mm9/jB96xowZdNddd0kNJF0I0fv000+Tbmpw+zyvdjI05v2PctGdC+WlwZUAmICKtvhOeCkWsyoLfPPnz6dOnTpJDSJdiRyEadOmaT1kf7y/LchTyjgB4Gkm7kSQQQmACah8i88iViPkX7Nmjdg7lxmAYjnvvffepFgfmO8sLr88Rc8HwMLyEIeoqSiDEgCDYKstPmZVQv4dO3bQAw88IB3wir/kmWeeKTIO7YwifsnidKCeDzCGxx//dxEGqARKAAyEXbb4wMpC/kgkIlJw69atKx3oihUzKyuLPv/8c7FWYkfgniNRL1Afb6NddPY4VASWP68SgFoAXhQeFZ4VSNgWXzVSeOHBLrzwQungVqw6u3btKrIf7YgsXKembwXy+Lttgbob0FTMPxhN6BZfVVN4n3vuOelgVqwZcWjJjtuFjcfzuNO3Agc76VOOSiuCEoBaAJdLHvKX0ktscB2GOOkUNsDyevy/NFIzebIVfuTot27dWjqIFWtH7BLk5eVpPW0PtJrM40LUiSykuv0ctKWo4sNQSgBqAdzn9tW6IHWa4RF3uKEo5i/SMM1gFVf4Mdd/9dVXpQNX0Tief/75dPjwYdHndkDbaZ5yAeBp6JU8PvQcDxmUANQACKv97P3/uzMsDB+0Mp23Kiv8e/bsEYd0ZANW0Xied955dOjQIW2EJBZtIAAYixyVvrbar/2tHEoAaoDDoTIaxcbfdYGv3PPDI1vh+TXGh/zxmDJlCjVt2lQ6UBXNI4qXHD16VHsLicNpU7QIgOf/k45WnsmoBKCagJdFUgUMv52+0h+femkGZWE/h/yxQOGLd955Rzo4Fa0hMikx9UokGmnHgnEACMeEK4MSgGpgxL4wNR3vpqZ9Y1f6raEs7I9FcXExPfLII9JBqWgtn3zyyYTmCWTC++fwOJnlpWhlCwAMJQCVILbr/rHcT4/N9VIGK2sG7tyXGKmZrCzsD4VC1LlzZ+lgtDNRNahNmzb029/+lm6//Xa6++676Z577hEHkfT6gKeffrr0d+1O1CpMBDBmEZUiAa2y7T8dSgCqgHc2hzjcd1JrhOFY7Kus+qqRrELYD6DenmwQ2oX169cXhTdeeOEF6tevH61evZocDgeVlWkZVCcBTuQdPHiQZs+eTR9++KHIy2/ZsqW0LbsQx4pxxsJq4Br1Ouygzs5x0j7JEe94KAGQINbzf7HST7fP9lLmEAdl4DJGK+b7Gk8W9judTuE9ZQMw0Tz33HPplVdeoXnz5onpidFAiI2SXm+99ZZoS/YMiWb79u0pGPzlIq2ZWOstoTrf59NrS3yVbv/pUAIQByzy6XiQw/4r2PBbIbd6BBs/PH9FRRcNpqwkdyyw74xS2bKBlyjCK8MgN2/ebOkcGG0tW7ZMrIHYrTbhv/71L+0prcHAfWFq9G0+ravg9F88lABUgOHbwtSeDTCrd375Nh/2VRNUtSce2G+2k9dDFeBhw4YlfPUbQBnwRx99lDIzM6XPajUxFdi1a5f2dObjqYVFdNPYQpEPUhUoAdAQO1u6eZ6XbmaDPzV2mw+nqxKQ0x8LhHSoz4fFMdlgs5q/+93vaPHixdrT2QuYHlx55ZXS57aaWKC1AhgfF/Dc//9tPHHcVAYlAHH4dFOQske5KLuPRam93EbGAMfxjMLKEnxKSkpssdp/ySWX0OTJk217JFYH1h5Q6NMOdxRMmjRJeyrzsDdQRu04ilwqqf9fEZQAxKDbTA/diCQKrPSjqILZq/0cZdTnF9bih3IB0I/wVoSXXnpJOrisYuPGjal3795Jd9MOVuOx3Sj7TlYRUzZs15qJsXlR6jLHS0cjVdtdAZQAaHhvQ4AaDXJQ/f6O8muwzS7ggQsc4ub8eyrJ2vrqq6+kA8sq3nbbbbR//37taZIPWDe57LLLpN/NKvbt21d7GnPw0fYQfcqs6vwfSHsBmHi0mF5c4KNbprJRDmXjN9vzYx2BxSVj6In37MP4KxKAiRMnJmx1G16/T58+tg/3q4KioiKRcCT7nlYQUyez+hGf+9amAC1yVW/LNe0F4I0NQTpziJOawvhxlh/Gb+Z5fiwossicye3o23yVYd26ddSoUSPpgDKbOOuOVfVUQjgcFjcMy76vFZwzZ472JMaitOwYfbQtKK4Lrw7SWgB6LffTzTO9IrU3QxRShOc3yfuLHQT2/MNcYr5/y7wiMeevDJgzJqp8FxYb7VryqrbAFeWJyqFAFqMZCJeW0dQj0Qoq/1WMtBQAR7CMXlkfoIvY6E/DSj/m/BYs+KFYI4y/U24hvbP15AtCf//736WDyGwieaWqabrJCqQWJ2JhEFM5M/ICAsVldDRc/XeWdgIwhef8/TaE6Hr2/Mi2E0k+Zi74xW3z4e8qW+zTgVBRNoDMJCreDh48WHuC1Mfy5csTskUIYTcaoZKyant/IG0EACm+xTxP6r4uQJ2GF1JLrMIjxdfM3P6Ybb7nVvlpk6ukSsbv8Xiobdu20sFjFpGxNmHCBO0J0geffPKJtD/MJO4XMHoxsLSGn5c2ArCYjW/ynghdgfp93xeIubipST4QmCFOOn9cIT013Us5B6ueJvv4449LB45ZbNCggbgPMB2BnIYbbrhB2i9mcu3atdoTJBYpLwBIjyzlqdGrG4N01zg3tYDHz+Gw3yzPH7PNl80hf5dFPlp0OFolzw+MHTtWOmDMIkLgmTNnaq2nJ3BDktVTgV69emmtJxYpLwBbfaXCAC/nOT9q9sMwTfX8Mdt8d4130392hYUAQYhOBpyRb9GihXTAmMVBgwZprac3rL4zAQeo7ICUFQDYG4zuSzbAp6Z76HQs9HFILkJzmeHWlpJtPkw51nur5vmBHj16SAeLWbT6qKqdgV0BrIPI+skMYjcAgp9opKwAoHIvFt26LPJTix/yKRs3pop9/jjDNYraNl+zEa7j23xYdKxqWqbVYSjOzqdCdp+RePnll6V9ZRaHDh2qtZw4pJwAYEhjXI/lsP+txT66YKKHMgagbr+Jxs/EliKIf68J7r//fukgMYO4LwAZcQonAtd8YStU1mdm8Omnn9ZaThxSTgAsv65L2+e/eopbCE5NgAs7ZQPEDDZs2JC2bdumtawQDxQmlfWbGUQ5t0Qj5QQg9rquzP4Oc8/0S/b5qwuE4VgQkg0QM5ioarXJgtzcXGm/mUGIcaIzLlNGAJDoo1/XdRXPw1vkOs29rkuyz1+Vlf54jB49Wjo4zCC8m5r3Vw4U8cQJSFn/mUEry4XJkDICcMJ1XX1NvK6rlvv8sUC5a1wnJRsYRhPbi3a4tioZYGUi1rhx47RWE4OUEAD9uq5bx7vNv66rFvv88UCNfNmgMIOo5KNQNQwZMkTah2awZ8+eWquJQUoIwNyjxdRzfZCy+xVQpri1xyTPX8t9/lggFL/00kulg8JoduzYUdQTVKgadu/eLe1HM/jAAw9orSYGSS0A8Lo4BPHgQh/dgFp+ONOPeb/MgGvLWu7zx8PK035z587VWlWoCiDOrVq1kval0cSNSYlEUgvApqJSWpMfpcZslA0GsOcfbYLxo04Af36zEYV0VY6LHlzhF4uNtQUKQ8gGhNG87777tBYVqoOuXbtK+9NonnfeeVqLiUFSCgDMD97/A/bCL8/3ir14UWDT8HP9CPmdlMnicuU0L321JkjDD1Z+33pVgJVfK2r8Iakl0avMyQqrjgljcTaRSEoBKIweo9089756ThG1HcieH2G/0RV9EPKPdFGTHKdIKPqf1QE65Cul/GqUXK4IVqWcPvbYY1qLCtWFVacycYNRInMBkk4A4P0XOIrp45UBajHOLbLwyo3fYAGIy/DLYc+PLfTaBv+oTHvqqadKB4PR3Lhxo9aqQnWxatUqaZ+aQRSASRSSSgBgfC72wJ9sC9KlPB+vh4o+SMgxOs1XkuF3MGSMSn/55ZfSQWA0cc++Qs2Bm5dl/WoG9+zZo7VqPZJKALDhNmhHmLrMQz0/HPAxYdGvggy/muzzxwOryyi1LRsERtOud/YlC/CurCrHvnr1aq1V65E0AoDttgD/43eTPHQWDB/e38g0XwMz/CoCavzLBoDRvP7667UWFWoDq0qyz58/X2vReiSNACwtLKEJe8OUNdhJmbhOC3N+kZxjEA3M8KsIuDtfNgCMJq7qVqg9rr76amn/Gs1ERmtJIQDw/qKm33g2UqT5Gnm23+AMv4qAkNKKvH8sMOJAi0LtYdUNQitXrtRatB62F4Awu+ClR4vFffl1e+cbn+xjcIZfRcANtbKXbzT/+te/ai0q1BZW3SOIqWGiYGsBQPiNq47/yl65pajp5zB23o9pBBt+Zl8HXTHNIy4Mmees3uWKVcWbb74pfflGc8mSJVqLCrWFVdmaW7Zs0Vq0HrYWgL2BMpqVF6WWPO+va3RNP2wdjnJRNnv/i4Y46ZlVfnFlWBGOFhoMhP/t27eXvnwj+etf/1q0pWAMHnzwQWk/G81EZmvaVgAwjr/nuXj3n4ooYzCy/WD8RgkAfw6LScZAJ53KkUWvZX4aeShSXk+wvHlDgW0e2Ys3mh9++KHWooIReOihh6T9bDT379+vtWg9bCsAR9gbd17ko3YI+3HG38hkH0QS7P3PYAG4dKaHdrpLKa8GFytWFW+88Yb0xRvNzZs3ay0qGAEkU8n62WiiGGmiYEsBgCl+uylI50xwi4M4hh7ywWfh2PBwJ/1tThG9ye1grcHMwBmhuezFG8lzzz1Xhf8G4+abb5b2tdEMBAJai9bDdgIQZev3l5TR1RymN0JdP3h/mSHXlGNcVK+/gxqxACzIi9I6j7mFMqDuspduNHHASMFY/OY3v5H2tZFs0qSJ1lpiYDsBWMsGOWZniLIGOigDd/jJjLimhJiw9792nJu6zisif2mZ4dt98RgxYoT0xRvN2bNnay0qGAUrIreLLrpIay0xsJUAYAH+v7tCdBvm6Ej4MfSIbyFlDHGIsmHvrg+K3YUSC0JmK+6c+9WvfiUKjCoYizZt2kj720jedtttWmuJga0EYH5BMd23sIiyv9cSfoxK9dUE5QyOAG7mz5zrKBZiYwWs8CJ//vOftdYUjALWU1C3X9bfRvKJJ57QWkwMbCMACMUfXean87FIZ3R1n1GFlNWngG6a46XhO8J0wKCjvSeDVfP/b7/9VmtRwSi4XOyAJH1tNHE+JJGwhQDA+NexV24y1k31cNDHYONHqu8VLCpvbg6SN3qMDCjpVyVYdcvM+vXrtRYVjMLatWulfW00Ey3eCRcA2GKw7Bi9ttBHGbkI1Y1c9ce830mZ/JlfrArQdJ5iWGT7As8++6z0pRtJzP9LS409uKRANGHCBGl/G820vxjEz5PxTe4SOpsNVZzxN8r7a6m+zfhz2/G8/0BRKbnY+1sJK4p/3HnnnVprCkbim2++kfa30UTpsUQioQIAc9xYVEI9V/rLE34Qrhu18MeflTHQQTdN9dA/1gRMT/aJh1Xzf5X+aw7+8Y9/SPvbaPr9fq3FxCChAgDvP/xAhC7Bfr/w/hJDrgmx6j/CRU1/cNCb6wK0gSMMqzF58mTpCzeaCxYs0FpUMBJWHARq166d1lrikFABmMVz8meX+Mrr+8FojfL+EBQWgMeneWnykajpyT4y9OrVS/rSjSRKSqviH+bgsssuk/a5kcRtzYlGwgQA+/Avrw/QBaK+HxuszJBrxELK7F9A2TwF+OlQ1LBqvtVFt27dpC/dSKJmnYLxwD2K9erVk/a5kXz99de1FhOHhAnAbPb+V83wsLHy3B/Hc6XGXE1iAZG9/6WjC+mOn3yG1/SrDqwo//Xwww9rrSkYie3bt0v722jaoXZjQgQAIfkDK/x0Gry/kdt+SPj5oYBeXhmg8YcTlxqLyz9kL9xo/vvf/9ZaVDASY8aMkfa30dy0aZPWYuKQEAFY7iqhX43nUB2FPoza9sO5gVwXXcuiMj6vmMKJmPhrWLZsmfSFG00sNCoYj7ffflva30ayfv36triy3XIBQEj+/Eo/ZYu5v1Hev1CcHMwc6qTeG4K03Z+Yeb+O/v37S1+60dy7d6/WooKRsKIQyG9/+1uttcTCcgHY7i2lM4TBsvEbte03ykWNWAB+zdHE0WAZhRLo/YHXXntN+tKNJG6tSeSlkqkKHAI688wzpX1uJO1SvdlSAYBZvr8uWG78htX2ZzEZ7KRzJxTSe2sCopZgonHHHXdIX7qRvPzyy7XWFIwE6vPJ+ttofvfdd1qLiYWlApAfKqMLx2Gl3kDvP6aQmg900J0Li2ibwZd51BRI8JC9dCPZpUsXrTUFIzF8+HBpfxvNpUuXai0mFpYKQJ/tYTFPN8z7YwFxsIO6TPfQFzvDCdvyiwXC8rp160pfupHs0aOH1qKCkXjhhRek/W0kMX2zSwEXywQgwvPya6d4jPP+yBoc7aL6vfOp784QbfXZw/vn5+dLX7rR/Oyzz7QWFYxEx44dpf1tJO10gMsyAZhwMEpZ8P64ftuIlF+kDuc46U8T3LSpqMQW3h+w6g6AUaNGaS0qGAWPx0MZGRnS/jaSdhJvSwQAi/J3zC4qz9E3Yt8fAsLTiKyBBTRjX4TcVlX4qAKsOkeOXAMFYzFt2jRpXxtN3BNpF1giAEtcJZSNWvyoymuE90fSD08lbmTvX1Jm7THfkwGru7KXbjQPHz6stahgFKw4AtysWTNbbd+aLgD4qs+uDZTX+UPYLjPo6nK4izKHuWjIjnB5IzYCarzJXrzRDIft992THSjRLetrI9m1a1etNXvAdAHAaby2k2G0LAAyY64u2ftn8VTiskkeckfslwjz+OOPS1+8kcQug7oFyFjs3r1b2tdG0y77/zpMF4DcQxGqj7Rfo274Ye9/ymgXfbI5ZKvQX8ctt9wiffFGsnnz5lprCkbhyy+/lPa10dy2bZvWoj1gqgAUs4P+22p/+d6/EYt/Y9xUd5iTLpnpob1+exbCtKKQxDnnnKO1pmAUcEGHrK+NJFKM7Ra5mSoAjugx+o3Y+0f4b4AAsPdvPqaQ/m9LyDbbfvGwohAo9qoVjAOOb1uRvPXYY49pLdoHpgoA7vlrBu+PW35kBl0dcgSRNRje30sbi+xbBvvss8+Wvnwjef3112utKRgB5FTI+tloDho0SGvRPjBVAD7ZEKQMXPRhhPcf6aKmI1z02sby67ztitNPP1368o3kjTfeqLWmYASsOP4LHjhwQGvRPjBNAJD8gyu+jVn848/hSOKCqR5anoAKv9UBLuqQvXwjec0112itKdQWR48epaysLGk/G8lLLrlEa9FeME0AfnIVU5ZR9f54ClEvxynyCRJ81P+ksKKY5BVXXKG1plBbfP7559I+NpqffPKJ1qK9YJoAPKMn/8gMujpE5mCui5qPKxSFRO0MZHjJXr7RTPSd8qkCrMhfeuml0j42kjhfYNfMTVMEANt/IvkHN/3IjLq6HOakPy4qSmidv6ogFApJB4DRRMVhhdoDOfmy/jWat99+u9ai/WCKAHiKj1Ej1PsbY8D8f3QhZQ530oD9Ee3T7Qtc8yQbAEazTZs2WosKtcHzzz8v7V+jaYfy3xXBFAFYXlhCmeKabwMEgIXkvEluOhq2X9pvPBBSWnGcFHvW6kbg2qGgoIAaNGgg7V8jecopp1AgENBatR9MEYB/bgiWz/9rm/3Hv496f2/z59l87e84mjRpIh0IRjMvL09rUaEmeOedd6T9ajSffPJJrUV7wnABwB79Bcj+Q+EPmVFXhxz+t+T5/xab1PqrCqxIBAKXL1+utahQXWCqhmO5sn41mnPnztVatScMFwDc+JuJsl9GHP1l439mgc/WiT/xsGJVGRw9erTWokJ1YdXBn7POOsv2pdsNF4C8YCnV4bC99nX/CsWtwUvy7b31F48bbrhBOhiMJvavFaoPFOO0KkpDbQi7w3ABmJgXpTpDOAKorQCMdtGZw522T/yJx1133SUdDEbzqaee0lpUqA6++OILaX+awa1bt2qt2heGC0B3JAAZceXXCBfdMserfWry4KGHHpIOBqOprgavPpxOp2WLtIgEkwGGC8AVs7zGJADx/P/drSHtU5MHVtSV01lYWKi1qlAVvPjii9J+NIOzZ8/WWrU3DBeAVhPYeGu7ADiW5/9DnbTAmVzzf2DAgAHSAWEGp0+frrWqcDJs2bLFkkM/IE5rJkvJNkMFoLjsWPmtv7Xd/x9dSG1ZRHwlSbYAwFiyZIl0UJhB7GUrnBwwxj/+8Y/SPjSD8+fP11q2PwwVAHe0rPzmn9qkAGPxkEXk4UW+pEn+iQXCctmgMIMoP6ZwcvTt21faf2YQNSGTCYYKAOr0lZf/roUAjGEOd9IPe+yf+18RrCgKonPz5s1aqwoyoNov0nFlfWcGFy5cqLWcHDBUAFa4isXiXa2mAPhd/ozVHnsX/qgMv//976WDwwy+/fbbWqsK8SgpKaFOnTpJ+80MorBossFQAZhyJCoKd0oNu6oc66K6OU4qtNF1X9XFs88+Kx0gZrB9+/ZJs+BkNT766CNpn5lFrP8kGwwVgMH7wrXPAeDpQ/sJ7qSc/+uwcicAnDVrltaygg5sw2VnZ0v7ywza6cbf6sBQAfh6FwsAdgFkhl1Vjiqkexb6tE9MThw6dEg6SMwiqgSrKOBnIAPPqoQfnStWrNBaTy4YKgD/3RmqvQDw7/9rc1D7xOTFxRdfLB0oZnHmzJlay+kNh8NB5557rrSPzGIyp2UbKgB99hgwBeDfzzmQvDsAOl555RXpYDGL1113XdpHAbgwFdGQrH/MIm77cbvd2hMkHwwVgJz9BgjAcCetKky+DMB4WHXXfCxzc3O11tMPkUjEsvr+sZwyZYr2BMkJQwVg/OFI7XcBhrkoL5T85a6CwaAlJcJj2aJFC8rPz9eeIH0Az9+5c2dpn5jJJ554QnuC5IWhArDAES3PBJQZdlU51EWRZKoAUgmsTD/ViePIdi9CYSR8Ph/deuut0r4wk6gpkMyhvw5DBWCnv6Q8Eaiml4EgCYgFhJJ6E/BnoGqPbPCYzc8++0x7gtQGbvW5+uqrpX1gJjMzM2nx4sXaUyQ3DBUAf0kZZeIm4JpWA0YKcS4EIDVQXFxMrVu3lg4iM4nKxD/++KP2FKmJVatWifLosu9vNnv27Kk9RfLDUAE4xv/XCDcB11gAOALIdWqflhp47733pIPIbNavX58WLVqkPUVqYejQoeL7yb632bz55ptTqiS7oQIAtJqkGbLMwE9G7SBQKgFXQll1Dj2eqHy7adMm7UmSH1jpf/XVV6Xf1Qqec845dOTIEe1pUgOGC8Cfl/u1rcAaiACOAvMUIlXWAHR069ZNOqCsIDLikul8ekXANV5WVVyWETss27dv154mdWC4AIw6FKE6Ax3lC3oyI6+MmgBgKpFKWLBggXRQWUXcJJSTk6M9TXIBVXzffffdhEVRYKNGjVL2HgbDBeBoqJTqD2YBqGlNgBwXOaOpd+3VvffeKx1cVvKNN94Qe+bJAhzoQdET2XexihCeqVOnak+UejBcAIIlx+h0hP81vRlouJOWu5M/EzAeu3btsjwxSEacUbD7wZV169YlJIdCxkGDBmlPlZowXACQw3PrT0XlCUE1uRtghJO+2ZM8Xqo6ePPNN6WDzGpiH/v1118XZbLtBBj+Y489ZskFq1Xhxx9/rD1Z6sJwAQC+3h2mDNwOXJPdgFEu6rrcr31SagFZa4nIC6iIKJWFMuaJTB9GrsTIkSMtu1GpquzRo0daHK4yRQD2BsuoNTICMQ2obhQwupBaT/FQcYpms2IPWzbgEsmGDRvS008/LY4Uo4yW2YDRz5kzh1566SVbCaJO9EW6XL9uigBgGtB9oa/8jsDqRgH88xkjXZQfTk0FQJ7+3XffLR14dmDLli3p+eefp7Fjx4rCJkYAorJx40YaOHAgPfroo9S0aVNp23Zgr1690upYtSkCAKwoKKaG/Qs4CqjmbgCyCIc6aU0KLgTqwNy7bdu20gFoN+K8+3333Scuuvz666+FMKD2HRY1Dxw4IBKdDh48SHv37qX169eLy0pQEu3DDz8UQoI6BYgwZJ9tJ2KBNlm3SmsD0wQAl3p2neYR+/qY10uNXUbtQNAnG5O/KlBlwGESK2vWKVZMZEwiVyMdYZoAALMORih7iJMyhjnYuKs6FeCfY8G4doKbkvBioGqhd+/e0gGpaB1RVTkVM/yqClMFoISn8TdN9VDjXI4CkB5c5QXBQsoa6KQFR1J3GqADYbJsYCqaT0xPUEMwnWGqAAA5uyN07iQ3ZfblKGA0e/dxMoOXcJiT/jLfJ6YSqQysiD/44IPSAapoHh955BEKhZLv9mmjYboAeKPH6M/L/HQJEoNQMRgiUJVIgKcBTYa5aL0reW8IqiqwSv7www9LB6qisTzttNNo1KhRWs8rmC4AcOBDD0TpgxV+ymSvnjFE3xo8yZoAtgP5Z19f6hPbiikeCAgRgFeSDVpFY4hTmQUFBVqPKwCmCwBwJFxG6wuLqR0b9em4PBRbg1WJBHJddE6uk/YVlVJRqq8IMiACf/nLX6SDV7HmVF6/YlgiADBdX0kZ/e/KAHWd5RW7AiISGHWSKIBFInOwgz5bFaBVnpKUjwIAZKC9/PLL0oGsWH0qr185LBEAoOTYMY4CSqj3jhA1HeqgU/uzCGBnoLJMQUQII5x0eY6LftgbJk/0WMovCuoYMWKEpddapxqV168aLBMAAMa7hj35Y/O8dM9UD2UjQQhTAggBpgTxAoAdAxaIrL4F9JdFRTRsd5i8qXpIQALccWf1FWPJzgYNGogDTi5X6hSXNROWCgDgYi++4EiUBu0KU+PxbsruXUCZgxzs6WMEIHarEFHAcBd1GOkSOQW7/CUiQShNAgHy+/0if1422BV/Jgp34Fp2pCYrVB2WCwAMFwa8K1hGXZb66Y9s5L9G/j+igBO2CjVBEGJQSJkDnZTN0ULfbUGaeaSYjqboYSEZcDhl3Lhxll96mQyE4eOGnh07dmi9pVAdWC4AOrCqP4kNeRxHAn9d5qOsURzqf5/Phu6gDFwvJqYETD0agDAMd9IfJrjp/gVF9JOzREwpUuQSoSoB5bxwyAY16mTGkE7E4Z3nnntOHEJSqDkSJgCw2zBbcJiFYNiBCF0zu4iuGeakCznU/xW2CRERwOixU6BHBKNcVL+fgxrw3/97c4hWHC2mTUWlFEqXlUENOKabrtOCVq1aiTm+CvWNQcIEIBYb2Ii/3x2h79cF6JXlfrqQ5/qZQ5yU2d9BmYOclIGpARYMQRQaYQG4fbqXXphbRB9tD1EeTwcQCaRDwlAs1q5dK+6mT9QlGVYR3w/p0ijOaUXBknSCLQQgwB4cc/qjgTKaeiRKD7EIXDCmkC5gY2/HbMbRQBaiAZ08FWg6yEFncDRw4zwvzTwcpR3uEtrpLyV38TFKn9WBcuBAC+rXJUuNgaqyU6dO1KdPn5S4hNOusIUAwGsL8j/2BEpp0P4Ivbs6QO8u89MLC4uo0zQPNURqcA5HA1gjGMCRQZ8Cyvwun87kqKDH/CL6YKmfPtwWpmUsBLhdGJ8lqH12OgDeccyYMeKe/GQowiFju3bt6O2331aLehbBFgIQiyBHAweCZbTNU0rbCktoWl6EeqzxU4fpHjqdjf10nhaAzVgIGjObMDsMKKALOSLoOMNL3+4Mi9ThAo4GCjiiKODIwstRAdYJ0mnBECfdEDK/8MILdNZZZ0mNzQ48//zz6ZlnnqEhQ4bQvn37tKdXsAq2EwDYqD6fBw+FS8VtQ69sCNKzS3z07KwienZ2EXWZ4aFrp7ip3UgXNerH0cD3+VRvqJMemualr5b46QeOHn5YE6QfdodpdkExbfeXpN1ioQ5sI6ImH64Nx1kDXLGVqGpEHTt2pBdffFFk6aXaPXvJCNsJQDxgtPs5IljFof0yRzEtO1LOvjtD9DJHBn+YWyQuIsFtRA1659MF3+bT774toJt4evD7IU66cY6X3tkcoklHIhwNlIpqw8hDgBZAYNJTEsov2sQiIi6+eOWVV6hz5850zTXXiBC8NinIzZs3pyuvvJL+9Kc/iTMNn3/+uZiW4DKSwsJCrXUFu8D2AgADhaHCYGO5qYjnu3lRMe9/bGWAusznqGCCm+7LcdH9g5yCfxpdSDfN9VL3tQH6YmeQph2N0gIWEaQj7/CV0uFQGRWxIpRhsUDhBAQCARGSw3BR6HPy5Mk0YcIEURQUnDhxIk2bNk1c34V6elu2bBFZiwrJBdsLQEXAav/eYCmt9ZTSXDbq6SwG0/dFaPrOME1nUZi+PUyTdoXoPzvC1GtrkN7YGKTXeRrx0roAfcZ/N+JglBY6S2gvUouhMAoKaYikFQCYrB4ZIKQXRHgfw1BpGa1ngRhzKEKfsiC8xgLwt9UBjhpCNHR/hOYVlNBunxIAhfRF0gpAVVDKoX1B5Bht43B/iauEZuYX06Qj5Z5/g7eU9gXKyB1VUwCF9EVKCwCAyCDMHj7A/+JjFvHUAf+OxUW2fRFBKCikK1JeAGIBW9epoKCQZgKgoKBwIpQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikLYj+Pwk7SiFpR3rfAAAAAElFTkSuQmCC'
    decodedImage = base64.b64decode(enocdedImage)
    img = tk.PhotoImage(data=decodedImage)
    master.wm_iconphoto(True, img)

    #Variable List
    global tk_test_list, ModelDropdown, SNText, StockText, MnfText, ColorText, CxText, POText, YouText, USB_Port_Text, Attenuation,IP_Text,IP_Port_Text,Tune_DSA_EN
    buttonwidth = 10
    atten = []
    atten.append('Disabled')
    for num in list(range(0,127)):
        atten.append(num/4)

    Tune_DSA_EN = tk.IntVar()

    COM_ports = list(serial.tools.list_ports.comports())
    ports = []
    for port in COM_ports:
        ports.append(port.description)

    tk_test_list = {
                'Voltage':False, 'Current':False,'LO_Lock':False,'NFG':False,
                'LO_Leak_Input':False,'LO_Leak_Output':False,'S11':False,'S22':False,
                'P1dB':False, 'Image_Rej':False,'In_Band_Spur':False,'Phase_Noise':False
                }

    ModelOptions = sorted(Settings_List_Options(settings_master_list, "Name"))

    #Menu
    menubar = tk.Menu(master)
    filemenu = tk.Menu(menubar, tearoff=0)
    calibrate_menu = tk.Menu(filemenu, tearoff=0)
    filemenu.add_cascade(label="Calibrate...",menu=calibrate_menu,underline=0)
    calibrate_menu.add_command(label="ENR Table", command=lambda:ENRCalibrationWindow(master))
    calibrate_menu.add_command(label="Return Loss (S11/S22)", command=donothing)
    #calibrate_menu.add_command(label="S22", command=donothing)
    #calibrate_menu.add_command(label="Save as...", command=donothing)
    filemenu.add_separator()
    filemenu.add_command(label="Exit", command=master.destroy)
    menubar.add_cascade(label="File", menu=filemenu)
    master.config(menu=menubar)

    #Model Frame
    ModelFrame = tk.LabelFrame(master,text="Unit Selection Menu")
    tk.Label(ModelFrame,text="Profile: ").grid(row=1,column=1,sticky='W')
    ModelDropdown = ttk.Combobox(ModelFrame,value=ModelOptions) ##Makes the menu
    ModelDropdown.set('None') ## Set Default Value
    ModelDropdown.grid(row=1,column=2,sticky='W',ipadx=115) ## Positions the Menu
    ModelFrame.pack(padx=5,pady=5,fill=tk.X)

    #Test Frame
    TestFrame = tk.LabelFrame(master,text="Test Selection Menu")
    column = 0
    row = 0
    for test in tk_test_list.keys():
        tk_test_list[test] = tk.IntVar()
        tk_test_list[test].set(True)
        tk.Checkbutton(TestFrame,text=F'{test}',variable=tk_test_list[test]).grid(row=row, column=column,sticky='W')
        column += 1
        if(column > 3):
            row += 1
            column = 0

    tk.Button(TestFrame,text='NFG Only',command=lambda:TestSelection_NFG_ONLY(tk_test_list),width=buttonwidth).grid(row=row+1,column=0,padx=2,pady=2,sticky='W')
    tk.Button(TestFrame,text='Deselect All',command=lambda:TestDeselectAll(tk_test_list),width=buttonwidth).grid(row=row+1,column=1,padx=2,pady=2,sticky='W')
    tk.Button(TestFrame,text='Select All',command=lambda:TestSelectAll(tk_test_list),width=buttonwidth).grid(row=row+1,column=2,padx=2,pady=2,sticky='W')
    TestFrame.pack(padx=5,pady=5,fill=tk.X)

    #Communication Frame
    CommunicationFrame = tk.LabelFrame(master,text="Communication Menu")
    tk.Label(CommunicationFrame,text='COM Port:').grid(row=1,column=1,sticky='E')
    USB_Port_Text = ttk.Combobox(CommunicationFrame,value=ports,state='normal',width=25)
    USB_Port_Text.grid(row=1,column=2,sticky='W',ipadx=2)
    tk.Label(CommunicationFrame,text='                  IP:').grid(row=1,column=3,sticky='E')
    IP_Text = tk.Entry(CommunicationFrame)
    IP_Text.grid(row=1,column=4,sticky='W',ipadx=2)
    tk.Label(CommunicationFrame,text='Port:').grid(row=2,column=3,sticky='E')
    IP_Port_Text = tk.Entry(CommunicationFrame)
    IP_Port_Text.grid(row=2,column=4,sticky='W',ipadx=2)
    CommunicationFrame.pack(padx=5,pady=5,fill=tk.X)

    #Attenuation Menu
    AttenuationFrame = tk.LabelFrame(master,text="Attenuation Menu")
    AttName = tk.Label(AttenuationFrame,text="Manual Set DSA:").grid(row=1,column=1,sticky='W')
    Attenuation = ttk.Combobox(AttenuationFrame,value=atten)
    Attenuation.set('0.5')
    Attenuation.grid(row=1,column=2,sticky='W')
    tk.Label(AttenuationFrame,text='dB        Gain = Max Gain - DSA').grid(row=1,columnspan=2,column=3,sticky='E')
    tk.Checkbutton(AttenuationFrame,text=F'Autotune DSA to target unit gain',variable=Tune_DSA_EN).grid(row=2, column=1,sticky='W',columnspan=2)

    AttenuationFrame.pack(padx=5,pady=5,fill=tk.X)

    #Information Frame
    InfoFrame = tk.LabelFrame(master,text="Information Menu")
    tk.Label(InfoFrame,text='Serial Number:').grid(row=1,column=1,sticky='E')
    SNText = tk.Entry(InfoFrame)
    SNText.grid(row=1,column=2,sticky='W')
    tk.Label(InfoFrame,text='Stock Number:').grid(row=1,column=3,sticky='E')
    StockText = tk.Entry(InfoFrame)
    StockText.grid(row=1,column=4,sticky='W')
    tk.Label(InfoFrame,text='Supplier S/N:').grid(row=2,column=1,sticky='E')
    MnfText = tk.Entry(InfoFrame)
    MnfText.grid(row=2,column=2,sticky='W')
    tk.Label(InfoFrame,text='Colour:').grid(row=2,column=3,sticky='E')
    ColorText = tk.Entry(InfoFrame)
    ColorText.grid(row=2,column=4,sticky='W')
    tk.Label(InfoFrame,text='Customer:').grid(row=3,column=1,sticky='E')
    CxText = tk.Entry(InfoFrame)
    CxText.grid(row=3,column=2,sticky='W')
    tk.Label(InfoFrame,text='PO Number:').grid(row=3,column=3,sticky='E')
    POText = tk.Entry(InfoFrame)
    POText.grid(row=3,column=4,sticky='W')
    tk.Label(InfoFrame,text='Tester:').grid(row=4,column=1,sticky='E')
    YouText = tk.Entry(InfoFrame)
    YouText.grid(row=4,column=2,sticky='W')
    InfoFrame.pack(padx=5,pady=5,fill=tk.X)

    tk.Button(master,text='Clear',command=ClearGUI,width=buttonwidth).pack(anchor='sw',padx=5,pady=5)
    tk.Button(master,text='Load Last',command=LoadGUIvalues,width=buttonwidth).pack(anchor='sw',padx=5,pady=5)
            

    #tk.Button(master,text='OFF',command=StopATE,width=width).grid(row=7,column=1,sticky='W',padx=2,pady=2)
    tk.Label(master,text ='Press CTRL+C to stop ATE during test.').pack(anchor='sw',padx=5)
    tk.Button(master,text='BEGIN',command=StartATE,width=buttonwidth).pack(anchor='se',padx=5,pady=5)

    master.mainloop()

def ClearGUI():
    entrybox = [SNText, StockText, MnfText, ColorText, CxText, POText, YouText, USB_Port_Text, IP_Text, IP_Port_Text]
    for item in entrybox:
        item.delete(0, tk.END)
    TestDeselectAll(tk_test_list)
    ModelDropdown.set("None")
    Attenuation.set('Disabled')
    Tune_DSA_EN.set(False)

def LoadGUIvalues():

    ClearGUI()
     
    with open(GUI_VALUES_PATH, 'r') as f:
        buffer = json.load(f)

    for key,value in buffer["Test_List"].items():
        if value:
            tk_test_list[key].set(True)
    try:
        ModelDropdown.set(buffer["Name"])
    except:
        ModelDropdown.set("None")
    try:
        Attenuation.set(buffer['Attenuation'])
    except:
        Attenuation.set('0.5')
    SNText.insert(0,buffer['Orbital_SN'])
    StockText.insert(0,buffer['stock_number'])
    MnfText.insert(0,buffer['Manufacturer_SN'])
    ColorText.insert(0,buffer['Color'])
    CxText.insert(0,buffer['Customer'])
    POText.insert(0,buffer['PO'])
    YouText.insert(0,buffer['Tester'])
    USB_Port_Text.insert(0,buffer['Serial_Port'])
    IP_Text.insert(0,buffer['IP'])
    IP_Port_Text.insert(0,buffer['IP_Port'])
    Tune_DSA_EN.set(buffer['DSA_Autotune'])

def SaveGUIvalues(path,values):
    with open(path, 'w') as f:
        json.dump(values,f)

def TestDeselectAll(test_list):
    for test in test_list.keys():
        test_list[test].set(False)

def TestSelectAll(test_list):
    for test in test_list.keys():
        test_list[test].set(True)

def TestSelection_NFG_ONLY(test_list):
    TestDeselectAll(test_list)
    test_list['Voltage'].set(True)
    test_list['Current'].set(True)
    test_list['LO_Lock'].set(True)
    test_list['NFG'].set(True)

def StartATE():
    SN = str(SNText.get())
    tester = str(YouText.get())
    if(SN == ''):
        SN = 'No_Name'
    
    folder_path = Make_Folder(ATE_PATH, SN) 

    info = F"Unit: {SN} | Tester: {tester} | Script: {os.path.basename(__file__)} |"

    global logger
    logger = setup_logger(F"{SN}", F"{folder_path}\\{SN}.log", '[%(levelname)s] %(message)s', True)
    ATE_logger = setup_logger('ATE1', 'ATE1.log', '%(asctime)s || %(message)s', False)
    
    ATE_logger.info(info)
    logger.info(info)
    ATE_logger.handlers.clear()
    logger.info(F"Setup logger: {folder_path}\\{SN}.log")
    
    try:
        main(folder_path)
    except Exception as e:
        error = Catch_Exception(e)
        logger.critical(error)
        StopATE()

def StopATE():

    Pwr_off()
    if Resources['SG']:
        SG.Output(False)
    if Resources['SA']:
        SA.write(':ABORt')
    logger.info('Stopping ATE')
    logger.handlers.clear()
    sys.exit()

def Catch_Exception(exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error = [exc_type ,fname,exc_tb.tb_lineno,exception]

    return error
    
def Settings_List_Options(settings_list, key):
    options = []
    for i in range(len(settings_list)):
        options.append(settings_list[i][0][key]) 
    return options 

def Sleepy(duration):
    logger.debug(F'Extra sleep time: {duration} seconds')
    time.sleep(duration) 

def Popup(master,name,width,height):
    window = tk.Toplevel(master)
    window.title(F"{name}")
    window.geometry(F"{width}x{height}")
    #window.configure(bg=BG1)
    window.resizable(height=1,width=1)

    return window

def donothing():
    tk.messagebox.showwarning("Warning", "Functionality under construction")

def ENRCalibrationWindow(master):
    width = 500
    ENRWindow = Popup(master,"ENR Calibration",width,700)

    #Select Units
    step1frame = tk.LabelFrame(ENRWindow,text="Select Profile(s) to Calibrate: (CTRL-A to select all)") #Select units from dropdown list
    scrollbar = tk.Scrollbar(step1frame)
    scrollbar.pack(side=tk.RIGHT,fill=tk.BOTH)
    ModelListbox = tk.Listbox(step1frame,yscrollcommand=scrollbar.set,selectmode = "multiple")
    scrollbar.config(command=ModelListbox.yview)
    ModelListbox.pack(expand = tk.YES, fill = tk.X)
    for settings in settings_master_list:
        ModelListbox.insert(tk.END, settings[0]['Name'])
    tk.Label(step1frame,text="Note: All associated .STA files under unit Profile(s) will be calibrated").pack(anchor='w')
    step1frame.pack(fill = tk.X,padx=5,pady=5)

    step2frame = tk.LabelFrame(ENRWindow,text="Select ENR Table:") #Select table from .csv
    path_text = tk.StringVar()
    tk.Button(step2frame,text='Select File',command=lambda:AskPath(path_text),width=15,bd=2).pack(anchor='w')
    tk.Entry(step2frame,textvariable=path_text,width=50).pack(fill='both',expand=True)
    tk.Label(step2frame,text="Note: Either .ENR or .csv filetype accepted").pack(anchor='w')
    tk.Label(step2frame,text="Note2: The selected ENR table will be automatically saved to the Spectrum Analyzer as a .ENR filetype at the start of the calibration",wraplength=width-10).pack(anchor='w')
    tk.Label(step2frame,text="Note3: The saved ENR table will be named [Noise Source Serial Number].ENR ").pack(anchor='w')
    step2frame.pack(fill=tk.X,padx=5,pady=5)

    step3frame = tk.LabelFrame(ENRWindow,text="Calibration Information:") #Enter noise source serial number
    serial_text = tk.StringVar()
    model_text = tk.StringVar()
    tester_text = tk.StringVar()
    tk.Label(step3frame,text="Noise Source Model Number:").grid(row=1,column=1,sticky='E')
    tk.Entry(step3frame,textvariable=model_text,width=10).grid(row=1,column=2) 
    tk.Label(step3frame,text="Noise Source Serial Number:").grid(row=2,column=1,sticky='E')
    tk.Entry(step3frame,textvariable=serial_text,width=10).grid(row=2,column=2) 
    tk.Label(step3frame,text="Tester:").grid(row=3,column=1,sticky='E')
    tk.Entry(step3frame,textvariable=tester_text,width=10).grid(row=3,column=2)
    step3frame.pack(fill=tk.X,padx=5,pady=5)

    step4frame = tk.LabelFrame(ENRWindow,text="Steps to Calibrate:")
    tk.Label(step4frame,text="1. Select Unit Profile(s) to calibrate",anchor='w').pack(fill='both')
    tk.Label(step4frame,text="2. Create an ENR file using the Noise Source's ENR table",wraplength=width,anchor='w').pack(fill='both')
    tk.Label(step4frame,text="3. Use 'Select File' button to load ENR file",wraplength=width,anchor='w').pack(fill='both')
    tk.Label(step4frame,text="4. Fill in Calibration Information menu",anchor='w').pack(fill='both')
    tk.Label(step4frame,text="5. Remove coax input from Spectrum Analyzer input and replace with Noise Source",anchor='w').pack(fill='both')
    tk.Label(step4frame,text="6. Press 'Calibrate' to begin. Follow IDLE shell prompts",anchor='w').pack(fill='both')
    step4frame.pack(fill=tk.X,padx=5,pady=5)

    tk.Checkbutton(ENRWindow,text='I have read and understand the steps above',command=lambda:CheckButtonFlip(CalibrateButton)).pack()
    CalibrateButton = tk.Button(ENRWindow,text='Calibrate',command=lambda:ENRCalibration(ModelListbox, path_text.get(), serial_text.get(), tester_text.get(), model_text.get()),width=15,bd=2,state='disabled')
    CalibrateButton.pack()

def CheckButtonFlip(button):
    if(button['state'] == 'disabled'):
        button['state'] = 'normal'
    else:
        button['state'] = 'disabled'

def filterPort(text):
    #filter port number from string
    stripped_port = text.split('COM')[-1]
    filtered_port = ''
    for item in stripped_port:
        if(item.isdigit()):
            filtered_port += item

    return filtered_port

def AskPath(path_text):
    path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.ENR"), ("All files", "*.*"))).name
    path_text.set(path)    

def ENRCalibration(listbox, file_path, SN, tester, NS_model):
    info = F"ENR CALIBRATION | Tester: {tester} | Script: {os.path.basename(__file__)} | Noise Source: {SN}"
    cal_logger = setup_logger(F"{SN}", F"{CAL_PATH}\\{timestamp()[1]}_ENR_Calibration.log", '[%(levelname)s] %(message)s', True)
    cal_logger.info(info)

    #Validation
    if not file_path:
        cal_logger.error("No ENR table selected")
        cal_logger.handlers.clear()
        return
    
    with open(file_path) as f:
        ENR_matrix = list(csv.reader(f))

    frequency = []
    ENR = []
    if file_path.endswith(".ENR"):
        if not(ENR_matrix[0][0] == '# ENR Data File'):
            cal_logger.error("Unable to read ENR table")
            cal_logger.handlers.clear()
            return
        for row in ENR_matrix[13:]:
            frequency.append(row[0])
            ENR.append(row[1])
    elif file_path.endswith(".csv"):
        for row in ENR_matrix:
            frequency.append(row[0])
            ENR.append(row[1])
    else:
        cal_logger.error("Unable to read ENR table")
        cal_logger.handlers.clear()
        return
    if not listbox.curselection():
        cal_logger.error("No profiles selected")
        cal_logger.handlers.clear()
        return
    if not (SN.isalnum() and len(SN)<20):
        cal_logger.error("Noise Source Serial Number not valid.")
        cal_logger.error("Serial must be alphanumeric and less than 20 charaters")
        cal_logger.handlers.clear()
        return
    if not (SN.isalnum() and len(NS_model)<13):
        cal_logger.error("Noise Source Model Number not valid.")
        cal_logger.error("Serial must be alphanumeric and less than 13 charaters")
        cal_logger.handlers.clear()
        return
    if not tester.isalpha():
        cal_logger.error("Tester name not valid")
        cal_logger.handlers.clear()
        return

    ATE_logger = setup_logger('ATE1', 'ATE1.log', '%(asctime)s || %(message)s', False)
    ATE_logger.info(info)
    ATE_logger.handlers.clear()

    settings_list = []
    for i in listbox.curselection():
        settings_list += DS_Gen.Find_Settings(settings_master_list, listbox.get(i), 'Name')

    ENR_data_string = ""
    for index,value in enumerate(frequency):
        ENR_data_string += F"{frequency[index]},{ENR[index]}"
        if not(frequency[-1] == value):
            ENR_data_string += ","

    input("Please remove coaxial cable from Spectrum Analyzer input and replace with Noise Source")

    before = timestamp()[2]

    flag = True
    calibrated_states = []
    room_temperature = None
    for settings in settings_list:
        if settings['Name']:
            cal_logger.info(F"Profile: {settings['Name']}")
        if(calibrated_states.count(settings['nf_gain_state_file'])):
            cal_logger.debug(F"Skipping {settings['nf_gain_state_file']}: Already Calibrated")
            continue
        elif(settings['nf_gain_state_file'] == ''):
            cal_logger.warning(F"No state file associated with unit {settings['Name']}")
            continue
        calibrated_states.append(settings['nf_gain_state_file'])

        #Load state file
        SA.write('*CLS')
        SA.write("MMEM:LOAD:STAT 1,'" + settings["nf_gain_state_file"] + "'")
        cal_logger.debug(F"Load state: {settings['nf_gain_state_file']}")
        Wait(SA,cal_logger)
        time.sleep(5)

        SA.write(F":SYSTem:ERRor:VERBose ON")
        SA.write(":SENSe:NFIGure:CORRection:ENR:COMMon:STATe ON")
        SA.write(":SENSe:NFIGure:CORRection:ENR:MODE TABLe")

        if(flag):
            #Get Room Temperature of Calibration
            room_temperature = float(Get_Temp()[1])
            cal_logger.info(F'Room temperature: {room_temperature}C')
            #Save ENR
            SA.write('*CLS')
            SA.write(F":SENSe:NFIGure:CORRection:ENR:CALibration:TABLe:DATA {ENR_data_string}")
            cal_logger.debug(SA.query(F":SENSe:NFIGure:CORRection:ENR:CALibration:TABLe:DATA?"))
            SA.write(F":SENSe:NFIGure:CORRection:ENR:CALibration:TABLe:ID:DATA '{NS_model}'")
            SA.write(F":SENSe:NFIGure:CORRection:ENR:CALibration:TABLe:Serial:DATA '{SN}'")
            SA.write(F":MMEM:STORe:ENR CALibration, 'C:{SN}.ENR'")
            error = SA.query('SYSTem:ERRor?')
            if(error == '+12604,"File already exists"\n'):
                SA.write(F":MMEMory:DELete 'C:{SN}.ENR'")
                SA.write(F":MMEM:STORe:ENR CALibration, 'C:{SN}.ENR'")
                cal_logger.debug(F"Overwrite ENR Table in Spectrum Analyzer: C:{SN}.ENR")
            else:
                cal_logger.info(F"Saved ENR Table to Spectrum Analyzer: C:{SN}.ENR")
            flag = False

        #Calibrate
        cal_logger.info(F"Calibrating state: {settings['nf_gain_state_file']}")
        SA.write(F":MMEM:LOAD:ENR CALibration, 'C:{SN}.ENR'")
        if(room_temperature):
            SA.write(F':SENSe:NFIGure:CORRection:TEMPerature:BEFore {room_temperature} CEL')
            SA.write(F':SENSe:NFIGure:CORRection:TEMPerature:AFTer {room_temperature} CEL')
            cal_logger.debug(F"Update Before/After Tables with room temperature {room_temperature}C")
        SA.write('*CLS')
        SA.write(":SENSe:NFIGure:CORRection:COLLect:ACQuire STANdard")
        CalWait(SA, cal_logger)

        #Save State
        SA.write('*CLS')
        SA.write(F"MMEM:STORe:STAT 1,'{settings['nf_gain_state_file']}'")
        error = SA.query('SYSTem:ERRor?')
        if(error == '+615,"File exists"\n'):
            SA.write(F":MMEMory:DELete '{settings['nf_gain_state_file']}'")
            SA.write(F"MMEM:STORe:STAT 1,'{settings['nf_gain_state_file']}'")
            cal_logger.debug(F"Overwrite state: {settings['nf_gain_state_file']}")
        else:
            cal_logger.debug(F"Saved state: {settings['nf_gain_state_file']}")

    global SLEEPYTIME
    after = timestamp()[2]
    cal_logger.debug(F"Time spent waiting on equipment and loading state files: {SLEEPYTIME}")
    cal_logger.debug(F"Total calibration time: {after-before}")
    cal_logger.info("Calibration Complete")
    cal_logger.handlers.clear()
    SLEEPYTIME = datetime.timedelta(seconds=0)

def CalWait(resource, log):
    #cal time ~1.5 minutes per state
    global SLEEPYTIME
    resource.timeout = 60000 * 4 #Set max wait time to 4 minutes
    log.debug("Waiting on resource...")
    before = timestamp()[2]
    resource.query('*OPC?')
    after = timestamp()[2]
    duration = after - before
    SLEEPYTIME += duration
    log.debug(F"Time waited on resource: {duration}")
    resource.timeout = 20000

settings_master_list = DS_Gen.Load_All_Settings(SETTINGS_PATH) #Load settings
Equipment_Init() 
GUI(settings_master_list)

#Version Log:
#------------------------------
#R2 - 20220727 -    Ported from LNB ATE2 R6.py. Import related modules. 
#                   Remove temperature profiles/temperature chamber testing capabilities. 
#                   Auto generated limit lines
#                   Signal gen power limit
#                   logging
#                   removed shutil from SA and VNA screenshot and data collection functions
#                   Variable ripple analysis 
#                   LO Leak avoid harmonics
#R2.1 - 20220930 -  ENR Calibration for state files. Fixed duplicate logging. Added test selection menu
#R2.2 - 20221106 -  Added option to disable attenuation settings. 
#                   Increased coms delay from 0.15 to 0.5 to avoid missing build string. 
#                   Added $pll,* command for TriKa/DKa units. 
#                   Accounted for DSA set =/= band set
#R2.3 - 20230207 -  Updated to LNB_Communication_R10: changed all 'send' to 'query'. 
#                   Send $dbg commands and $pll command for logging. Record $pll to .json file 
#                   Close logging handlers to avoid duplicate logs in ATE1_logger. 
#                   Extended Atten steps to include 0 and changed increment to 0.25/step. 
#                   Added dropdown menu for COM ports. 
#                   Implimented Tian's auto set DSA function into NFG function. 
#                   Split "NFG sweep" into seperate function
#                   SG broken off into seperate class to manage Holzworth, Anritsu, and Keysight Signal Generators
