#Ported from DS_GV0.1.py
#Adjusted by Benjamin Stadnik
#Orbital Research Ltd
#2021-04-14
#Python 3.9

import xlsxwriter
import xlwings as xw
import json
import time
import sys
import os
import csv

settings_path = '\\\ORB-SVR-FS01\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\ATE1_SETTINGS.csv'
LOGO = '\\\ORB-SVR-FS01\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\orblogo.png'

def Generate(folder_path, filename, data, settings):
    
    workbook = xlsxwriter.Workbook(folder_path + '\\' + filename + '_Band' + str(data['Band']) +'.xlsx')
    sheet1_name = ''
    sheet2_name = ''
    try:
        if(bool(data['Temperature']['Enable'])):
            sheet1_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Datasheet'
            sheet2_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Data'
        else:
            sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
            sheet2_name = 'Band ' + str(data['Band']) + ' Data'
            
    except:
        sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
        sheet2_name = 'Band ' + str(data['Band']) + ' Data'
        
    worksheet = workbook.add_worksheet(sheet1_name)
    worksheet.set_landscape()
    global not_bold
    bold = workbook.add_format({'bold': True,'font_name': 'Helvetica'})
    not_bold = workbook.add_format({'bold': False,'font_name': 'Helvetica'})
    not_bold_data = workbook.add_format({'bold': False,'num_format': '#,##0.00','font_name': 'Helvetica'})
    bold.set_font_size(8)
    not_bold.set_font_size(8)

    # Add a format. Light red fill with dark red text.
    global format_fail
    format_fail = workbook.add_format({'bg_color': '#FFC7CE', 'font_color': '#9C0006'})
    global format_pass
    # Add a format. Green fill with dark green text.
    format_pass = workbook.add_format({'bg_color': '#C6EFCE', 'font_color': '#006100'})

    ## Client, PO and Date 
    worksheet.write('A2', 'Client', bold) ## Client Text
    worksheet.write('B2', data['Customer'], bold) ## Client Name    
    worksheet.write('A3', 'PO', bold) ## PO Text
    worksheet.write('B3', data['PO'], bold) # PO Number
    worksheet.write('A4', 'Date', bold) ## Date Text
    worksheet.write('B4', data['Date'], bold) ## Date Field

    ##Model, Serial and Stock
    worksheet.write('C2', 'Model', bold) ## Model Text
    worksheet.write('D2', data['Model'], bold) ## Model Number / Product Number
    worksheet.write('C3', 'Serial Number', bold) ## Unit Serial Text
    worksheet.write('D3', data['Orbital_SN'], bold) ## Serial Number
    worksheet.write('D4', data['stock_number'], bold) ## Stock Number
    worksheet.write('C4', 'Stock', bold) ## Stock Text

    ## Tester and Reviewer
    worksheet.write('E3', 'Tested By', bold) ## Tested by Text
    worksheet.write('F3', data['Tester'], not_bold) ## Tester Name
    worksheet.write('E4', 'Reviewed By', bold) ## Checked by Text
    worksheet.write('F4', 'IM', not_bold) ## Reviewer Name

    ## Logo Insert
    worksheet.insert_image('G1', LOGO)

    ##Set column width
    worksheet.set_column('B:C',20)
    worksheet.set_column('D:F',15)
    worksheet.set_column('G:H',15)

    #Headings
    worksheet.write('B6', 'Compliance Parameters',bold) 
    worksheet.write('C6', 'Specification', bold)
    worksheet.write('D6','Unit',bold)
    worksheet.write('E6','Status',bold)
    worksheet.write('G6','Measured Parameters',bold)
    worksheet.write('H6','Spec',bold)
    worksheet.write('I6','Data',bold)
    worksheet.write('J6','Unit',bold)
    worksheet.write('L6','Phase Noise',bold)
    worksheet.write('M6','Spec',bold)
    worksheet.write('N6','Data',bold)
    worksheet.write('O6','Unit',bold)

    if 'Show' in settings['Note_4'] and data['NFG']['DSAdB']: 
        worksheet.write('G4','DSA Setting',bold) 
        worksheet.write('H4',str(data['NFG']['DSAdB']) + ' dB' + '(' + data['NFG']['DSAhex'] + ')',not_bold) 

    ## Parameter Info
    Compliance =    [
                    ['RF Input Frequency', settings['RF_In'], 'GHz', 'confirmed'],
                    ['IF Output Frequency', settings['IF_Out'], 'MHz', 'confirmed'],
                    ['Local Osc Frequency', settings['LO_Spec'], 'GHz', 'confirmed'],
                    ['DC Input Voltage Range', settings['Volt_Spec'], 'VDC', 'confirmed'],
                    ['Input Flange', settings['Input_Connector'],'std', 'confirmed'],
                    ['Output Connector',settings['Output_Connector'],'std','confirmed'],
                    ['Desense @-40 dBm input',settings['Desense_Spec'],'dB',"0.05 max"],
                    ['Overdrive @-20dBm input','no harm','dBm','confirmed'],
                    ['Size',settings['Size'],'mm','confirmed'],
                    ['Weight',settings['Weight'],'g','confirmed'],
                    ['Color',data['Color']]
                    ]
         #Datasheet_Generator_R8_TX2DF1.py -20230921 - DF add 'confirmed to size and weight'
    Write_Matrix(worksheet,6,1,Compliance)
    print("hi1")
    upper, lower, gain = Get_Gain_Spec(settings['Gain_Spec'])
    MasterPass = []
    SpurPass = 0 
    if(data['Model'] == 'Horizon'):
        Measured =  [
                    ['Noise Figure, Max', settings['NF_Spec_Max'], data['NFG']['max_NF'], 'dB'],
                    ['Noise Figure, Ave', settings['NF_Spec_Ave'], data['NFG']['averageNF'], 'dB'],
                    ['Gain (Ave)', '60', data['NFG']['averageGain'],'dB'],
                    ['Gain Flatness', settings['Per_900M_Spec'], data['NFG']['Ripple_900M_max'],'dB'],
                    ['Max Ripple 10 MHz', settings['Per_10M_Spec'], data['NFG']['Ripple_10M_max'], 'dB'],
                    ['Image Rejection',settings['ImRej_Spec'],data['min_image_rej'],'dBc'],
                    ['LO Leakage Input', settings['LoLeakIn_Spec'],data['lo_leakage_in'],'dBm'],
                    ['LO Leakage Output', settings['LoLeakOut_Spec'], data['lo_leakage_out'],'dBm'],
                    ['P1dB (min)',settings['P1dB_Spec'],data['P1dB']['P1dB'],'dBm'],
                    ['OIP3 (min)',settings['OIP3_Spec'],data['P1dB']['OIP3'],'dBm'],
                    [F"DC Current, {settings['Voltage']}V",settings['Current_Spec'],data['current'],'mA'],
                    ['Input VSWR (max)',settings['S11_Spec'],data['S11']],
                    ['Output VSWR (max)',settings['S22_Spec'],data['S22']],
                    ['Spur at RX Input','-70',data['spur_pass'], 'dBc'],
                    ['In Band Spurious, Maximum','-50',-data["max_spur_num"], 'dBc'],
                    ['Out of Band Spurious, Maximum','-40',data['max_harmonic'], 'dBc'],
                    ['IP1dB (min)',settings['IP1dB_Spec'],data['P1dB']['IP1dB'],'dBm'],
                    ['IIP3 (min)',settings['IIP3_Spec'],data['P1dB']['IIP3'],'dBm'],
                    ]
        MasterPass.append(TestPass(worksheet,'I7',data['NFG']['max_NF'],settings['NF_Spec_Max'],0))
        MasterPass.append(TestPass(worksheet,'I8',data['NFG']['averageNF'],float(settings['NF_Spec_Ave'])+0.02,0))
        MasterPass.append(TestPass(worksheet,'I9',data['NFG']['averageGain'],61.5,58.5))
        MasterPass.append(TestPass(worksheet,'I10',data['NFG']['Ripple_900M_max'],float(settings['Per_900M_Spec'][3:]),0))
        MasterPass.append(TestPass(worksheet,'I11',data['NFG']['Ripple_10M_max'],float(settings['Per_10M_Spec'][3:])+0.005,0))                             
        MasterPass.append(TestPass(worksheet,'I12',data['min_image_rej'],float(settings['ImRej_Spec']),-1000))
        MasterPass.append(TestPass(worksheet,'I13',data['lo_leakage_in'],float(settings['LoLeakIn_Spec']),-1000))
        MasterPass.append(TestPass(worksheet,'I14',data['lo_leakage_out'],float(settings['LoLeakOut_Spec']),-1000))
        MasterPass.append(TestPass(worksheet,'I15',data['P1dB']['P1dB'],1000,0))
        MasterPass.append(TestPass(worksheet,'I16',data['P1dB']['OIP3'],1000,float(settings['OIP3_Spec'])))
        MasterPass.append(TestPass(worksheet,'I17',data['current'],float(settings['Current_Spec']),0))
        MasterPass.append(TestPass(worksheet,'I18',data['S11'],float(settings['S11_Spec'][:-2]),0))
        MasterPass.append(TestPass(worksheet,'I19',data['S22'],float(settings['S22_Spec'][:-2]),0))
        worksheet.conditional_format('I20', {'type':     'cell',
                                        'criteria': '<>',
                                        'value':   '"Pass"',
                                        'format':   format_fail})
        worksheet.conditional_format('I20', {'type':     'cell',
                                        'criteria': '=',
                                        'value':   '"Pass"',
                                        'format':   format_pass})
        print("hi")
        if data['spur_pass']:
            if data['spur_pass'] == 'Pass':
                SpurPass = 1
        MasterPass.append(SpurPass)
        MasterPass.append(TestPass(worksheet,'I21',data['max_spur_num'],-50,-1000))
        MasterPass.append(TestPass(worksheet,'I22',data['max_harmonic'],-40,-1000))
        MasterPass.append(TestPass(worksheet,'I23',data['P1dB']['IP1dB'],1000,-48))
        MasterPass.append(TestPass(worksheet,'I24',data['P1dB']['IIP3'],1000,-38))
        
        Write_Matrix(worksheet,6,6,Measured)
        A_col = ['2043.1','2043.18','','','','','','2043.3','','','']
        F_col = ['2043.4','2043.4','2043.5','2043.11','','','2043.13','2043.13','','2043.8','','2043.20','2043.21','2043.24','2043.24','2043.26','2043.10','2043.9'] 
        K_col = ['2043.17']
        Write_Column(worksheet,6,0,A_col)
        Write_Column(worksheet,6,5,F_col)
        Write_Column(worksheet,6,10,K_col)
        additional_row = 6
    else:
        print("hi2")
        
        #Datasheet_Generator_R8_TX2DF1.py -20230921 - DF add exception if Litecom in display of NF in DS
        roundedNFmax = data['NFG']['max_NF']
        roundedNFave = data['NFG']['averageNF']
        
        if not settings['Model'].find('LCL') == -1:
            print (f'Using LCL model {roundedNFmax} {roundedNFave}')
            if data['NFG']['max_NF'] >= float(settings['NF_Spec_Max']):
                roundedNFmax = round(data['NFG']['max_NF'],2)
            elif data['NFG']['max_NF'] < float(settings['NF_Spec_Ave']):
                roundedNFmax = round(data['NFG']['max_NF'],2)
            else:
                roundedNFmax = round(data['NFG']['max_NF'],1)
            roundedNFave = round(data['NFG']['averageNF'],2)
            print (f'Using updated data LCL model {roundedNFmax} {roundedNFave}')
        #Datasheet_Generator_R8_TX2DF1.py -20230921 - DF add exception if Litecom in display of NF in DS (also see )
            
        Measured =  [
                    ['Noise Figure, Max', settings['NF_Spec_Max'], roundedNFmax, 'dB'],
                    ['Noise Figure, Ave', settings['NF_Spec_Ave'], roundedNFave, 'dB'],
                    ['Gain', settings['Gain_Spec'], data['NFG']['averageGain'],'dB'],
                    [F'Max Ripple {settings["ripple_to_show"]} MHz', settings[F'Per_{settings["ripple_to_show"]}M_Spec'], data['NFG'][F'Ripple_{settings["ripple_to_show"]}M_max'], 'dB'],
                    ['In Band Spurs',settings['Spur_Spec'],data['spur_pass'], 'dBc'],
                    ['Image Rejection',settings['ImRej_Spec'],data['min_image_rej'],'dBc'],
                    ['LO Leakage Input', settings['LoLeakIn_Spec'],data['lo_leakage_in'],'dBm'],
                    ['LO Leakage Output', settings['LoLeakOut_Spec'], data['lo_leakage_out'],'dBm'],
                    ['P1dB (Output)',settings['P1dB_Spec'],data['P1dB']['P1dB'],'dBm'],
                    ['OIP3',settings['OIP3_Spec'],data['P1dB']['OIP3'],'dBm'],
                    [F"DC Current, {settings['Voltage']}V",settings['Current_Spec'],data['current'],'mA'],
                    ['Input VSWR',settings['S11_Spec'],data['S11'],''],
                    ['Output VSWR',settings['S22_Spec'],data['S22'],'']
                    ]

        #print(TestPass[:-1])
        print("hi2.2")
        Write_Matrix(worksheet,6,6,Measured)
        
        MasterPass.append(TestPass(worksheet,'I7',data['NFG']['max_NF'],settings['NF_Spec_Max'],0))
        MasterPass.append(TestPass(worksheet,'I8',data['NFG']['averageNF'],float(settings['NF_Spec_Ave']),0))
        MasterPass.append(TestPass(worksheet,'I9',data['NFG']['averageGain'],upper,lower))
        MasterPass.append(TestPass(worksheet,'I10',data['NFG'][F'Ripple_{settings["ripple_to_show"]}M_max'],float(settings[F'Per_{settings["ripple_to_show"]}M_Spec'][3:]),0))
        worksheet.conditional_format('I11', {'type':     'cell',
                                        'criteria': '<>',
                                        'value':   '"Pass"',
                                        'format':   format_fail})
        worksheet.conditional_format('I11', {'type':     'cell',
                                        'criteria': '=',
                                        'value':   '"Pass"',
                                        'format':   format_pass})  
        print("hi3")   
        if data['spur_pass']:   
            if data['spur_pass'] == 'Pass':
                SpurPass = 1
        print("hi4")
        MasterPass.append(SpurPass)                    
        MasterPass.append(TestPass(worksheet,'I12',data['min_image_rej'],1000,float(settings['ImRej_Spec'])))
        MasterPass.append(TestPass(worksheet,'I13',data['lo_leakage_in'],float(settings['LoLeakIn_Spec']),-1000))
        MasterPass.append(TestPass(worksheet,'I14',data['lo_leakage_out'],float(settings['LoLeakOut_Spec']),-1000))
        MasterPass.append(TestPass(worksheet,'I15',data['P1dB']['P1dB'],1000,float(settings['P1dB_Spec'])))
        MasterPass.append(TestPass(worksheet,'I16',data['P1dB']['OIP3'],1000,float(settings['OIP3_Spec'])))
        MasterPass.append(TestPass(worksheet,'I17',data['current'],float(settings['Current_Spec']),0))
        MasterPass.append(TestPass(worksheet,'I18',data['S11'],float(settings['S11_Spec'][:-2]),0))
        MasterPass.append(TestPass(worksheet,'I19',data['S22'],float(settings['S22_Spec'][:-2]),0))
        additional_row = 0
    print("hi5")
    
    if(settings['Spe1']):
        Write_Matrix(worksheet,19,6,[[settings['Spe1'],'','Pass']])
    if(settings['Spe2']):
        Write_Matrix(worksheet,20,6,[[settings['Spe2'],'','Pass']])

    row = 7
    if(settings['P10_Spec']):
        Write_Matrix(worksheet,row-1,11,[['10 Hz',settings['P10_Spec'],data['phase']['10Hz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['10Hz'],float(settings['P10_Spec']),-1000))
        row+=1
    if(settings['P100_Spec']):
        Write_Matrix(worksheet,row-1,11,[['100 Hz',settings['P100_Spec'],data['phase']['100Hz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['100Hz'],float(settings['P100_Spec']),-1000))
        row+=1
    if(settings['P1K_Spec']):
        Write_Matrix(worksheet,row-1,11,[['1 kHz',settings['P1K_Spec'],data['phase']['1kHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['1kHz'],float(settings['P1K_Spec']),-1000))
        row+=1
    if(settings['P10K_Spec']):
        Write_Matrix(worksheet,row-1,11,[['10 kHz',settings['P10K_Spec'],data['phase']['10kHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['10kHz'],float(settings['P10K_Spec']),-1000))
        row+=1
    if(settings['P100K_Spec']):
        Write_Matrix(worksheet,row-1,11,[['100 kHz',settings['P100K_Spec'],data['phase']['100kHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['100kHz'],float(settings['P100K_Spec']),-1000))
        row+=1
    if(settings['P1M_Spec']):
        Write_Matrix(worksheet,row-1,11,[['1 MHz',settings['P1M_Spec'],data['phase']['1MHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['1MHz'],float(settings['P1M_Spec']),-1000))
        row+=1
    if(settings['P10M_Spec']):
        Write_Matrix(worksheet,row-1,11,[['10 MHz',settings['P10M_Spec'],data['phase']['10MHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['10MHz'],float(settings['P10M_Spec']),-1000))
        row+=1
    if(settings['P100M_Spec']):
        Write_Matrix(worksheet,row-1,11,[['100 MHz',settings['P100M_Spec'],data['phase']['100MHz'],'dBc/Hz']])
        MasterPass.append(TestPass(worksheet,F'N{row}',data['phase']['100MHz'],float(settings['P100M_Spec']),-1000))
        row+=1
    
    #LO Lock 
    if data['lo_lock'] is True:
        data['lo_lock'] = 'Pass'
    if data['lo_lock'] is False:
        data['lo_lock'] = 'Fail'
    worksheet.write('L16','LO Lock',bold)
    worksheet.write('M16','Pass/Fail',not_bold)
    worksheet.write('N16',data["lo_lock"],not_bold)
    worksheet.conditional_format('N16', {'type':     'cell',
                                        'criteria': '<>',
                                        'value':   '"Pass"',
                                        'format':   format_fail})
    worksheet.conditional_format('N16', {'type':     'cell',
                                        'criteria': '=',
                                        'value':   '"Pass"',
                                        'format':   format_pass}) 

    ## Open Screenshots, invert their colours, insert to worksheet
    image1 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_NFG.PNG'
    image2 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_Phase_Noise.PNG'
    if(data['Script_Revision'].count('ATE2')):
        scale = {'x_scale': 0.535, 'y_scale': 0.535}
    else:
        scale = {'x_scale': 1, 'y_scale': 1} 
    worksheet.insert_image(F'A{22+additional_row}', image1 , scale)
    worksheet.insert_image(F'G{22+additional_row}', image2 , scale) 

    ###Notes under the screenshots, Usually detailing what equipment was used, uncertainty etc
    Notes = [
            [settings['Note_1']],
            [settings['Note_2']],
            [settings['Note_3']]
            ]
    Write_Matrix(worksheet,46+additional_row,0,Notes)

    if(data['Build_Info']):
        row = 51
        build = ''
        if(type(data['Build_Info']) is list):
            build = ' '.join(data['Build_Info'])
        else:
            build = data['Build_Info'] 
        if(type(data['PLL_Build']) is list):
            pll = ' '.join(data['PLL_Build'])
        else:
            pll = data['PLL_Build'] 
        worksheet.write('A50','Build Information: ' + build + ' ' + pll,not_bold)

    ## Adding second Page for frequency, NF, and Gain List
    worksheet2 = workbook.add_worksheet(sheet2_name)

    #Frequency	Noise 	Gain	Per 10 MHz	Per 120 Mhz	Per 500 MHz
    worksheet2.write('A1','Averages',bold)
    worksheet2.write('A2','Minimum',bold)
    worksheet2.write('A3','Range',bold)
    worksheet2.write('A4','Maximums',bold)
    worksheet2.write('A5','Frequency',bold)
    worksheet2.write('B5','Noise',bold)
    worksheet2.write('C5','Gain',bold)
    worksheet2.write('D5','Linear Gain',bold)
    worksheet2.write('E5','Linear NF',bold)

    row = 6
    Write_Column(worksheet2, row, 0, data['NFG']['Frequency'])
    Write_Column(worksheet2, row, 1, data['NFG']['NF'])
    Write_Column(worksheet2, row, 2, data['NFG']['Gain'])

    flag = 0
    for i in range(len(data['NFG']['Frequency'])):
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency'])) and flag < 1:
            index_interest_start = i
            flag = 1
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_stop_Frequency'])) and flag < 2:
            index_interest_stop = i
            flag = 2
            break
        
    #Averages, maximums and minimums
    worksheet2.write_formula('E4','=10^(B4/10)',not_bold)
    worksheet2.write_formula('D4','=10^(C4/10)',not_bold)
    worksheet2.write('B1',data['NFG']['averageNF'],not_bold)
    worksheet2.write('C1',data['NFG']['averageGain'],not_bold)    
    worksheet2.write_formula('B4','=MAX(B' + str(row+index_interest_start) + ':B' + str(row+index_interest_stop) +')',not_bold)
    worksheet2.write_formula('C4','=MAX(C' + str(row+index_interest_start) + ':C' + str(row+index_interest_stop) +')',not_bold)
    worksheet2.write_formula('C2','=MIN(C' + str(row+index_interest_start) + ':C' + str(row+index_interest_stop) +')',not_bold)
    worksheet2.write_formula('C3','=C4-C2',not_bold)

    #Pass/Fail for stuff above
    MasterPass.append(TestPass(worksheet2,'C1',data['NFG']['averageGain'],upper,lower))
    MasterPass.append(TestPass(worksheet2,'B1',data['NFG']['averageNF'],float(settings['NF_Spec_Ave']),-1000))
    MasterPass.append(TestPass(worksheet2,'C2',data['NFG']['min_Gain'],1000,lower))
    MasterPass.append(TestPass(worksheet2,'C3',float(data['NFG']['max_Gain'])-float(data['NFG']['min_Gain']),upper-lower,-1000))
    MasterPass.append(TestPass(worksheet2,'C4',data['NFG']['max_Gain'],upper,-1000))
    MasterPass.append(TestPass(worksheet2,'B4',data['NFG']['max_NF'],float(settings['NF_Spec_Max']),-1000))

    ##Write GAIN LINEAR
    Write_Column(worksheet2, row, 3, [10 ** (x/10) for x in data['NFG']['Gain']])

    ##Write NF Linear
    Write_Column(worksheet2, row, 4, [10 ** (x/10) for x in data['NFG']['NF']])

    ##Ripple Specs and Ripple
    worksheet2.write('F2','Amplitude Response, Max', bold)

    ripple = ['10', '120', '500', '550', '700', '750', '800', '900', '1000'] #Always in MHz
    col = 5
    for value in ripple:
        if(settings[F'Per_{value}M_Spec']):       
            worksheet2.write(2,col,settings[F'Per_{value}M_Spec'], bold)
            worksheet2.write(3,col,data['NFG'][F'Ripple_{value}M_max'],not_bold)
            worksheet2.write(4,col,F'Per {value} MHz',bold)
            for i in range(len(data['NFG']['Frequency'])):
                if int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + (int(value) * 1000000):
                    ripple_index = i
                    break
            Write_Column(worksheet2, 5+ripple_index, col, data['NFG'][F'Ripple_{value}M'])
            
            MasterPass.append(TestPass(worksheet2,chr(ord('@')+(col+1))+'4',data['NFG'][F'Ripple_{value}M_max'],float(settings[F'Per_{value}M_Spec'][3:]),-1000))
            
            col += 1

    try:
        lenp = int(len(data['phase']['SMOOTH'])/2)
        #Phase Noise Section
        worksheet2.write('K4','Phase Noise Measurement, Smoothed', bold)
        worksheet2.write('K5','Frequency Offset, Hz', bold)
        worksheet2.write('L5','Phase Noise, dBc/Hz', bold)

        ##WRITE Frequency Offset
        for i in range(row,row+lenp):
            tmp = "K"
            tmp = tmp + str(i)
            tmp_index = i-row
            worksheet2.write(tmp,data['phase']['SMOOTH'][tmp_index*2],not_bold)

        ##WRITE phase noise smoothed data
        for i in range(row,row+lenp):
            tmp = "L"
            tmp = tmp + str(i)
            tmp_index = i-row
            worksheet2.write(tmp,data['phase']['SMOOTH'][tmp_index*2+1],not_bold)
    except:
        pass
    
    if(data['Model'] == 'Horizon'):
        worksheet3 = workbook.add_worksheet('LO Leakage Harmonics')
        worksheet3.write('A1','Frequency',bold)
        worksheet3.write('B1','LO Leakage In Loss',bold)
        worksheet3.write('C1','LO Leakage Out Loss',bold)
        worksheet3.write('D1','LO Leakage In',bold)
        worksheet3.write('E1','LO Leakage Out',bold)

        Write_Column(worksheet3,1,0,[d['Frequency'] for d in data['lo_leak_out_harmonics']])
        
        Write_Column(worksheet3,1,3,[d['Power'] for d in data['lo_leak_in_harmonics']])
        Write_Column(worksheet3,1,4,[d['Power'] for d in data['lo_leak_out_harmonics']])
        Write_Column(worksheet3,1,1,[float(x) for x in settings['lo_leakage_input_cable_loss'].split(",")])
        Write_Column(worksheet3,1,2,[float(x) for x in settings['lo_leakage_output_cable_loss'].split(",")])
        image4 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_LO_Leakage_Input.PNG'
        image5 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_LO_Leakage_Output.PNG'

        worksheet3.write('A6','Captured LO Leakage In',bold)
        worksheet3.write('K6','Captured LO Leakage Out',bold)
        worksheet3.insert_image('A7', image4 , scale)
        worksheet3.insert_image('K7', image5 , scale) 

        offset = 0
        for i in [2,4]:
            image6 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_LO_Leakage_Input_' + str(i) + '_harmonic.PNG'
            image7 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_LO_Leakage_Output_' + str(i) + '_harmonic.PNG'

            worksheet3.insert_image(F'A{31+offset}', image6 , scale)
            worksheet3.insert_image(F'K{31+offset}', image7 , scale) 

            offset +=24

    #Code for Master Pass 
    MP = 'Pass'
    if 0 in MasterPass:
        MP = 'Fail'
    worksheet.write('E2', 'Pass/Fail', bold) ## Tested by Text
    worksheet.write('F2', MP, bold) ## Tester Name

    workbook.close()

def TestPass(worksheet,cell,data,upper,lower):
    Pass = 0 
    if upper == None:
        return Pass
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': '>',
                                        'value':   upper,
                                        'format':   format_fail})
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': 'between',
                                        'minimum':  lower,
                                        'maximum':  upper,
                                        'value':   upper,
                                        'format':   format_pass})
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': '<',
                                        'value':   lower,
                                        'format':   format_fail})
    if data == None:
        return Pass
    if float(lower)< float(data) < float(upper):
        Pass = 1

    return Pass

def Get_Gain_Spec(spec):
    if not spec:
        return None

    #Split spec string to list of numbers
    temp = ''
    numbers = []
    spec += ' '
    for character in spec:
        if character.isdigit() or character == '.':
            flag = True
            temp += character
        elif(flag):
            numbers.append(float(temp))
            temp = ''
            flag = False

    #Interpret list of numbers       
    gain = None
    lower = None
    upper = None
    if(float(len(numbers)) > 2): #Specific Spec ex:60(+3,-1)
        gain = numbers[0]
        upper = gain + numbers[1]
        lower = gain - numbers[2]
    elif(float(len(numbers)) > 1):
        if(float(numbers[1]) > 20): #Range Spec ex:55-65
            lower = numbers[0]
            upper = numbers[1]
            gain = (lower + upper) / 2 
        else: #General Spec ex:55 +/-1
            gain = numbers[0]
            lower = gain - numbers[1]
            upper = gain + numbers[1]  
    else: #No Spec
        gain = numbers[0]

    
    return upper, lower, gain

def Write_Column(worksheet, start_row, start_column, items): #coordinate mode: 'C7' = (6,2). indexing starts at (0,0)
    row = start_row
    for item in items:
        worksheet.write(row, start_column, item, not_bold)
        row += 1

def Write_Matrix(worksheet, start_row, start_column, matrix): #coordinate mode: 'C7' = (6,2). indexing starts at (0,0)
    row = start_row
    for row_list in matrix:
        col = start_column
        for item in row_list:
            worksheet.write(row, col, item, not_bold)
            col += 1
        row += 1

def Merge(folder_path, name): #Merge Datasheets for multiband units
    print('Merging Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)
    wb_new = xw.Book()
    for file in files: #Find all .xlsx files in folder
        if file.endswith('.xlsx'):
            wb_temp = xw.Book(folder_path + '\\' + file)
            for sheet in wb_temp.sheets: #Append all sheets from .xlsx file to new file
                sheet.api.Copy(Before=wb_new.sheets[-1].api)
            wb_temp.close()
            #wb_temp.app.quit()

    wb_new.sheets[-1].delete()
    wb_new.sheets[0].activate()
    wb_new.save(folder_path + '\\' + name + '.xlsx')
    #wb_new.close()
    wb_new.app.quit()

def Load_JSON(file):
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()
    return data

def GenerateFromJSON(folder_path):
    settings_master_list = Load_All_Settings(settings_path)
    print('Generating Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)
    i = 0
    for file in files: #Find all .json files in folder
        if file.endswith('.json'):
            if file.count("Settings"):
                continue
            print(file)
            data = Load_JSON(file)
            settings_list = Find_Settings(settings_master_list, data['Name'], "Name")
            settings = settings_list[i]
            Generate(os.path.dirname(__file__), os.path.splitext(file)[0].split('_Band')[0], data, settings)
            i += 1

def Find_Settings(settings_master_list, selected, key):
    #print(selected)
    settings_list = []
    for i in range(len(settings_master_list)):
        if selected == settings_master_list[i][0][key]:
            for settings in settings_master_list[i]:
                settings_list.append(settings)
    return settings_list

def Load_All_Settings(file):
    #Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)
    f.close

    #print(matrix)

    #Genrate list of dictionaries from matrix, grouping bands together if model name left blank 
    key_list = matrix[0]
    output_list = []
    temp_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            temp_dict[key_list[index]] = value
        temp_list.insert(0,temp_dict)
        if(rows[0] != ''):
            output_list.append(temp_list)
            temp_list = []
    output_list.reverse()

    output_list.pop(0)
    #print(output_list)
    
    return output_list 

def main():
    message = input("Enter 'M' to merage all datasheets in current directory. Enter 'G' to generate datasheets from RAW JSON data.")
    if(message == 'M'):
        Merge(os.path.dirname(__file__), 'Merged Datasheets')
        print('Datasheets merged. Exit program.')
    elif(message == 'G'):
        GenerateFromJSON(os.path.dirname(__file__))
        print('Generated datasheets. Exit program.')
    else:
        print('Exit program.')
        sys.exit()


if(__name__ == '__main__'):
	main()

#Change Log
#-------------------------
#Revision 2.0 - 20210902 - Added Max and Ave NF to Datasheet. Added Max and Ave NF Spec to LNB Settings. Changed 'data' and 'settings' to ATE2 naming convention. Changed raw data starting row from '6' to variable 'row'. Ripple now starts at cooresponding frequency. Added special note 1 and 2 back to datasheet
#Revision 3 - 20211109 - added "filename" variable to differentiate from "data['Orbital_SN']" dependancy. Added standalone application merge and generate functionality.
#R4 - 20220420 - Transposed ATE2_Settings.csv. Upgraded Load_All_Settings() for transposed matrix
#R5 - 20220621 - Find_Settings has find key. Merge function updated compatibility for Win7.
#R6 - 20220809 - Horizon Datasheet template, matrix and column writing functions
#R7 - 20221004 - Tian Hao Xu added TestPass function to append "Fail" beside tests that fail 
#R7_TX_2 - 20220105 - TX added coloured Pass/Fail criteria using conditional formatting + added a master Pass/Fail reporting on each band 
#R8 - 20230207 - Added FW to datasheet
#R8_TX2DF1 - 20230921 - DF add exception if Litecom in display of NF in DS near line 180 and add 'confirmed to size and weight' near line 108
#R8_TX2DF1BS - 20240129 - Fixed GenerateFromJSON applying compliance parameters to every band when manually generating datasheet and path to network drive
#R8_TX2DF1BS2 - 20240214 - Changed line 348 from row = 5 -> row = 6 changing the selected data for analysis, discluding the headers
#R9 - 20240214 - Rounding data to 2 decimal places